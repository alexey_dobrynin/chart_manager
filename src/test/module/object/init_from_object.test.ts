import PriceHistoryAppState from '../../../app/module/price_history_app/price_history_app_state'
import ChartState from '../../../app/module/chart/chart_state'
import Quote from '../../../app/module/quote/quote'
import initFromObject from '../../../app/module/object/init_from_object'
import {Scale, ScaleKeys} from '../../../app/module/scale/scale'

test('init from json test', () => {
  const priceHistoryAppStateExpected = new PriceHistoryAppState()
  priceHistoryAppStateExpected.chartState.scale = <ScaleKeys>Scale.code.tick
  const chartState = new ChartState()
  chartState.quote = '1'
  priceHistoryAppStateExpected.quoteList.items.push(new Quote(chartState, 'SBER', 'sberbank'))

  const priceHistoryAppStateReceived = new PriceHistoryAppState()
  initFromObject(
    priceHistoryAppStateReceived,
    {
      chartState: {
        scale: "1"
      },
      quoteList: {
        items: [
          {
            chartState: {quote: "1"},
            code: "SBER",
            name: "sberbank"
          }
        ]
      }
    }
  )

  expect(priceHistoryAppStateReceived).toStrictEqual(priceHistoryAppStateExpected)
})