import MovingAveragePointListCalculator
    from '../../../../../app/module/trade/strategy/moving_average/moving_average_point_list_calculator'
import MovingAverageType from '../../../../../app/module/filter/moving_average_type'
import BarPoint from '../../../../../app/module/point/bar_point'
import TickPoint from '../../../../../app/module/point/tick_point'

test('Moving Average Calculator', () => {
    const movingAverageCalculator = new MovingAveragePointListCalculator(
        MovingAverageType.typeList.movingAverageExponential,
        '11',
        '60',
        BarPoint.type.closeValue
    )

    const tickPointList = [
        new TickPoint('212', '2021-03-24 10:03:37.000'),
        new TickPoint('212', '2021-03-24 10:03:37.500'),
        new TickPoint('212', '2021-03-24 10:04:01.000'),
        new TickPoint('212', '2021-03-24 10:04:07.000'),
        new TickPoint('212', '2021-03-24 10:04:20.000'),
        new TickPoint('212', '2021-03-24 10:04:20.250'),
        new TickPoint('212.5', '2021-03-24 10:04:20.500'),
        new TickPoint('212.5', '2021-03-24 10:04:20.750'),
        new TickPoint('212.5', '2021-03-24 10:04:21.000'),
        new TickPoint('213', '2021-03-24 10:04:57.000'),
        new TickPoint('213.5', '2021-03-24 10:04:57.333'),
        new TickPoint('213.5', '2021-03-24 10:04:57.666'),
        new TickPoint('213', '2021-03-24 10:04:58.000'),
    ]

    tickPointList.forEach(function (tickPoint){
        movingAverageCalculator.addPoint(tickPoint)
    })


    const movingAverageExponentialPointListExpected = [
        new TickPoint('212', '2021-03-24 10:03:37.000'),
        new TickPoint('212', '2021-03-24 10:03:37.500'),
        new TickPoint('212', '2021-03-24 10:04:01.000'),
        new TickPoint('212', '2021-03-24 10:04:07.000'),
        new TickPoint('212', '2021-03-24 10:04:20.000'),
        new TickPoint('212', '2021-03-24 10:04:20.250'),
        new TickPoint('212.0833', '2021-03-24 10:04:20.500'),
        new TickPoint('212.1528', '2021-03-24 10:04:20.750'),
        new TickPoint('212.2107', '2021-03-24 10:04:21.000'),
        new TickPoint('212.3423', '2021-03-24 10:04:57.000'),
        new TickPoint('212.5353', '2021-03-24 10:04:57.333'),
        new TickPoint('212.6961', '2021-03-24 10:04:57.666'),
        new TickPoint('212.7468', '2021-03-24 10:04:58.000'),
    ]

    expect(movingAverageCalculator.movingAveragePointList)
        .toStrictEqual(movingAverageExponentialPointListExpected)
})