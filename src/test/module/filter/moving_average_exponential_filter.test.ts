import MovingAverageExponentialFilter from '../../../app/module/filter/moving_average_exponential_filter'
import TickPoint from '../../../app/module/point/tick_point'

const data = [
    {
        description: 'moving average exponential filter',
        movingAverageExponentialFilter: new MovingAverageExponentialFilter(2, 60),
        tickPointList: [
            new TickPoint('67.9', '2021-03-29 09:59:00'),
            new TickPoint('68.2', '2021-03-29 10:00:00'),
            new TickPoint('68.01', '2021-03-29 10:01:00'),
            new TickPoint('68.12', '2021-03-29 10:02:00'),
            new TickPoint('68.06', '2021-03-29 10:03:00'),
            new TickPoint('67.94', '2021-03-29 10:04:00'),
        ],
        movingAverageExponentialPointListExpected: [
            new TickPoint('67.9', '2021-03-29 09:59:00.000'),
            new TickPoint('68.1', '2021-03-29 10:00:00.000'),
            new TickPoint('68.04', '2021-03-29 10:01:00.000'),
            new TickPoint('68.0933', '2021-03-29 10:02:00.000'),
            new TickPoint('68.0711', '2021-03-29 10:03:00.000'),
            new TickPoint('67.9837', '2021-03-29 10:04:00.000'),
        ]
    },
    {
        description: 'moving average exponential filter (with milliseconds)',
        movingAverageExponentialFilter: new MovingAverageExponentialFilter(11, 60),
        tickPointList: [
            new TickPoint('212', '2021-03-24 10:03:37.000'),
            new TickPoint('212', '2021-03-24 10:03:37.500'),
            new TickPoint('212', '2021-03-24 10:04:01.000'),
            new TickPoint('212', '2021-03-24 10:04:07.000'),
            new TickPoint('212', '2021-03-24 10:04:20.000'),
            new TickPoint('212', '2021-03-24 10:04:20.250'),
            new TickPoint('212.5', '2021-03-24 10:04:20.500'),
            new TickPoint('212.5', '2021-03-24 10:04:20.750'),
            new TickPoint('212.5', '2021-03-24 10:04:21.000')
        ],
        movingAverageExponentialPointListExpected: [
            new TickPoint('212', '2021-03-24 10:03:37.000'),
            new TickPoint('212', '2021-03-24 10:03:37.500'),
            new TickPoint('212', '2021-03-24 10:04:01.000'),
            new TickPoint('212', '2021-03-24 10:04:07.000'),
            new TickPoint('212', '2021-03-24 10:04:20.000'),
            new TickPoint('212', '2021-03-24 10:04:20.250'),
            new TickPoint('212.0833', '2021-03-24 10:04:20.500'),
            new TickPoint('212.1528', '2021-03-24 10:04:20.750'),
            new TickPoint('212.2107', '2021-03-24 10:04:21.000')
        ]
    }
]

describe.each(data)(`Moving average exponential`, (testData) => {
    it(testData.description, () => {
        const movingAverageExponentialPointListReceived = testData.movingAverageExponentialFilter
            .filter(testData.tickPointList)

        expect(movingAverageExponentialPointListReceived)
            .toStrictEqual(testData.movingAverageExponentialPointListExpected)
    });
})