import MovingAverageWeightedFilter from '../../../app/module/filter/moving_average_weighted_filter'
import TickPoint from '../../../app/module/point/tick_point'

test('moving average weight filter', () => {
    const movingAverageWeightFilter = new MovingAverageWeightedFilter(2, 60)
    const tickPointList = [
        new TickPoint('67.9', '2021-03-29 09:59:00'),
        new TickPoint('68.2', '2021-03-29 10:00:00'),
        new TickPoint('68.01', '2021-03-29 10:01:00'),
        new TickPoint('68.12', '2021-03-29 10:02:00'),
        new TickPoint('68.06', '2021-03-29 10:03:00'),
        new TickPoint('67.94', '2021-03-29 10:04:00'),
    ]
    const movingAverageWeightPointListReceived = movingAverageWeightFilter.filter(tickPointList)
    const movingAverageWeightPointListExpected = [
        new TickPoint('67.9', '2021-03-29 09:59:00'),
        new TickPoint('68.1', '2021-03-29 10:00:00'),
        new TickPoint('68.0733', '2021-03-29 10:01:00'),
        new TickPoint('68.0833', '2021-03-29 10:02:00'),
        new TickPoint('68.08', '2021-03-29 10:03:00'),
        new TickPoint('67.98', '2021-03-29 10:04:00'),
    ]
    expect(movingAverageWeightPointListReceived).toStrictEqual(movingAverageWeightPointListExpected)
})