import MovingAverageSimpleFilter from '../../../app/module/filter/moving_average_simple_filter'
import TickPoint from '../../../app/module/point/tick_point'

test('moving average simple filter', () => {
    const movingAverageSimpleFilter = new MovingAverageSimpleFilter(2, 60)
    const tickPointList = [
        new TickPoint('67.9', '2021-03-29 09:59:00'),
        new TickPoint('68.2', '2021-03-29 10:00:00'),
        new TickPoint('68.01', '2021-03-29 10:01:00'),
        new TickPoint('68.12', '2021-03-29 10:02:00'),
        new TickPoint('68.06', '2021-03-29 10:03:00'),
        new TickPoint('67.94', '2021-03-29 10:04:00'),
    ]
    const movingAverageSimplePointListReceived = movingAverageSimpleFilter.filter(tickPointList)
    const movingAverageSimplePointListExpected = [
        new TickPoint('67.9', '2021-03-29 09:59:00'),
        new TickPoint('68.05', '2021-03-29 10:00:00'),
        new TickPoint('68.105', '2021-03-29 10:01:00'),
        new TickPoint('68.065', '2021-03-29 10:02:00'),
        new TickPoint('68.09', '2021-03-29 10:03:00'),
        new TickPoint('68', '2021-03-29 10:04:00'),
    ]
    expect(movingAverageSimplePointListReceived).toStrictEqual(movingAverageSimplePointListExpected)
})