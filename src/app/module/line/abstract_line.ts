import AbstractPoint from '../point/abstract_point'

export default class AbstractLine {

    /** @type AbstractPoint[] */
    pointList: AbstractPoint[]

    /**
     * @param {Object[]} queryResult
     */
    constructor(queryResult: object) {
        this.pointList = []
    }

    /**
     * @return {AbstractPoint[]} список точек
     */
    getPointList() {
        return this.pointList
    }

    clone() {
        const line: AbstractLine = Object.assign(Object.create(Object.getPrototypeOf(this)), this)
        line.pointList = line.pointList.slice()
        return line
    }
}