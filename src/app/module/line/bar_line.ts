import AbstractLine from './abstract_line'
import BarPoint from '../point/bar_point'

interface BarPointData{
    min_price: string,
    max_price: string,
    open_price: string,
    close_price: string,
    date_time: string
}

export default class BarLine extends AbstractLine {

    /**
     * @param {Object[]} queryResult
     */
    constructor(queryResult: BarPointData[]) {
        super(queryResult)

        queryResult.forEach(
            /**
             * @param {String} point.date_time дата и время в формате "YYYY-MM-DD hh:mm:ss"
             * @param {Number} point.min_price минимальное значение
             * @param {Number} point.max_price максимальное значение
             * @param {Number} point.open_price значение при открытии
             * @param {Number} point.close_price значение при закрытии
             */
            (point) => {
                this.pointList.push(
                    new BarPoint(
                        parseFloat(point.min_price),
                        parseFloat(point.max_price),
                        parseFloat(point.open_price),
                        parseFloat(point.close_price),
                        point.date_time)
                )
            }
        )
    }
}