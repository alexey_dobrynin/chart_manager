import AbstractLine from './abstract_line'
import TickPoint from '../point/tick_point'
import BarTicksPoint from '../point/bar_ticks_point'

export default class TickLine extends AbstractLine {

    /**
     * @param {Object[]} queryResult
     */
    constructor(queryResult: {price: string, time: string}[]) {
        super(queryResult)
        queryResult.forEach(
            /**
             * @param {String} point.time дата и время в формате "YYYY-MM-DD hh:mm:ss.uuu"
             * @param {Number} point.price цена
             */
            (point) => {
                this.pointList.push(new TickPoint(point.price, point.time))
            }
        )
    }

    /**
     * @param {TickPoint} tickPoint
     */
    addPoint(tickPoint: TickPoint) {
        this.pointList.push(tickPoint)
    }

    /**
     * @return {TickPoint[]}
     */
    getPointList() {
        return <TickPoint[]>super.getPointList();
    }

    /**
     * @return {BarTicksPoint[]}
     */
    getBarTicksList() {
        return this.getPointList().map((tickPoint) => {
            const tickLine = new TickLine([])
            tickLine.addPoint(<TickPoint>tickPoint)
            return new BarTicksPoint(tickLine, tickPoint.getDateTime())
        })
    }

    /**
     * @param {TickPoint[]} pointList
     * @return {TickLine}
     */
    setPointList(pointList: TickPoint[]) {
        this.pointList = pointList
        return this
    }

    /**
     * @return {Number[]} список цен
     */
    getPriceList() {
        return this.getPointList().map((tickPoint) => {
            return (<TickPoint>tickPoint).getPrice()
        })
    }

    /**
     * @return {Number} максимальное значение
     */
    getMaxValue() {
        return Math.max(... this.getPriceList())
    }

    /**
     * @return {Number} минимальное значение
     */
    getMinValue() {
        return Math.min(... this.getPriceList())
    }

    /**
     * @return {Number}
     */
    getOpenValue() {
        const pointList = this.getPointList()
        return pointList.length > 0
            ? (<TickPoint>pointList[0]).getPrice()
            : NaN
    }

    /**
     * @return {Number}
     */
    getCloseValue() {
        const pointList = this.getPointList()
        return pointList.length > 0
            ? (<TickPoint>pointList[this.pointList.length - 1]).getPrice()
            : NaN
    }

    getLastTickPoint() {
        const pointList = this.getPointList()
        return pointList.length > 0
            ? <TickPoint>pointList[this.pointList.length - 1]
            : undefined
    }
}