import DateTimeHelper from '../helper/date_time_helper'
import {Scale, ScaleKeys} from '../scale/scale'
import TickLine from './tick_line'
import BarTicksPoint from '../point/bar_ticks_point'
import BarLine from './bar_line'
import TickPoint from "../point/tick_point";

/** График гисограммы, хранящий в каждой точке линейный график */
export default class BarTicksLine extends BarLine {

    /**
     * @param {TickPoint[]} tickPointList
     * @param {ScaleKeys} scale если в качестве параметра передается число, то это количество секунд в периоде
     */
    constructor(tickPointList: TickPoint[], scale: ScaleKeys | number) {
        super([])
        const scaleToDate = typeof scale === 'number'
            ? Scale.getScaleToDate(scale*1000)
            : Scale.date[scale]
        this.initPointList(tickPointList, scaleToDate)
    }

    /**
     * @param {TickPoint[]} tickPointList
     * @param {Function} scaleToDate
     */
    initPointList(tickPointList: TickPoint[], scaleToDate: (date: Date) => Date)
    {
        let tickLine = new TickLine([])
        let previousScaleDate, scaleDate
        if(tickPointList.length > 0){
            scaleDate =
            previousScaleDate = scaleToDate(tickPointList[0].getDate())
            for (const tickPoint of tickPointList) {
                scaleDate = scaleToDate(tickPoint.getDate())
                if(scaleDate.getTime() !== previousScaleDate.getTime()){
                    this.pointList.push(
                        new BarTicksPoint(
                            tickLine,
                            DateTimeHelper.unixToDateTimeMillisecondsFormat(previousScaleDate.getTime())
                        )
                    )
                    tickLine = new TickLine([])
                    previousScaleDate = scaleDate
                }
                tickLine.pointList.push(tickPoint)
            }
            this.pointList.push(
                new BarTicksPoint(
                    tickLine,
                    DateTimeHelper.unixToDateTimeMillisecondsFormat(scaleDate.getTime())
                )
            )
        }

    }

    getPointList() {
        return <BarTicksPoint[]>this.pointList
    }

    /**
     * @return {TickPoint[]} список точек
     */
    getTickPointList()
    {
        const tickPointList: TickPoint[] = []
        for (const barTicksPoint of this.getPointList()) {
            tickPointList.concat(barTicksPoint.getTickPointList())
        }
        return tickPointList
    }
}