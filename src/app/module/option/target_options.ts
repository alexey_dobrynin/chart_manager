import Option from './option'
import BarPoint from '../point/bar_point'

export default class TargetOptions {

    /**
     * @return {Option[]} список целей для применения инструмента
     */
    static getOptions() {
        return [
            new Option('максимальная', BarPoint.type.maxValue),
            new Option('средняя', BarPoint.type.averageValue),
            new Option('минимальная', BarPoint.type.minValue),
            new Option('открытия', BarPoint.type.openValue),
            new Option('закрытия', BarPoint.type.closeValue)
        ]
    }
}