/** Опция */
export default class Option {

    name
    value

    /**
     * @param {String} name название опции
     * @param {String} value значение опции
     */
    constructor(name: string, value: string | null = null) {
        this.name = name
        this.value = value === null ? name : value
    }

    /**
     * @return {String} название опции
     */
    getName() {
        return this.name
    }

    /**
     * @return {String} значение опции
     */
    getValue() {
        return this.value
    }
}