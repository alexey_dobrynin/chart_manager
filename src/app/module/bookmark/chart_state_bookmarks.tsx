import React from 'react'
import Modal from '../modal/modal'
import BookmarkOperation from './bookmark_operation'
import {BookmarkEditFormComponent} from './bookmark_edit_form'
import {connector, PriceHistoryAppChildRefresh} from '../price_history_app/connector'
import Bookmark from './bookmark'

interface ChartStateBookmarksState {
  modalOpen: boolean
  modalContent: JSX.Element|null
  selectedBookmark: string
}

export interface ChartStateBookmarksInterface {
  closeModal: () => void
  state: ChartStateBookmarksState
  showBookmarkList: () => () => void
  refreshState: () => void
}

/** Закладки состояний графиков */
class ChartStateBookmarks extends React.Component<PriceHistoryAppChildRefresh, ChartStateBookmarksState> implements ChartStateBookmarksInterface {

  constructor(props: PriceHistoryAppChildRefresh) {
    super(props)
    this.state = {
      modalOpen: false,
      modalContent: null,
      selectedBookmark: ''
    }
  }


  render() {

    const priceHistoryAppState = this.props.state

    return <nav
      className="dropdown"
      id="chart_bookmarks"
    >
      <button
        className="drop_button"
        onClick={ this.showBookmarkList() }
      >
        Закладки ({ Object.keys(priceHistoryAppState.bookmarkList.getItems()).length })
      </button>
      <div className="dropdown-content">
        <div className="dropdown-item"
             onClick={ this.showBookmarkEditModal(BookmarkOperation.mode.create) }>Создать закладку</div>
      </div>
      <Modal isOpen={this.state.modalOpen} onClose={() => this.closeModal()}>
        { this.state.modalContent }
      </Modal>
    </nav>
  }

  showBookmarkList() {
    return () => {
      const bookmarkList = this.props.state.bookmarkList
      const bookmarkOptionList = Object.values(bookmarkList.getItems()).map(
        /**
         * @param {Bookmark} bookmark
         * @return {JSX.Element}
         */
        (bookmark: Bookmark) => {
          return <option key={bookmark.name} value={bookmark.name}>{bookmark.name}</option>
        }
      )
      bookmarkOptionList.unshift(
        <option key="bookmark_not_selected_option" value="" disabled>не выбрано</option>
      )

      const bookmarkListForm = <form id="bookmarkListForm">
        <h3>Список закладок</h3>
        <select
          size={20}
          defaultValue={ this.state.selectedBookmark }
          onChange={ this.changeBookmark() }
          onDoubleClick={ this.selectBookmark() }
        >
          {bookmarkOptionList}
        </select>
        <hr/>
        <button type="button" onClick={ this.selectBookmark() }>Загрузить</button>
        <button type="button" onClick={ this.showBookmarkEditModal( BookmarkOperation.mode.create ) }>Создать</button>
        <button type="button" onClick={ this.showBookmarkEditModal( BookmarkOperation.mode.edit ) }>Перезаписать</button>
        <button type="button" onClick={ this.showBookmarkEditModal( BookmarkOperation.mode.rename ) }>Переименовать</button>
        <button type="button" onClick={ this.deleteBookmark() } >Удалить</button>
      </form>

      this.showModal(bookmarkListForm)
    }
  }

  deleteBookmark() {
    return () => {
      this.props.deleteChartBookmark(this.state.selectedBookmark)
      this.setState({selectedBookmark: ''})
      this.showBookmarkList()()
    }
  }

  changeBookmark() {
    return (event: React.ChangeEvent<HTMLSelectElement>) => {
      this.setState({selectedBookmark: event.target.value})
    }
  }

  selectBookmark() {
    return () => {
      this.props.loadChartFromBookmark(this.state.selectedBookmark)
      this.props.refreshApp()
    }
  }

  /**
   * @param {String} mode
   * @return {Function}
   */
  showBookmarkEditModal(mode: string) {
    return () => {
      const bookmarkName = mode === BookmarkOperation.mode.create
        ? this.getBookmarkBlankName()
        : this.state.selectedBookmark

      const showBookmarkCreateForm = <BookmarkEditFormComponent
        name={ bookmarkName }
        mode={ mode }
        chartStateBookmarks={ this }
      />
      this.showModal(showBookmarkCreateForm)
    }
  }

  /**
   * @return {String}
   */
  getBookmarkBlankName() {
    /** @type {PriceHistoryAppState} */
    const priceHistoryAppState = this.props.state
    const chartState = priceHistoryAppState.chartState
    const filteredQuoteList = priceHistoryAppState.quoteList.getItems().filter((quote) => {
      return quote.chartState.quote === chartState.quote
    })
    const quote = filteredQuoteList[0]
    const dateTime = chartState.dateTime
    const scale = chartState.scale
    return `${quote.code}_${dateTime}_${scale}`.replaceAll(' ', '_')
  }

  /**
   * @param {JSX.Element} form
   */
  showModal(form: JSX.Element) {
    this.setState({
      modalOpen: true,
      modalContent: form
    })
  }

  closeModal() {
    this.setState({modalOpen: false})
  }

  refreshState() {
    this.setState({})
  }
}

export const ChartStateBookmarksComponent = connector(ChartStateBookmarks)