import ChartState from '../chart/chart_state'
import SelectedTools from '../tool/selected_tools'

export default class Bookmark {

  constructor(public name: string, public chartState: ChartState, public selectedTools: SelectedTools) {}
}