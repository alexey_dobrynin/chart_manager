import React from 'react'
import BookmarkOperation from './bookmark_operation'
import ObjectHelper from '../helper/object_helper'
import Bookmark from './bookmark'
import {ChartStateBookmarksInterface} from './chart_state_bookmarks'
import {connector, PriceHistoryAppConnectorProps} from '../price_history_app/connector'

type BookmarkEditFormProps = PriceHistoryAppConnectorProps & {
    name: string
    mode: string
    chartStateBookmarks: ChartStateBookmarksInterface
}

interface BookmarkEditFormState {
    name: string
}

class BookmarkEditForm extends React.Component<BookmarkEditFormProps, BookmarkEditFormState> {

    constructor(props: BookmarkEditFormProps) {
        super(props)
        this.state  = {
            name: props.name
        }
    }

    render() {
        switch (this.props.mode) {
            case BookmarkOperation.mode.create:
                return this.renderCreateForm()
            case BookmarkOperation.mode.edit:
                return this.renderEditForm()
            case BookmarkOperation.mode.rename:
                return this.renderRenameForm()
        }
    }

    renderBookmarkName() {
        return <input
            name="bookmark_name"
            type="text"
            value={this.state.name}
            size={80}
            onChange={ this.changeBookmarkName() } />
    }

    renderCreateForm() {
        return <form>
            <h3>Создание закладки состояния графика</h3>
            { this.renderBookmarkName() }
            <hr/>
            <button type="button" onClick={ this.clickCreateBookmark() }>Создать</button>
        </form>
    }

    renderEditForm() {
        return <form>
            <h3>Перезапись закладки состояния графика</h3>
            { this.renderBookmarkName() }
            <hr/>
            <button type="button" onClick={ this.clickEditBookmark() }>Перезаписать</button>
        </form>
    }

    renderRenameForm() {
        return <form>
            <h3>Переименование закладки состояния графика</h3>
            { this.renderBookmarkName() }
            <hr/>
            <button type="button" onClick={ this.clickRenameBookmark() }>Переименование</button>
        </form>
    }

    changeBookmarkName() {
        return (event: React.ChangeEvent<HTMLInputElement>) => {
            this.setState({
                name: event.target.value
            })
        }
    }

    clickRenameBookmark() {
        return () => {
            this.props.renameChartBookmark(this.props.name, this.state.name)
            this.props.chartStateBookmarks.state.selectedBookmark = this.state.name
            this.props.chartStateBookmarks.showBookmarkList()()
        }
    }

    clickEditBookmark() {
        return () => {
            this.props.deleteChartBookmark(this.props.name)
            this.createBookmark()
            this.props.chartStateBookmarks.state.selectedBookmark = this.state.name
            this.props.chartStateBookmarks.showBookmarkList()()
        }
    }

    clickCreateBookmark() {
        return () => {
            this.createBookmark()
            this.props.chartStateBookmarks.closeModal()
            this.props.chartStateBookmarks.refreshState()
        }
    }

    createBookmark() {
        const chartState = ObjectHelper.clone(this.props.state.chartState)
        const selectedTools = ObjectHelper.clone(this.props.state.selectedTools)
        const bookmark = new Bookmark(this.state.name, chartState, selectedTools)
        this.props.createChartBookmark(bookmark)
    }
}

export const BookmarkEditFormComponent = connector(BookmarkEditForm)