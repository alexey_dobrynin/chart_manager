export default class BookmarkOperation {
  static mode = Object.freeze({
    create: 'create',
    edit: 'edit',
    rename: 'rename'
  })
}