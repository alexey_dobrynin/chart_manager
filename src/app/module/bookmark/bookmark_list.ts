import ObjectStructure from '../object/object_structure'
import Bookmark from './bookmark'
import ChartState from '../chart/chart_state'
import initFromObject from '../object/init_from_object'
import SelectedTools from '../tool/selected_tools'

export default class BookmarkList extends ObjectStructure {
  /**
   * @param {String} bookmarkObject.name
   * @param {Object} bookmarkObject.chartState
   * @param {Object} bookmarkObject.selectedTools
   * @return {Bookmark}
   */
  objectToItem(bookmarkObject: {name: string, chartState: Record<string, any>, selectedTools: Record<string, any>}) {
    const chartState = new ChartState()
    const selectedTools = new SelectedTools({})
    initFromObject(chartState, bookmarkObject.chartState)
    initFromObject(selectedTools, bookmarkObject.selectedTools)
    return new Bookmark(bookmarkObject.name, chartState, selectedTools)
  }

  /**
   * @return {Object<String, Bookmark>}
   */
  getItems() {
    return super.getItems()
  }
}