import PriceHistoryAppState from '../price_history_app/price_history_app_state'
import Header from '../form/header'
import ColorParameter from '../form/parameter/color_parameter'
import AbstractFormElement from '../form/abstract_form_element'
import TimeIntervalTool from './time_interval_tool'

export default abstract class OneColorTool extends TimeIntervalTool{

    color: string = '#000000'

    protected constructor(name?: string, code?: string) {
        super(
            name === undefined ? 'Одноцветный инструмент' : name,
            code === undefined ? OneColorTool.name : code
        )
    }

    /**
     * @return {String} код цвета
     */
    getColor() {
        return this.color
    }

    /**
     * @param {PriceHistoryAppState} state
     * @return {AbstractFormElement[]} список элементов формы добавления
     */
    getFullFormElements(state: PriceHistoryAppState): AbstractFormElement[] {
        const fullFormElements = super.getFullFormElements(state)
        fullFormElements.push(
            new Header({
                text: 'Цвет',
                size: 5
            }),
            new ColorParameter({
                name: 'color',
                description: 'цвет',
                value: this.color,
                onChange: (event) => {
                    this.color = event.target.value
                }
            })
        )
        return fullFormElements
    }
}