import BarPointTypeTool from './bar_point_type_tool'
import MultiplyTool from './multiply_tool'
import MovingAverageTool from './moving_average_tool'
import ObjectList from '../object/object_list'
import TradeStrategyTool from '../tool/trade_strategy_tool'
import MovingAverageDiffTool from "./moving_average_diff_tool";

/** Набор инструментов */
export default class ToolList extends ObjectList {

  barPointTypeTool: BarPointTypeTool

  multiplyTool: MultiplyTool

  movingAverageTool: MovingAverageTool

  movingAverageDiffTool: MovingAverageDiffTool

  tradeStrategyTool: TradeStrategyTool

  constructor() {
    super()
    this.barPointTypeTool = new BarPointTypeTool()
    this.multiplyTool = new MultiplyTool()
    this.movingAverageTool = new MovingAverageTool()
    this.movingAverageDiffTool = new MovingAverageDiffTool()
    this.tradeStrategyTool = new TradeStrategyTool()
  }

  /**
   * @return {AbstractTool[]}
   */
  getItems() {
    return super.getItems()
  }

  /**
   * @return {Object<String,AbstractTool>}
   */
  getAsObject() {
    return super.getAsObject()
  }

}