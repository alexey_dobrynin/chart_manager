import BarPointTypeTool from './bar_point_type_tool'
import MovingAverageFilterType from '../filter/moving_average_filter_type'
import Header from '../form/header'
import IntParameter from '../form/parameter/int_parameter'
import SelectParameter from '../form/parameter/select_parameter'
import {Scale} from '../scale/scale'
import TickPoint from '../point/tick_point'
import PeriodIntervalOptions from '../chart/period_interval_options'
import PriceHistoryAppState from '../price_history_app/price_history_app_state'
import Option from '../option/option'
import AbstractLine from '../line/abstract_line'
import AbstractPoint from '../point/abstract_point'
import {SeriesLineDataPoint} from '../series/data'

/** Инструмент скользящей средней */
export default class MovingAverageTool extends BarPointTypeTool {

    period: string = '1'
    periodInterval: string = '1'
    filter: string = 'MovingAverageFilter'

    constructor(name?: string, code?: string) {
        super(
            name === undefined ? 'Скользящая средняя' : name,
            code === undefined ? MovingAverageTool.name : code
        )
    }

    initParameters(state: PriceHistoryAppState) {
        super.initParameters(state)
        const periodIntervalOption = state.chartState.getPeriodIntervalOption()
        this.period = this.getPeriodDefaultValue(state, periodIntervalOption)
        this.periodInterval = periodIntervalOption.getValue()
        this.filter = MovingAverageFilterType.getTypeList().MovingAverageSimpleFilter
        this.fromDatetime = this.getFromDateTimeDefaultValue(state)
        this.toDatetime = this.getToDateTimeDefaultValue(state)
    }

    getFullFormElements(state: PriceHistoryAppState) {
        const fullFormElements = super.getFullFormElements(state)
        fullFormElements.push(
            new Header({ text: 'Период', size: 5}),
            new IntParameter({
                name: 'period',
                description: 'Период',
                min: 1,
                step: 1,
                value: this.period,
                onChange: (event) => {
                    this.period = event.target.value
                }
            }),
            new SelectParameter({
                name: 'periodInterval',
                description: 'Интервал',
                options: PeriodIntervalOptions.getOptions(),
                value: this.periodInterval,
                onChange: (event) => {
                    this.periodInterval = event.target.value
                }
            }),
            new Header({ text: 'Тип', size: 5}),
            new SelectParameter({
                name: 'filter',
                description: 'Тип',
                options: MovingAverageFilterType.getOptions(),
                value: this.filter,
                onChange: (event) => {
                    this.filter = event.target.value
                }
            })
        )
        return fullFormElements
    }

    /**
     * @param {PriceHistoryAppState} state
     * @param {Option} periodIntervalOption
     * @return {String} значение интервала по умолчанию
     */
    getPeriodDefaultValue(state: PriceHistoryAppState, periodIntervalOption: Option) {
        const seconds = Scale.seconds[state.chartState.scale]
        const intervalSeconds = parseInt(periodIntervalOption.getValue())
        return String(Math.floor(seconds/intervalSeconds))
    }

    /**
     * @param {PriceHistoryAppState} state
     * @return {String} с даты
     */
    getFromDateTimeDefaultValue(state: PriceHistoryAppState) {
        return state.chartState.dateTime.split(' ').join('T')
    }

    /**
     * @param {PriceHistoryAppState} state
     * @return {String} по дату
     */
    getToDateTimeDefaultValue(state: PriceHistoryAppState) {
        return state.chartState.getNextDatetime().split(' ').join('T')
    }

    /**
     * @return {MovingAverageFilter}
     */
    createMovingAverageFilter()
    {
        const filterObjectList = MovingAverageFilterType.getFilterObjectList()
        /** @type MovingAverageFilter */
        const filter = filterObjectList[this.filter]
        return filter.clone(parseInt(this.period), parseInt(this.periodInterval))
    }

    getLineData(line: AbstractLine): SeriesLineDataPoint[] {
        const pointList = line.getPointList().map(
            /**
             * @param {BarPoint} barPoint
             * @return {TickPoint}
             */
            (barPoint: AbstractPoint) => {
                return new TickPoint(
                    barPoint.getValueByType(this.barPoint).toString(),
                    barPoint.getDateTime()
                )
            }
        )

        return this.filterMovingAverage(pointList)
    }

    protected filterMovingAverage(pointList: TickPoint[]) {
        const fromDateUnixTime = (new Date(this.fromDatetime)).getTime()
        const toDateUnixTime = (new Date(this.toDatetime)).getTime()
        return this
            .createMovingAverageFilter()
            .filter(pointList)
            .filter((point: AbstractPoint) => {
                return fromDateUnixTime <= point.getUnixTime() && point.getUnixTime() <= toDateUnixTime
            }).map((point: AbstractPoint) => {
                return [
                    point.getDateTime(),
                    (<TickPoint>point).getPrice()
                ]
            })
    }
}