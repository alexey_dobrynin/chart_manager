import Header from '../form/header'
import DateTimeParameter from '../form/parameter/datetime_parameter'
import PriceHistoryAppState from '../price_history_app/price_history_app_state'
import AbstractFormElement from '../form/abstract_form_element'
import AbstractTool from './abstract_tool'
import ChartNumber from '../chart/chart_number'
import MultiselectParameter from '../form/parameter/multiselect_parameter'

export default abstract class TimeIntervalTool extends AbstractTool {

    static chartNumberDefault = '[1]'

    fromDatetime: string = '0'
    toDatetime: string = '0'
    chartNumber: string = TimeIntervalTool.chartNumberDefault

    protected constructor(name?: string, code?: string) {
        super(
            name === undefined ? 'Временной интервал' : name,
            code === undefined ? TimeIntervalTool.name : code
        )
    }

    initParameters(state: PriceHistoryAppState) {
        this.fromDatetime = this.getFromDateTimeDefaultValue(state)
        this.toDatetime = this.getToDateTimeDefaultValue(state)
        this.chartNumber = '[1]'
    }

    /**
     * @param {PriceHistoryAppState} state
     * @return {String} с даты
     */
    getFromDateTimeDefaultValue(state: PriceHistoryAppState): string {
        return state.chartState.fromDateTime.split(' ').join('T')
    }

    /**
     * @param {PriceHistoryAppState} state
     * @return {String} по дату
     */
    getToDateTimeDefaultValue(state: PriceHistoryAppState): string {
        return state.chartState.toDateTime.split(' ').join('T')
    }

    getFullFormElements(state: PriceHistoryAppState): AbstractFormElement[] {
        const fullFormElements = super.getFullFormElements(state)
        fullFormElements.push(
            new Header({text: 'С даты', size: 5}),
            new DateTimeParameter({
                name: 'fromDatetime',
                description: 'с даты',
                value: this.fromDatetime as string,
                onChange: (event) => {
                    this.fromDatetime = event.target.value
                }
            }),
            new Header({
                text: 'По дату',
                size: 5
            }),
            new DateTimeParameter({
                name: 'toDatetime',
                description: 'по дату',
                value: this.toDatetime as string,
                onChange: (event) => {
                    this.toDatetime = event.target.value
                }
            }),
            new Header({
                text: 'Номер графика',
                size: 5
            }),
            new MultiselectParameter({
                name: 'chartNumber',
                description: 'номер графика',
                options: ChartNumber.getOptions(),
                value: this.chartNumber,
                onChange: (event) => {
                    const selectedValues: string[] = [...<any>event.target.options]
                        .filter(option => option.selected)
                        .map(x => x.value)
                    this.chartNumber = selectedValues.length > 0
                        ? JSON.stringify(selectedValues)
                        : TimeIntervalTool.chartNumberDefault
                }
            })

        )
        return fullFormElements
    }
}