import Header from '../form/header'
import SelectParameter from '../form/parameter/select_parameter'
import DateTimeHelper from '../helper/date_time_helper'
import TargetOptions from '../option/target_options'
import BarPoint from '../point/bar_point'
import PriceHistoryAppState from '../price_history_app/price_history_app_state'
import AbstractLine from '../line/abstract_line'
import AbstractFormElement from '../form/abstract_form_element'
import OneColorTool from './one_color_tool'
import {SeriesLineDataPoint} from '../series/data'

export default class BarPointTypeTool extends OneColorTool {

    barPoint: string = BarPoint.type.closeValue

    constructor(name?: string, code?: string) {
        super(
            name === undefined ? 'Значение' : name,
            code === undefined ? BarPointTypeTool.name : code
        )
    }

    initParameters(state: PriceHistoryAppState) {
        super.initParameters(state)
        this.barPoint = BarPoint.type.closeValue
    }

    getFullFormElements(state: PriceHistoryAppState): AbstractFormElement[] {
        const fullFormElements = super.getFullFormElements(state)
        fullFormElements.push(
            new Header({
                text: 'Точка на столбце',
                size: 5
            }),
            new SelectParameter({
                name: 'barPoint',
                value: this.barPoint,
                description: 'Точка на столбце',
                onChange: (event) => {
                    this.barPoint = event.target.value
                },
                options: TargetOptions.getOptions()
            })
        )
        return fullFormElements
    }

    getLineData(line: AbstractLine): SeriesLineDataPoint[] {
        const lineData: [string, number][] = []
        const fromDateUnixTime = (new Date(this.fromDatetime)).getTime()
        const toDateUnixTime = (new Date(this.toDatetime)).getTime()

        for (const point of line.getPointList()) {
            const pointDate = point.getDate()
            const pointDateUnixTime = pointDate.getTime()

            if(fromDateUnixTime <= pointDateUnixTime && pointDateUnixTime <= toDateUnixTime){
                lineData.push([
                    DateTimeHelper.unixToDateTimeMillisecondsFormat(pointDateUnixTime),
                    point.getValueByType(this.barPoint)
                ])
            }
        }

        return lineData
    }
}