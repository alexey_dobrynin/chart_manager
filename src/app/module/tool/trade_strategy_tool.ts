import Header from '../form/header'
import SelectParameter from '../form/parameter/select_parameter'
import BarTicksLine from '../line/bar_ticks_line'
import TickLine from '../line/tick_line'
import Option from '../option/option'
import TradeStrategyList from '../trade/trade_strategy_list'
import TradeStrategy from '../trade/strategy/trade_strategy'
import PriceHistoryAppState from '../price_history_app/price_history_app_state'
import AbstractLine from '../line/abstract_line'
import BarTicksPoint from '../point/bar_ticks_point'
import TimeIntervalTool from './time_interval_tool'
import ColorParameter from '../form/parameter/color_parameter'
import IntParameter from '../form/parameter/int_parameter'
import React from 'react'
import {TradeActionType} from '../trade/trade_action_type'
import {SeriesTradeDataPoint} from '../series/data'
import DateTimeHelper from '../helper/date_time_helper'

/** Инструмент отображения торговой операции для торговой стратегии */
export default class TradeStrategyTool extends TimeIntervalTool {

    static chartNumberDefault = '[1,2]'

    static colorProfitDefault = '#88ff88'

    static colorLossesDefault = '#ff8888'

    static strokeColorDefault = 'black'

    tradeStrategy: string = ''

    tradeStrategyParams: string = ''

    brokersCommission: number = 0.0003

    colorProfit: string = TradeStrategyTool.colorProfitDefault

    colorLosses: string = TradeStrategyTool.colorLossesDefault

    strokeColor: string = TradeStrategyTool.strokeColorDefault

    constructor(name?: string, code?: string) {
        super(
            name === undefined ? 'Стратегия торговли' : name,
            code === undefined ? TradeStrategyTool.name : code
        )
    }

    getColorProfit(): string {
        return this.colorProfit
    }

    getColorLosses(): string {
        return this.colorLosses
    }

    getStrokeColor(): string {
        return this.strokeColor
    }

    initParameters(state: PriceHistoryAppState) {
        super.initParameters(state)
        this.chartNumber = TradeStrategyTool.chartNumberDefault
        this.tradeStrategyParams = ''
        this.syncTradeStrategyWithParams()
        this.colorProfit = TradeStrategyTool.colorProfitDefault
        this.colorLosses = TradeStrategyTool.colorLossesDefault
        this.strokeColor = TradeStrategyTool.strokeColorDefault
    }

    syncTradeStrategyWithParams(){
        if(this.tradeStrategyParams.length > 0){
            const tradeStrategyParams = JSON.parse(this.tradeStrategyParams)
            const strategy = this.createTradeStrategyList().getTradeStrategyByParamsClassName(tradeStrategyParams.code)
            this.tradeStrategy = strategy instanceof TradeStrategy
              ? strategy.constructor.name
              : TradeStrategy.name
        } else {
            this.tradeStrategy = TradeStrategy.name
        }
    }

    /**
     * @param {PriceHistoryAppState} state
     * @return {Option[]} список параметров стратегий торговли
     */
    getStrategyParamsListOptions(state: PriceHistoryAppState) {
        return Object.keys(state.selectedTradeStrategy.getItems()).map((tradeStrategyParams) => {
            return new Option(tradeStrategyParams)
        })
    }

    getFullFormElements(state: PriceHistoryAppState) {
        const fullFormElements = super.getFullFormElements(state)
        fullFormElements.push(
            new Header({ text: 'Торговая стратегия', size: 5}),
            new SelectParameter({
                name: 'tradeStrategyParams',
                description: 'Торговая стратегия',
                options: this.getStrategyParamsListOptions(state),
                value: this.tradeStrategyParams,
                onChange: (event) => {
                    this.tradeStrategyParams = event.target.value
                    this.syncTradeStrategyWithParams()
                }
            }),
            new Header({ text: 'Комисся брокера', size: 5}),
            new IntParameter({
                name: 'brokersCommission',
                description: 'Комисся брокера',
                min: 0.00001,
                step: 0.00001,
                value: this.brokersCommission.toString(),
                onChange: (event: React.ChangeEvent<HTMLInputElement>) => {
                    this.brokersCommission = parseFloat(event.target.value)
                }
            }),
            new Header({
                text: 'Цвет прибыли',
                size: 5
            }),
            new ColorParameter({
                name: 'colorProfit',
                description: 'цвет прибыли',
                value: this.colorProfit,
                onChange: (event) => {
                    this.colorProfit = event.target.value
                }
            }),
            new Header({
                text: 'Цвет убытков',
                size: 5
            }),
            new ColorParameter({
                name: 'colorLosses',
                description: 'цвет убытков',
                value: this.colorLosses,
                onChange: (event) => {
                    this.colorLosses = event.target.value
                }
            }),
            new Header({
                text: 'Цвет контура',
                size: 5
            }),
            new ColorParameter({
                name: 'strokeColor',
                description: 'цвет контура',
                value: this.strokeColor,
                onChange: (event) => {
                    this.strokeColor = event.target.value
                }
            })
        )
        return fullFormElements
    }

    getLineData(line: AbstractLine) {
        let lineData: SeriesTradeDataPoint[] = []
        let lineDataItem: any[] = []
        const lineDataItemMaxLength = 5
        let totalProfit = 0

        let barTickPointList: BarTicksPoint[] = []
        if(line instanceof BarTicksLine) {
            barTickPointList = line.getPointList()
        } else if (line instanceof TickLine){
            barTickPointList = line.getBarTicksList()
        }

        const tradeStrategyList = this.createTradeStrategyList()
        const tradeStrategy = tradeStrategyList.getTradeStrategyByClassName(this.tradeStrategy)

        if(tradeStrategy instanceof TradeStrategy){
            tradeStrategy.initTradeStrategy(this.tradeStrategyParams)
            const actionList = tradeStrategy.trade(barTickPointList).getActionList()
            for (const tradeAction of actionList) {
                if(tradeAction.getActionType() === TradeActionType.Buy){
                    lineDataItem.push(
                        DateTimeHelper.dateTimeToDateTimeFormat(tradeAction.getDateTime()),
                        tradeAction.getPrice()
                    )
                } else if (tradeAction.getActionType() === TradeActionType.Sell){
                    lineDataItem.push(
                        DateTimeHelper.dateTimeToDateTimeFormat(tradeAction.getDateTime()),
                        tradeAction.getPrice()
                    )
                    totalProfit += this.getProfit(lineDataItem[1], lineDataItem[3])
                    lineDataItem.push(totalProfit)
                    if(lineDataItem.length === lineDataItemMaxLength){
                        lineData.push(<SeriesTradeDataPoint>lineDataItem)
                    }
                    lineDataItem = []
                }
            }

        }

        return lineData
    }

    getProfit(buyPrice: number, sellPrice: number) {
        const commissionBuy = buyPrice * this.brokersCommission
        const commissionSell = sellPrice * this.brokersCommission
        return (sellPrice - buyPrice) - (commissionBuy + commissionSell)
    }

    /**
     * @return {TradeStrategyList}
     */
    createTradeStrategyList() {
        return new TradeStrategyList()
    }
}