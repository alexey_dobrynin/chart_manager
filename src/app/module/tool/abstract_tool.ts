/** Базовый класс инструментов */
import Button from '../form/button'
import ObjectHelper from '../helper/object_helper'
import PriceHistoryAppState from '../price_history_app/price_history_app_state'
import AbstractFormElement from '../form/abstract_form_element'
import {ActiveToolListInterface} from '../price_history_app/active_tool_list'
import AbstractLine from '../line/abstract_line'
import HorizontalRule from '../form/horizontal_rule'
import {ChartType, SeriesData, SeriesItem} from '../series/series'

export default abstract class AbstractTool {

    [key: string]: any

    name: string
    code: string

    protected constructor(name: string, code: string) {
        this.name = name
        this.code = code
    }

    /**
     * @return {String} название инструмента
     */
    getName(): string {
        return this.name
    }

    /**
     * @return {String} код инструмента
     */
    getCode(): string {
        return this.code
    }

    /**
     * Инициализация параметров
     * @param {PriceHistoryAppState} state */
    initParameters(state: PriceHistoryAppState) {

    }

    /**
     * @param {PriceHistoryAppState} state
     * @return {AbstractFormElement[]} список элементов формы
     */
    getFormElements(state: PriceHistoryAppState): AbstractFormElement[] {
        return [
        ]
    }

    /**
     * @param {PriceHistoryAppState} state
     * @return {AbstractFormElement[]} список элементов формы добавления
     */
    getFullFormElements(state: PriceHistoryAppState): AbstractFormElement[] {
        return this.getFormElements(state)
    }

    /**
     * @param {ActiveToolList} activeToolList
     * @return {AbstractFormElement[]} список элементов формы добавления
     */
    getAddFormElements(activeToolList: ActiveToolListInterface): AbstractFormElement[] {
        const state = activeToolList.props.state
        this.initParameters(state)
        const addFormElements = this.getFullFormElements(state)
        addFormElements.push(
            new HorizontalRule(),
            new Button({
                text: 'Добавить',
                onClick: (() => {
                    activeToolList.addTool(this.clone())
                    activeToolList.closeModal()
                    activeToolList.props.refreshApp()
                })
            })
        )
        return addFormElements
    }

    /**
     * @param {ActiveToolList} activeToolList
     * @return {AbstractFormElement[]} список элементов формы добавления
     */
    getEditFormElements(activeToolList: ActiveToolListInterface) {
        const state = activeToolList.props.state
        const editFormElements = this.getFullFormElements(state)
        editFormElements.push(
            new HorizontalRule(),
            new Button({
                text: 'Изменить',
                onClick: (() => {
                    activeToolList.editTool(this)
                    activeToolList.closeModal()
                    activeToolList.props.refreshApp()
                })
            })
        )
        return editFormElements
    }

    /**
     * @param {String} jsonString
     */
    initFromJson(jsonString: string) {
        const toolParams = JSON.parse(jsonString)
        this.initFromObject(toolParams)
    }

    /**
     * @param {Object} object
     */
    initFromObject(object: Record<string, any>) {
        Object.getOwnPropertyNames(this).forEach((propertyName) => {
            this[propertyName] = object[propertyName]
        })
    }

    /**
     * @return {String} json-строка
     */
    toJson(){
        const object: Record<string, string> = {}
        Object.getOwnPropertyNames(this).forEach((propertyName) => {
            object[propertyName] = this[propertyName]
        })
        return JSON.stringify(object)
    }

    abstract getLineData(line: AbstractLine): SeriesData

    clone(): AbstractTool {
        return <AbstractTool>ObjectHelper.clone(this)
    }
}