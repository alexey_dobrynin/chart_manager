import MovingAverageTool from './moving_average_tool'
import AbstractLine from '../line/abstract_line'
import {SeriesLineDataPoint} from '../series/data'
import AbstractPoint from '../point/abstract_point'
import TickPoint from '../point/tick_point'

/** Инструмент скользящей средней изменения цены */
export default class MovingAverageDiffTool extends MovingAverageTool {

    constructor(name?: string, code?: string) {
        super(
            name === undefined ? 'Скользящая средняя изменения цены' : name,
            code === undefined ? MovingAverageDiffTool.name : code
        )
        this.chartNumber = '[2]'
    }

    getLineData(line: AbstractLine): SeriesLineDataPoint[] {
        const sourcePointList = line.getPointList()
        let previousPointPrice = sourcePointList.length > 0 ?
            sourcePointList[0].getValueByType(this.barPoint)
            : 0
        const pointList = line.getPointList().map(
            /**
             * @param {BarPoint} barPoint
             * @return {TickPoint}
             */
            (barPoint: AbstractPoint) => {

                const currentPointPrice = barPoint.getValueByType(this.barPoint)
                const priceDiff = currentPointPrice - previousPointPrice
                const tickPoint = new TickPoint(
                    priceDiff.toString(),
                    barPoint.getDateTime()
                )
                previousPointPrice = currentPointPrice
                return tickPoint;
            }
        )

        return  this.filterMovingAverage(pointList)
    }
}