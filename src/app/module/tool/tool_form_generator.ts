import {ActiveToolListInterface} from '../price_history_app/active_tool_list'
import FormElementGenerator from '../form/form_element_generator'
import AbstractTool from '../tool/abstract_tool'

/** Генератор формы параметров инструментов */
export default class ToolFormGenerator {

    static getAddFormElements(tool: AbstractTool, activeToolList: ActiveToolListInterface){
        return tool.getAddFormElements(activeToolList).map(((formElement) => {
            const getTemplateFunction = FormElementGenerator.getTypeToGetTemplateFunction(formElement)
            return getTemplateFunction(formElement)
        }))
    }

    static getEditFormElements(tool: AbstractTool, activeToolList: ActiveToolListInterface){
        return tool.getEditFormElements(activeToolList).map(((formElement) => {
            const getTemplateFunction = FormElementGenerator.getTypeToGetTemplateFunction(formElement)
            return getTemplateFunction(formElement)
        }))
    }
}