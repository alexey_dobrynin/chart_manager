import ObjectKeyStructure from '../object/object_key_structure'
import ToolList from '../tool/tool_list'
import {lcfirst} from '../helper/php_functions'
import AbstractTool from './abstract_tool'

export default class SelectedTools extends ObjectKeyStructure {

  /**
   * @return {ToolList}
   */
  getToolList(): ToolList {
    return new ToolList()
  }

  /**
   * @param {String} toolObject.code
   * @return {AbstractTool}
   */
  objectToItem(toolObject: {code: string}): AbstractTool {
    const toolName = lcfirst(toolObject.code)
    const toolList = this.getToolList()
    const tool = toolList[toolName].clone()
    tool.initFromObject(toolObject)
    return tool
  }

  /**
   * @return {Object<String, AbstractTool>}
   */
  getItems(): Record<string, AbstractTool> {
    return super.getItems()
  }
}