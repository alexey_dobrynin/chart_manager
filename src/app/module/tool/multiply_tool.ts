import BarPointTypeTool from './bar_point_type_tool'
import Header from '../form/header'
import IntParameter from '../form/parameter/int_parameter'
import MathHelper from '../helper/math_helper'
import PriceHistoryAppState from '../price_history_app/price_history_app_state'
import React from 'react'
import AbstractLine from '../line/abstract_line'
import {SeriesLineDataPoint} from "../series/data";

export default class MultiplyTool extends BarPointTypeTool {

    multiplicationFactor: number

    constructor() {
        super('Коэффициент умножения', MultiplyTool.name);
        this.multiplicationFactor = 1
    }

    initParameters(chartPanelApp: PriceHistoryAppState) {
        super.initParameters(chartPanelApp)
    }

    /**
     * @param {PriceHistoryAppState} state
     * @return {AbstractFormElement[]} список элементов формы
     */
    getFullFormElements(state: PriceHistoryAppState) {
        const fullFormElements = super.getFullFormElements(state)
        fullFormElements.push(
            new Header({ text: 'Коэффициент умножения', size: 5}),
            new IntParameter({
                name: 'multiplicationFactor',
                description: 'Коэффициент умножения',
                min: 0.00001,
                step: 0.00001,
                value: this.multiplicationFactor.toString(),
                onChange: (event: React.ChangeEvent<HTMLInputElement>) => {
                    this.multiplicationFactor = parseFloat(event.target.value)
                }
            }),
        )
        return fullFormElements
    }

    getLineData(line: AbstractLine): SeriesLineDataPoint[] {
        return  super
            .getLineData(line)
            .map((point) => {
                point[1] = MathHelper.round( point[1] * this.multiplicationFactor, -7)
                return point
            })
    }
}