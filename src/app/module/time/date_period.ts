/** Временной интевал */
export default class DatePeriod {

    begin: Date
    end: Date

    /**
     * @param {Date} begin
     * @param {Date} end
     */
    constructor(begin: Date, end: Date) {
        this.begin = begin
        this.end = end
    }

    /**
     * @return {Date}
     */
    getBegin(): Date {
        return this.begin
    }

    /**
     * @return {Date}
     */
    getEnd(): Date {
        return this.end
    }
}