import DateTimeHelper from '../helper/date_time_helper'

/** Временной диапазон */
export default class TimePeriod {

    begin: Date
    end: Date

    /**
     * @param {Date} begin
     * @param {Date} end
     */
    constructor(begin: Date, end: Date) {
        this.begin = begin
        this.end = end
    }

    /**
     * @param {Date} dateTime
     */
    changeDate(dateTime: Date) {
        const dateUnixTime = DateTimeHelper.dateTimeToDate(dateTime).getTime()
        const beginTime = this.getBegin().getTime() - DateTimeHelper.dateTimeToDate(this.getBegin()).getTime()
        const endTime = this.getEnd().getTime() - DateTimeHelper.dateTimeToDate(this.getEnd()).getTime()
        this.begin = new Date(dateUnixTime + beginTime)
        this.end = new Date(dateUnixTime + endTime)
    }

    /**
     * @return {Date}
     */
    getBegin(): Date {
        return this.begin
    }

    /**
     * @return {Date}
     */
    getEnd(): Date {
        return this.end
    }
}