import {AnyAction} from 'redux'
import Bookmark from '../bookmark/bookmark'

export default interface CreateChartBookmarkAction extends AnyAction
{
    bookmark: Bookmark
}