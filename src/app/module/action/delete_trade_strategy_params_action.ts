import {AnyAction} from 'redux'

export default interface DeleteTradeStrategyParamsAction extends AnyAction
{
    tradeStrategyParams: string
}