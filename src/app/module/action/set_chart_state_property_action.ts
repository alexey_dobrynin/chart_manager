import {AnyAction} from 'redux'

export default interface SetChartStatePropertyAction extends AnyAction
{
    quoteId: string
}