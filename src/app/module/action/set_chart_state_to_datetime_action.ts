import SetChartStatePropertyAction from './set_chart_state_property_action'

export default interface SetChartStateToDatetimeAction extends SetChartStatePropertyAction
{
    toDateTime: string
}