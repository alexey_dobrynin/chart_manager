import {AnyAction} from 'redux'

export default interface RenameChartBookmarkAction extends AnyAction
{
    oldName: string
    newName: string
}