import {AnyAction} from 'redux'
import AbstractTool from '../tool/abstract_tool'

export default interface AddActiveToolListAction extends AnyAction
{
    tool: AbstractTool
}