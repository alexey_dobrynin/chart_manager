import SetChartStatePropertyAction from './set_chart_state_property_action'

export default interface SetChartStateZoomXStartAction extends SetChartStatePropertyAction
{
    dataZoomXStart: string
}