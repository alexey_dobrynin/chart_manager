import {AnyAction} from 'redux'

export default interface DeleteChartBookmarkAction extends AnyAction
{
    name: string
}