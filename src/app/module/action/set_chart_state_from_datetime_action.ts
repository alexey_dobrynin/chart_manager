import SetChartStatePropertyAction from './set_chart_state_property_action'

export default interface SetChartStateFromDatetimeAction extends SetChartStatePropertyAction
{
    fromDateTime: string
}