import {AnyAction} from 'redux'

export default interface DeleteActiveToolListAction extends AnyAction
{
    toolJson: string
}