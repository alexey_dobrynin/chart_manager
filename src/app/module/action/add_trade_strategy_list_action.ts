import {AnyAction} from 'redux'
import AbstractTradeStrategyParams from '../trade/strategy/params/abstract_trade_strategy_params'

export default interface AddTradeStrategyListAction extends AnyAction
{
    tradeStrategyParams: AbstractTradeStrategyParams
}