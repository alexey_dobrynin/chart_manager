import {AnyAction} from 'redux';
import QuoteList from '../quote/quote_list';

export default interface SetQuoteListAction extends AnyAction{
    quoteList: QuoteList
}