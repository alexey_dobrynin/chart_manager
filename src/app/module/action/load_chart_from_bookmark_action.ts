import {AnyAction} from 'redux'

export default interface LoadChartFromBookmarkAction extends AnyAction
{
    bookmarkName: string
}