import DateTimeHelper from '../helper/date_time_helper'

export type ScaleKeys = 'tick' | '1 min' | '5 min' | '10 min' | '15 min' | '30 min' | '1 hour' | '1 day' | '1 week' | '1 month'

export class Scale {

  static code = Object.freeze({
    'tick': 'tick',
    '1 min': '1 min',
    '5 min': '5 min',
    '10 min': '10 min',
    '15 min': '15 min',
    '30 min': '30 min',
    '1 hour': '1 hour',
    '1 day': '1 day',
    '1 week': '1 week',
    '1 month': '1 month',
  })

  static dateTime: Record<string, string> = Object.freeze({
    'tick': `formatDateTime(time, '%Y-%m-%d %H:%M:%S')`,
    '1 min': `formatDateTime(time, '%Y-%m-%d %H:%M:00')`,
    '5 min': `floor(toUnixTimestamp(time, 'UTC') / (5*60)) * (5*60)`,
    '10 min': `floor(toUnixTimestamp(time, 'UTC') / (10*60)) * (10*60)`,
    '15 min': `floor(toUnixTimestamp(time, 'UTC') / (15*60)) * (15*60)`,
    '30 min': `floor(toUnixTimestamp(time, 'UTC') / (30*60)) * (30*60)`,
    '1 hour': `formatDateTime(time, '%Y-%m-%d %H:00:00')`,
    '1 day': `formatDateTime(time, '%Y-%m-%d 00:00:00')`,
    '1 week': `floor(toUnixTimestamp(time, 'UTC') / (24*60*60*7)) * (24*60*60*7)`,
    '1 month': `formatDateTime(time, '%Y-%m-01 00:00:00')`
  })

  static period: Record<string, number> = Object.freeze({
    'tick': 1000 * 60 * 60,
    '1 min': 1000 * 60 * 60 * 24,
    '5 min': 1000 * 60 * 60 * 24 * 5,
    '10 min': 1000 * 60 * 60 * 24 * 10,
    '15 min': 1000 * 60 * 60 * 24 * 15,
    '30 min': 1000 * 60 * 60 * 24 * 30,
    '1 hour': 1000 * 60 * 60 * 24 * 60,
    '1 day': 1000 * 60 * 60 * 24 * 366,
    '1 week': 1000 * 60 * 60 * 24 * 366 * 5,
    '1 month': 1000 * 60 * 60 * 24 * 366 * 20
  })

  static seconds: Record<string, number> = Object.freeze({
    'tick': 1,
    '1 min': 60,
    '5 min': 5 * 60,
    '10 min': 10 * 60,
    '15 min': 15 * 60,
    '30 min': 30 * 60,
    '1 hour': 60 * 60,
    '1 day': 60 * 60 * 24,
    '1 week': 60 * 60 * 24 * 7,
    '1 month': 60 * 60 * 24 * 30
  })

  static date: Record<string, (date: Date) => Date> = Object.freeze({
    'tick': (date: Date) => { return date },
    '1 min': Scale.getScaleToDate(60*1000),
    '5 min': Scale.getScaleToDate(5*60*1000),
    '10 min': Scale.getScaleToDate(10*60*1000),
    '15 min': Scale.getScaleToDate(15*60*1000),
    '30 min': Scale.getScaleToDate(30*60*1000),
    '1 hour': Scale.getScaleToDate(60*60*1000),
    '1 day': Scale.getScaleToDate(24*60*60*1000),
    '1 week': Scale.getScaleToDate(7*24*60*60*1000),
    '1 month':
      /** @var {Date} date */
        (date: Date): Date => {
        date = DateTimeHelper.clone(date)
        date.setHours(0,0,0,0)
        date.setDate(1)
        return date
      }
  })

  /**
   * @param {Number} scaleStepMilliSeconds число милисекунд в шаге
   */
  static getScaleToDate(scaleStepMilliSeconds: number): (date: Date) => Date {
    /** @var {Date} date */
    return (date: Date): Date => {
      return new Date(Math.floor(date.getTime() / scaleStepMilliSeconds ) * scaleStepMilliSeconds)
    }
  }
}