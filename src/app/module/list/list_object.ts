export default class ListObject {

  /** @type {*[]} */
  items: any[] = []

  /**
   * @param {Object[]} list
   */
  constructor(list: object[]) {
    this.initItemsFromObjectList(list)
  }

  /**
   * @param {Object[]} list
   */
  initItemsFromObjectList(list: object[])
  {
    this.items = list.map((item: object) => {
      return this.objectToItem(item)
    })
  }

  /**
   * @param {Object} object
   * @return {Object}
   */
  objectToItem(object: object): object
  {
    return object
  }
}