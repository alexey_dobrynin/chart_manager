import ChartGridRow from './chart_grid_row'

/** Сетка графиков */
export default class ChartGrid {
    constructor(
        private rows: ChartGridRow[]
    ) {
    }

    toObjectList(){
        return this.rows.map((row) => {
            return row.toObject()
        })
    }
}