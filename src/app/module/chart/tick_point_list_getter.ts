import {Scale} from '../scale/scale'
import DateTimeHelper from '../helper/date_time_helper'
import DailyTickDataCache from './daily_tick_data_cache'
import TickLine from '../line/tick_line'
import DatePeriod from '../time/date_period'
import ChartDataGetter from './chart_data_getter'
import TickPoint from "../point/tick_point";

/** Получатель списка точек линейного графика */
export default class TickPointListGetter {

  /**
   * @param {ChartDataGetter} chartDataGetter
   * @return {Promise<TickPoint[]>}
   */
  getTickLine(chartDataGetter: ChartDataGetter){
    const dateList = []
    const scale = chartDataGetter.chartState.scale
    const quoteId = chartDataGetter.chartState.quote
    const fromDateTime = new Date(chartDataGetter.chartState.dateTime)
    const toDateTime = new Date(fromDateTime.getTime() + Scale.period[scale])
    const datePeriodList = DateTimeHelper.getDatePeriodList(new DatePeriod(fromDateTime, toDateTime))

    for (const datePeriod of datePeriodList) {
      const date = DateTimeHelper.clone(datePeriod.getBegin())
      date.setHours(0)
      date.setMinutes(0)
      date.setSeconds(0)
      dateList.push(date)
      const fromDatetime = DateTimeHelper.dateTimeToDateTimeFormat(date)
      const toDate = DateTimeHelper.clone(date)
      toDate.setHours(23)
      toDate.setMinutes(59)
      toDate.setSeconds(59)
      const toDatetime = DateTimeHelper.dateTimeToDateTimeFormat(toDate)
      const dailyTickData = DailyTickDataCache.getDailyCache(quoteId, date)
      if(dailyTickData === null){
        chartDataGetter
          .getClickhouseRequest()
          .getTickData(quoteId, fromDatetime, toDatetime)
          .then(
            (dailyTickData: {data: {time: string, price: string}[]}) => {
              DailyTickDataCache.setDailyCache(quoteId, date, dailyTickData.data)
            }
          )
      }
    }
    return this.waitTickLine(quoteId, dateList, fromDateTime, toDateTime)
  }

  /**
   * @param {String} quoteId идентификатор котировки
   * @param {Date[]} dateList список дат на которые необходимо получить тиковые данные
   * @param {Date} fromDateTime дата и время с которого необходимо получить тиковые данные
   * @param {Date} toDateTime дата и время до которого необходимо получить тиковые данные
   * @return {Promise<TickPoint[]>}
   */
  waitTickLine(quoteId: string, dateList: Date[], fromDateTime: Date, toDateTime: Date){
    return new Promise<TickPoint[]>((resolve) => {
      const intervalId = setInterval(() => {
        if(DailyTickDataCache.checkCacheExist(quoteId, dateList)){
          clearInterval(intervalId)
          let pointList: TickPoint[] = []
          for (const date of dateList) {
            const dailyTickData = <{price: string, time: string}[]>DailyTickDataCache.getDailyCache(quoteId, date)
            const tickLine = new TickLine(dailyTickData)
            pointList = pointList.concat(tickLine.getPointList())
          }
          while ( pointList.length > 0 ) {
            const point = pointList[0]
            if(point.getDate() < fromDateTime){
              pointList.shift()
            } else {
              break
            }
          }
          for (let pointPosition = pointList.length; pointPosition > 0 ; pointPosition--) {
            const point = pointList[pointPosition - 1]
            if(point.getDate() > toDateTime){
              pointList.pop()
            } else {
              break
            }
          }
          resolve(pointList)
        }
      }, 100)
    })
  }
}