import ChartGrid from './chart_grid'
import ChartGridRow from './chart_grid_row'

/** Схемы сеток графиков */
export default class ChartGridSchema {
    constructor(
        private chartGridList: ChartGrid[] = [
            new ChartGrid([
                new ChartGridRow('5%', '5%', '5%', '80%')
            ]),
            new ChartGrid([
                new ChartGridRow('5%', '5%', '5%', '60%'),
                new ChartGridRow('5%', '5%', '70%', '15%'),
            ]),
            new ChartGrid([
                new ChartGridRow('5%', '5%', '5%', '40%'),
                new ChartGridRow('5%', '5%', '50%', '15%'),
                new ChartGridRow('5%', '5%', '70%', '15%'),
            ])
        ]
    ) {
    }

    getChartGrid(chartNumbers: number){
        if(chartNumbers > this.chartGridList.length || chartNumbers < 1){
            return this.chartGridList[0].toObjectList()
        }
        return this.chartGridList[chartNumbers - 1].toObjectList()
    }
}