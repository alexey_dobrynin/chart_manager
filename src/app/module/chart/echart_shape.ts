import * as echarts from 'echarts'

export const profitRect = echarts.graphic.extendShape({
  shape: {
    buyTime: 0,
    sellTime: 0,
    buyPrice: 0,
    sellPrice: 0,
    buyCommission: 0,
    sellCommission: 0
  },
  buildPath: function (ctx, shape) {
    const commissionPrice = shape.buyCommission - Math.abs(
      shape.sellCommission - shape.sellPrice)

    ctx.moveTo(shape.buyTime, shape.buyPrice)
    ctx.lineTo(shape.sellTime, shape.buyPrice)
    ctx.closePath()

    ctx.moveTo(shape.buyTime, commissionPrice)
    ctx.lineTo(shape.buyTime, shape.sellPrice)
    ctx.lineTo(shape.sellTime, shape.sellPrice)
    ctx.lineTo(shape.sellTime, commissionPrice)
    ctx.closePath()
  }
})

export const loosesProfitRect = echarts.graphic.extendShape({
  shape: {
    buyTime: 0,
    sellTime: 0,
    buyPrice: 0,
    sellPrice: 0,
    buyCommission: 0,
    sellCommission: 0
  },
  buildPath: function (ctx, shape) {
    const profit = Math.abs(shape.buyPrice - shape.sellPrice)
    const commission = Math.abs(shape.buyCommission - shape.buyPrice) +
      Math.abs(shape.sellCommission - shape.sellPrice)

    const looses = commission - profit
    const commissionPrice = shape.buyPrice + looses

    ctx.moveTo(shape.buyTime, shape.sellPrice)
    ctx.lineTo(shape.sellTime, shape.sellPrice)
    ctx.closePath()

    ctx.moveTo(shape.buyTime, commissionPrice)
    ctx.lineTo(shape.buyTime, shape.buyPrice)
    ctx.lineTo(shape.sellTime, shape.buyPrice)
    ctx.lineTo(shape.sellTime, commissionPrice)
    ctx.closePath()
  }
})

export const loosesRect = echarts.graphic.extendShape({
  shape: {
    buyTime: 0,
    sellTime: 0,
    buyPrice: 0,
    sellPrice: 0,
    buyCommission: 0,
    sellCommission: 0
  },
  buildPath: function (ctx, shape) {
    const commission = Math.abs(shape.buyCommission - shape.buyPrice) +
      Math.abs(shape.sellCommission - shape.sellPrice)
    const commissionPrice = shape.sellPrice + commission
    ctx.moveTo(shape.buyTime, commissionPrice)
    ctx.lineTo(shape.buyTime, shape.buyPrice)
    ctx.lineTo(shape.sellTime, shape.buyPrice)
    ctx.lineTo(shape.sellTime, commissionPrice)
    ctx.closePath()

    ctx.moveTo(shape.buyTime, shape.sellPrice)
    ctx.lineTo(shape.sellTime, shape.sellPrice)
    ctx.closePath()
  }
})

export enum tradeRectType {
  profit = 'profitRect',
  loosesProfit = 'loosesProfitRect',
  looses = 'loosesRect'
}

export const initEChartTradeRectShapes = () => {
  echarts.graphic.registerShape(tradeRectType.profit, profitRect)
  echarts.graphic.registerShape(tradeRectType.loosesProfit, loosesProfitRect)
  echarts.graphic.registerShape(tradeRectType.looses, loosesRect)
}