import ClickhouseRequest from "../clickhouse/clickhouse_request";
import ChartState from "./chart_state";

export default class ChartDataGetter {

    clickhouseRequest: ClickhouseRequest

    chartState: ChartState

    constructor(clickhouseRequest: ClickhouseRequest, chartState: ChartState) {
        this.clickhouseRequest = clickhouseRequest
        this.chartState = chartState
    }

    getChartState() {
        return this.chartState;
    }

    getClickhouseRequest() {
        return this.clickhouseRequest;
    }

    /**
     * @return {Promise} промис запроса на получение данных
     */
    getChartData(){
        const chartState = this.getChartState()
        const scaleName = chartState.scale
        const quoteId = chartState.quote
        const fromDatetime = chartState.dateTime
        const toDatetime = chartState.getEndPeriodDateTime()
        return this.getClickhouseRequest().getChartData(scaleName, parseInt(quoteId), fromDatetime, toDatetime)
    }

    /**
     * @return {Promise} промис запроса на получение данных
     */
    getTickData() {
        const chartState = this.getChartState()
        const quoteId = chartState.quote
        const fromDatetime = chartState.dateTime
        const toDatetime = chartState.getEndPeriodDateTime()
        return this.getClickhouseRequest().getTickData(quoteId, fromDatetime, toDatetime);
    }
}