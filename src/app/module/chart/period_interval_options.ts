import Option from '../option/option'

export default class PeriodIntervalOptions {

    /**
     * @return {Option[]} список временных интервалов
     */
    static getOptions() {
        return [
            new Option('секунд', String(1)),
            new Option('минут', String(60)),
            new Option('часов', String(60*60)),
            new Option('дней', String(60*60*24)),
            new Option('недель', String(60*60*24*7)),
        ]
    }
}