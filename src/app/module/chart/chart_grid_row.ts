/** Строка сетки графиков */
export default class ChartGridRow {
    constructor(
        private left: string = '0%',
        private right: string = '0%',
        private top: string = '0%',
        private height: string = '0%'
    ) {
    }

    toObject(){
        return {
            left: this.left,
            right: this.right,
            top: this.top,
            height: this.height
        }
    }
}