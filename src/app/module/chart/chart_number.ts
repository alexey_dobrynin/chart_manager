import Option from '../option/option'

export default class ChartNumber {

    static getOptions() {
        return [
            new Option('1', '1'),
            new Option('2', '2'),
            new Option('3', '3'),
        ]
    }
}