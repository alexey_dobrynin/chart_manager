import PeriodIntervalOptions from './period_interval_options'
import {Scale, ScaleKeys} from '../scale/scale'
import DateTimeHelper from '../helper/date_time_helper'
import {isset} from '../helper/php_functions'

/** Состояние графика */
class ChartState {

  quote: string = ''

  fromDateTime: string = ''

  toDateTime: string = ''

  dateTime: string = ''

  scale: ScaleKeys = 'tick'

  dataZoomXStart: number = NaN

  dataZoomXEnd: number = NaN

  timeFilter: string = ''

  constructor() { }

  /**
   * @return {boolean} true - параметры для запроса проинициализированы
   */
  checkQueryParamsExists(){
    return isset(this.scale, this.dateTime, this.quote)
  }

  /**
   * @return {Option} временной интервал
   */
  getPeriodIntervalOption() {
    const seconds = Scale.seconds[this.scale]
    const periodIntervalOptions = PeriodIntervalOptions.getOptions()
    let previousOption = periodIntervalOptions[0]
    periodIntervalOptions.forEach((option) => {
      const intervalSeconds = parseInt(option.getValue())
      if(intervalSeconds > seconds){
        return previousOption
      }
      previousOption = option
    })
    return previousOption
  }

  /**
   * @return {String}
   */
  getLastDatetime() {
    const scaleIntervalStep = Scale.period[this.scale]
    const fromDateTimeUnix = (new Date(this.fromDateTime)).getTime()
    const toDateTimeUnix = (new Date(this.toDateTime)).getTime()
    const dateTimeUnix = Math.max(toDateTimeUnix - scaleIntervalStep, fromDateTimeUnix)
    return DateTimeHelper.unixToDateTimeFormat(dateTimeUnix)
  }

  /**
   * @return {String}
   */
  getPreviousDatetime() {
    const dateTimeUnix = (new Date(this.dateTime)).getTime()
    const fromDateTimeUnix = (new Date(this.fromDateTime)).getTime()
    const scaleIntervalStep = Scale.period[this.scale]
    const previousDateTimeUnix = Math.max(dateTimeUnix - scaleIntervalStep, fromDateTimeUnix)
    return DateTimeHelper.unixToDateTimeFormat(previousDateTimeUnix)
  }

  /**
   * @return {String}
   */
  getNextDatetime() {
    const dateTimeUnix = (new Date(this.dateTime)).getTime()
    const toDateTimeUnix = (new Date(this.toDateTime)).getTime()
    const scaleIntervalStep = Scale.period[this.scale]
    const nextDateTimeUnix = Math.min(
      dateTimeUnix + scaleIntervalStep, toDateTimeUnix - scaleIntervalStep)
    return DateTimeHelper.unixToDateTimeFormat(nextDateTimeUnix)
  }

  getDateOnScaleChange() {
    const dateTimeUnix = (new Date(this.dateTime)).getTime()
    const fromDateTimeUnix = (new Date(this.fromDateTime)).getTime()
    const toDateTimeUnix = (new Date(this.toDateTime)).getTime()
    const scaleIntervalStep = Scale.period[this.scale]
    const startTimeUnix = Math.max(
      fromDateTimeUnix,
      Math.min(
        dateTimeUnix + scaleIntervalStep,
        toDateTimeUnix - scaleIntervalStep
      ) - scaleIntervalStep
    )
    return DateTimeHelper.unixToDateTimeFormat(startTimeUnix)
  }

  /**
   * @return {String} дата окончания периода в формате YYYY-MM-DD hh:mm:ss
   */
  getEndPeriodDateTime(){
    const scaleIntervalStep = Scale.period[this.scale]
    const dateTimeUnix = (new Date(this.dateTime)).getTime()
    const toDateTimeUnix = (new Date(this.toDateTime)).getTime()
    const endPeriodDateTimeUnix = Math.min(dateTimeUnix + scaleIntervalStep, toDateTimeUnix)
    return DateTimeHelper.unixToDateTimeFormat(endPeriodDateTimeUnix)
  }
}

export default ChartState