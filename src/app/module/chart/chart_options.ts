/** Настройки построения графика */
import AbstractPoint from '../point/abstract_point'
import {isset} from '../helper/php_functions'
import AbstractLine from '../line/abstract_line'
import {PriceHistoryAppInterface} from '../price_history_app/price_history_app'
import {series, SeriesItem} from '../series/series'
import BarPointTypeTool from '../tool/bar_point_type_tool'
import LineSeriesItemFactory from '../series/factory/line_series_item_factory'
import AbstractTool from '../tool/abstract_tool'
import TradeStrategyTool from '../tool/trade_strategy_tool'
import TradeSeriesItemFactory from '../series/factory/trade_series_item_factory'
import {SeriesItemFactory} from '../series/factory/series_item_factory'
import TimeIntervalTool from '../tool/time_interval_tool'
import ChartGridSchema from './chart_grid_schema'
import ProfitSeriesItemFactory from '../series/factory/profit_series_item_factory'


export default class ChartOptions {

  /** @type {AbstractLine} */
  line: AbstractLine

  priceHistoryApp: PriceHistoryAppInterface

  constructor(line: AbstractLine, priceHistoryApp: PriceHistoryAppInterface) {
    this.line = line
    this.priceHistoryApp = priceHistoryApp
  }

  /**
   * @return {AbstractLine}
   */
  getLine() {
    return this.line
  }

  getLineFilteredByTime() {
    const line = this.getLine().clone()
    const chartState = this.priceHistoryApp.props.state.chartState
    if(isset(chartState) && isset(chartState.timeFilter)) {
      const timeFilter = (new AbstractPoint(chartState.timeFilter)).getDate()
      line.pointList = line.getPointList().filter((point: AbstractPoint) => {
        return point.getDate() <= timeFilter
      })
    }
    return line
  }

  getSeriesSettings(lineData: AbstractLine, tool: AbstractTool, index: number): SeriesItem[] {
    const seriesItemFactoryList: SeriesItemFactory[] = []
    const name = (index+1).toString()
    switch (true){
      case tool instanceof TradeStrategyTool:
        seriesItemFactoryList.push(new TradeSeriesItemFactory(), new ProfitSeriesItemFactory())
            break
      default:
        seriesItemFactoryList.push(new LineSeriesItemFactory())
            break
    }
    return seriesItemFactoryList
        .map(seriesItemFactory => seriesItemFactory.createSeriesItem(name, lineData, tool, index))
  }

  createChartGridSchema() {
    return new ChartGridSchema()
  }

  getGrid() {
    const activeToolList = this.priceHistoryApp.props.state.selectedTools
    const toolsSettings = activeToolList.getItems()
    let maxChartNumber = 1
    Object.keys(toolsSettings).forEach((key) => {
      const activeTool = toolsSettings[key]
      if(activeTool instanceof TimeIntervalTool){
        const chartNumbers: string[] = JSON.parse(activeTool.chartNumber)
        for (const chartNumber of chartNumbers) {
          maxChartNumber = maxChartNumber < parseInt(chartNumber)
              ? parseInt(chartNumber)
              : maxChartNumber
        }

      }
    })
    return this.createChartGridSchema().getChartGrid(maxChartNumber)
  }

  getSeriesList(lineData: AbstractLine): series {
    const seriesList: series = []
    const activeToolList = this.priceHistoryApp.props.state.selectedTools
    const toolsSettings = activeToolList.getItems()

    Object.keys(toolsSettings).forEach((key, index) => {
      const activeTool = toolsSettings[key]
      for (const series of this.getSeriesSettings(lineData, activeTool, index)) {
        seriesList.push(series)
      }
    })

    if(seriesList.length === 0){
      const defaultTool = new BarPointTypeTool()
      defaultTool.color = 'gray'
      defaultTool.initParameters(this.priceHistoryApp.props.state)

      seriesList.push(this.getSeriesSettings(lineData, defaultTool, 1)[0])
    }

    return seriesList
  }

  getChartOptions() {
    const gridList = this.getGrid()
    const lineData = this.getLineFilteredByTime()
    const pointList = lineData.getPointList()
    const firstPoint = pointList.length > 0 ? pointList[0].getDateTime() : 'dataMin'
    const lastPoint = pointList.length > 0 ? pointList[pointList.length - 1].getDateTime() : 'dataMax'
    const seriesList = this.getSeriesList(lineData)
    const yAxisList = gridList.map((grid, index) => {
      return { type: 'value', min: 'dataMin', max: 'dataMax', gridIndex: index}
    })
    const xAxisList = gridList.map((grid, index) => {
      return { type: 'time', min: firstPoint, max: lastPoint, splitNumber: 10, gridIndex: index}
    })
    const xAxisIndex = gridList.map((grid, index) => {
      return index
    })

    return {
      xAxis: xAxisList,
      yAxis: yAxisList,
      series: seriesList,
      legend: {},
      grid: gridList,
      tooltip: {
        trigger: 'axis',
        position: function (pt: [string]) {
          return [pt[0], '10%'];
        },
        formatter: function (params: [{data: [string, number], color: string}]){
          const dateTime = params[0].data[0]
          const priceList = params.map((param) => {
            const price = param.data[1]
            return `<b style="color: ${param.color}">&#9679;</b> ${price}`
          })
          const priceListString = priceList.join('<br/>')
          return `${dateTime}<br/>${priceListString}`
        }
      },
      dataZoom: [
        {
          type: 'inside',
          start: this.priceHistoryApp.props.state.chartState.dataZoomXStart,
          end: this.priceHistoryApp.props.state.chartState.dataZoomXEnd,
          xAxisIndex: xAxisIndex
        },
        {
          start: 0,
          end: 100,
          xAxisIndex: xAxisIndex
        }
      ],
      toolbox: {
        right: '9%',
        feature: ChartOptions.getFeatures(this.priceHistoryApp)
      }
    }
  }

  /**
   * @param {PriceHistoryApp} priceHistoryApp
   * @return {{myClearTimeFilter: Object}}
   */
  static getFeatures(priceHistoryApp: PriceHistoryAppInterface) {
    const features: Record<string, any> = {}
    if(isset(priceHistoryApp.props.state.chartState.timeFilter)){
      features.myClearTimeFilter = ChartOptions.getClearTimeFilterFeature(priceHistoryApp)
    }
    return features
  }

  /**
   * @param {PriceHistoryApp} priceHistoryApp
   * @return {{onclick: onclick, show: boolean, icon: string, title: string}}
   */
  static getClearTimeFilterFeature(priceHistoryApp: PriceHistoryAppInterface) {
    return {
      show: true,
      title: 'Очистить временной фильтр',
      icon: 'path://M469 819q194 0 331-137t138-332-138-331-331-138-331 138-138 331 138 332 331 137z m0-834q151 0 258 107t107 258-107 258-258 107-258-107-107-258 107-258 258-107z m36 365l139-137q12-13 5-29t-24-16q-11 0-19 7l-137 139-138-139q-7-7-18-7-17 0-24 16t5 29l139 137-139 138q-18 17 1 36t36 1l138-138 137 138q18 18 37-1t1-36z',
      onclick: (()  => {
        const quoteId = priceHistoryApp.props.state.chartState.quote
        priceHistoryApp.props.setChartStateTimeFilter(quoteId, '')
        priceHistoryApp.refreshApp()
      })
    }
  }
}