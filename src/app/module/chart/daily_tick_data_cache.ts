import DateTimeHelper from '../helper/date_time_helper'

/** Кеш ежедневных данных по котировке */
export default class DailyTickDataCache {

    static cache: Record<string, Record<string, {time: string, price: string}[]>> = {}

    /**
     * @param {String} quoteId идентификатор котировки
     * @param {Date} date дата котировки
     * @param {Object[]} dailyTickData тиковые данные котировки по указанной дате
     */
    static setDailyCache(quoteId: string, date: Date, dailyTickData: {time: string, price: string}[])
    {
        if(!(quoteId in DailyTickDataCache.cache)){
            DailyTickDataCache.cache[quoteId] = {}
        }
        const dateFormat = DateTimeHelper.dateToDateFormat(date)
        DailyTickDataCache.cache[quoteId][dateFormat] = dailyTickData
    }

    /**
     * @param {String} quoteId идентификатор котировки
     * @param {Date} date дата котировки
     * @return {Object[]|null} тиковые данные котировки по указанной дате
     */
    static getDailyCache(quoteId: string, date: Date)
    {
        if(quoteId in DailyTickDataCache.cache){
            const dateFormat = DateTimeHelper.dateToDateFormat(date)
            if(dateFormat in DailyTickDataCache.cache[quoteId]){
                return DailyTickDataCache.cache[quoteId][dateFormat]
            }
        }
        return null
    }

    /**
     * @param {String} quoteId идентификатор котировки
     * @param {Date[]} dateList список дат
     * @return {Boolean} true - кеш тиковых данных по котировке за все даты существует
     */
    static checkCacheExist(quoteId: string, dateList: Date[])
    {
        if(quoteId in DailyTickDataCache.cache){
            for (const date of dateList) {
                const dateFormat = DateTimeHelper.dateToDateFormat(date)
                if(!(dateFormat in DailyTickDataCache.cache[quoteId])){
                    return false
                }
            }
            return true
        }
        return false
    }
}