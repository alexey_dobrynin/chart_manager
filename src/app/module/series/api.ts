import {tradeRectType} from '../chart/echart_shape'

export type RectStyle = {
    fill: string
    stroke: string
}

export type RectItem = {
    type: tradeRectType
    shape: {
        buyTime: number,
        sellTime: number,
        buyPrice: number,
        sellPrice: number,
        buyCommission: number,
        sellCommission: number
    },
    style: RectStyle,
}

export type RenderSeriesItemFunction = (params: any, api: RenderCustomSeriesApi) => RectItem

export type RenderCustomSeriesApi = {
    value: <Type>(n: number) => Type
    size: (size: [number, number]) => [number, number]
    coord: (coord: [number, number]) => [number, number]
    style: (params: Record<string, string>) => RectStyle
}