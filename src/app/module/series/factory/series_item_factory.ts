import AbstractLine from '../../line/abstract_line'
import AbstractTool from '../../tool/abstract_tool'
import {SeriesItem} from '../series'

export interface SeriesItemFactory {
    createSeriesItem(name: string, lineData: AbstractLine, tool: AbstractTool, zIndex: number): SeriesItem
}