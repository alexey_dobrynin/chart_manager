import {SeriesItemFactory} from './series_item_factory'
import AbstractLine from '../../line/abstract_line'
import AbstractTool from '../../tool/abstract_tool'
import {LineSeriesItem, SeriesType} from '../series'
import {Price, SellDateTime, SeriesLineDataPoint, SeriesTradeDataPoint} from '../data'
import TimeIntervalTool from '../../tool/time_interval_tool'
import TradeStrategyTool from "../../tool/trade_strategy_tool";

export default class ProfitSeriesItemFactory implements SeriesItemFactory {

    createSeriesItem(name: string, lineData: AbstractLine, tool: AbstractTool, zIndex: number): LineSeriesItem {
        const seriesTradeData = <SeriesTradeDataPoint[]>tool.getLineData(lineData)
        const seriesData: SeriesLineDataPoint[] = seriesTradeData.map((tradeDataPoint) => {
            const sellDateTime: SellDateTime = tradeDataPoint[2]
            const totalProfit: Price = tradeDataPoint[4]
            return [sellDateTime, totalProfit]
        })
        const color = tool instanceof TradeStrategyTool ? tool.getColorProfit() : TradeStrategyTool.colorProfitDefault
        const axisIndex = tool instanceof TimeIntervalTool ? parseInt(JSON.parse(tool.chartNumber).pop()) - 1 : 0
        return {
            type: SeriesType.Line,
            data: seriesData,
            lineStyle: {'color': color},
            itemStyle: {'color': color},
            barWidth: '50%',
            zindex: zIndex,
            name: name,
            yAxisIndex: axisIndex,
            xAxisIndex: axisIndex
        }
    }

}