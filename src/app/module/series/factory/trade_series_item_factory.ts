import {SeriesItemFactory} from './series_item_factory'
import AbstractLine from '../../line/abstract_line'
import AbstractTool from '../../tool/abstract_tool'
import {SeriesType, TradeSeriesItem} from '../series'
import {SeriesTradeDataPoint} from '../data'
import TradeStrategyTool from '../../tool/trade_strategy_tool'
import {RenderCustomSeriesApi, RenderSeriesItemFunction} from '../api'
import TimeIntervalTool from '../../tool/time_interval_tool'
import {tradeRectType} from '../../chart/echart_shape'

export default class TradeSeriesItemFactory implements SeriesItemFactory {

    private defaultProfitColor = '#88ff88'
    private defaultLossesColor = '#88ff88'

    createSeriesItem(name: string, lineData: AbstractLine, tool: AbstractTool, zIndex: number): TradeSeriesItem {
        const seriesData = <SeriesTradeDataPoint[]>tool.getLineData(lineData)
        const profitColor = tool instanceof TradeStrategyTool ? tool.getColorProfit() : this.defaultProfitColor
        const lossesColor = tool instanceof TradeStrategyTool ? tool.getColorLosses() : this.defaultLossesColor
        const axisIndex = tool instanceof TimeIntervalTool ? parseInt(JSON.parse(tool.chartNumber).shift()) - 1 : 0
        const commission = tool instanceof TradeStrategyTool ? tool.brokersCommission : 0
        const strokeColor = tool instanceof TradeStrategyTool ? tool.getStrokeColor() : 'black'

        return {
            type: SeriesType.Custom,
            renderItem: this.createRenderSeriesItemFunction(profitColor, lossesColor, strokeColor, commission),
            dimensions: ['buyDateTime', 'buyPrice', 'sellDateTime', 'sellPrice'],
            encode: {
                x: [0, 2],
                y: [1, 3],
                tooltip: [0, 1, 2, 3]
            },
            data: seriesData,
            zindex: zIndex,
            name: name,
            yAxisIndex: axisIndex,
            xAxisIndex: axisIndex
        }
    }

    createRenderSeriesItemFunction(
        profitColor: string, lossesColor: string, strokeColor: string, commission: number
    ): RenderSeriesItemFunction {
        return (params: any, api: RenderCustomSeriesApi) => {
            const buyDateTime: number = api.value(0)
            const buyPrice: number = api.value(1)
            const sellDateTime: number = api.value(2)
            const sellPrice: number = api.value(3)
            const commissionBuy = commission * buyPrice
            const commissionSell = commission * sellPrice

            const buyPoint = api.coord([buyDateTime, buyPrice])
            const sellPoint = api.coord([sellDateTime, sellPrice])
            const buyCommissionPoint = api.coord([buyDateTime, buyPrice + commissionBuy])
            const sellCommissionPoint = api.coord([sellDateTime, sellPrice + commissionSell])

            let shapeType: tradeRectType
            let color: string

            if(sellPrice > buyPrice){
                if(sellPrice - buyPrice > commissionBuy + commissionSell){
                    shapeType = tradeRectType.profit
                    color = profitColor
                } else {
                    shapeType = tradeRectType.loosesProfit
                    color = lossesColor
                }
            } else {
                shapeType = tradeRectType.looses
                color = lossesColor
            }
            const style = api.style({
                fill: color,
                stroke: strokeColor
            })

            return {
                type: shapeType,
                shape: {
                    buyTime: buyPoint[0],
                    sellTime: sellPoint[0],
                    buyPrice: buyPoint[1],
                    sellPrice: sellPoint[1],
                    buyCommission: buyCommissionPoint[1],
                    sellCommission: sellCommissionPoint[1]
                },
                style: style
            }
        }
    }
}