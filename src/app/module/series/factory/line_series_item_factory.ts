import {SeriesItemFactory} from './series_item_factory'
import AbstractLine from '../../line/abstract_line'
import AbstractTool from '../../tool/abstract_tool'
import {LineSeriesItem, SeriesType} from '../series'
import OneColorTool from '../../tool/one_color_tool'
import {SeriesLineDataPoint} from '../data'
import TimeIntervalTool from '../../tool/time_interval_tool'

export default class LineSeriesItemFactory implements SeriesItemFactory {

    private defaultColor = 'gray'

    createSeriesItem(name: string, lineData: AbstractLine, tool: AbstractTool, zIndex: number): LineSeriesItem {
        const seriesData = <SeriesLineDataPoint[]>tool.getLineData(lineData)
        const color = tool instanceof OneColorTool ? tool.getColor() : this.defaultColor
        const axisIndex = tool instanceof TimeIntervalTool ? parseInt(JSON.parse(tool.chartNumber).shift()) - 1 : 0
        return {
            type: SeriesType.Line,
            data: seriesData,
            lineStyle: {'color': color},
            itemStyle: {'color': color},
            barWidth: '50%',
            zindex: zIndex,
            name: name,
            yAxisIndex: axisIndex,
            xAxisIndex: axisIndex
        }
    }

}