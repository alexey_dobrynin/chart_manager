/* Дата в формате Y-m-d H:i:s.u */
export type DateTimeWithMilliSeconds = string

/* Цена */
export type Price = number

/* Точка на линейном графике */
export type SeriesLineDataPoint = [DateTimeWithMilliSeconds, Price]

/* Время покупки */
export type BuyDateTime = DateTimeWithMilliSeconds

/* Время продажи */
export type SellDateTime = DateTimeWithMilliSeconds

/* Цена покупки */
export type BuyPrice = Price

/* Цена продажи */
export type SellPrice = Price

export type TotalProfit = number

export type SeriesTradeDataPoint = [BuyDateTime, BuyPrice, SellDateTime, SellPrice, TotalProfit]
