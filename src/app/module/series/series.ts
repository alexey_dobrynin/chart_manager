import {SeriesLineDataPoint, SeriesTradeDataPoint} from './data'
import {ItemStyle, LineStyle} from './style'
import {RenderSeriesItemFunction} from "./api";

export enum ChartType {
    Line = 'line',
    Trade = 'trade'
}

export enum SeriesType {
    Line = 'line',
    Custom = 'custom'
}

export type SeriesDataPoint = SeriesLineDataPoint | SeriesTradeDataPoint

export type SeriesData = SeriesLineDataPoint[] | SeriesTradeDataPoint[]

export type LineSeriesItem = {
    type: SeriesType.Line
    data: SeriesLineDataPoint[]
    lineStyle: LineStyle
    itemStyle: ItemStyle
    barWidth: string
    zindex: number
    name: string
    yAxisIndex: number
    xAxisIndex: number
}

export type TradeSeriesItem = {
    type: SeriesType.Custom
    renderItem: RenderSeriesItemFunction
    dimensions: string[],
    encode: {
        x: number[],
        y: number[],
        tooltip: number[]
    },
    data: SeriesTradeDataPoint[]
    zindex: number
    name: string
    yAxisIndex: number
    xAxisIndex: number
}

export type SeriesItem = LineSeriesItem | TradeSeriesItem

export type series = SeriesItem[]


