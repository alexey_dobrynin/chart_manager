import ObjectKeyStructure from '../../object/object_key_structure'
import TradeStrategyList from '../../trade/trade_strategy_list'

export default class SelectedTradeStrategy extends ObjectKeyStructure {

  /**
   * @return {TradeStrategyList}
   */
  getTradeStrategyList() {
    return new TradeStrategyList()
  }

  /**
   * @param {String} tradeStrategyJson
   * @return {TradeStrategy|undefined}
   */
  jsonToItem(tradeStrategyJson: string) {
    return this.getTradeStrategyList().paramCodeToStrategy(tradeStrategyJson)
  }

  /**
   * @param {Object} tradeStrategyObject
   * @return {AbstractTool}
   */
  objectToItem(tradeStrategyObject: Record<string, any>) {
    const tradeStrategyJson = JSON.stringify(tradeStrategyObject)
    return this.jsonToItem(tradeStrategyJson)
  }

  /**
   * @return {Object<String, TradeStrategy>}
   */
  getItems() {
    return super.getItems()
  }
}