import TradeActionList from '../trade_action_list'
import TickLine from '../../line/tick_line'
import BarTicksPoint from '../../point/bar_ticks_point'
import AbstractTradeStrategyParams from './params/abstract_trade_strategy_params'
import {TradeActionType} from '../trade_action_type'
import TickPoint from '../../point/tick_point'

/** Стратегия торговли */
export default class TradeStrategy {

    tradeActionList: TradeActionList

    lastTradeAction: TradeActionType = TradeActionType.Sell

    barTicksPointList: BarTicksPoint[]

    params: AbstractTradeStrategyParams

    constructor() {
        this.barTicksPointList = []
        this.tradeActionList = new TradeActionList()
        this.params = new AbstractTradeStrategyParams()
    }

    getParams() {
        return this.params
    }

    setParams(params: AbstractTradeStrategyParams) {
        this.params = params
    }

    getBarTicksPointList()
    {
        return this.barTicksPointList
    }

    getStrategyName() {
        return 'Абстрактная торговая стратегия'
    }

    initTradeStrategy(tradeStrategyParams: string) {
        this.getParams().initFromJson(tradeStrategyParams)
    }

    trade(barTicksPointList: BarTicksPoint[]) {
        let barTicksPointListIndex = 0
        while (barTicksPointListIndex < barTicksPointList.length){
            const barTicksPoint = barTicksPointList[barTicksPointListIndex]
            const tickPointList = barTicksPoint.getTickPointList()
            const tickLine = new TickLine([])
            const tradeBarTicksPoint = new BarTicksPoint(tickLine, barTicksPoint.getDateTime())
            this.barTicksPointList.push(tradeBarTicksPoint)
            let tickPointListIndex = 0
            while (tickPointListIndex < tickPointList.length){
                const tickPoint = tickPointList[tickPointListIndex]
                tradeBarTicksPoint.tickLine.addPoint(tickPoint)
                this.auxiliaryCalculation()
                switch (this.lastTradeAction) {
                    case TradeActionType.Sell:
                        if(this.buyCondition()) {
                            this.buyAction()
                            this.lastTradeAction = TradeActionType.Buy
                        }
                        break
                    case TradeActionType.Buy:
                        if(this.sellCondition()){
                            this.sellAction()
                            this.lastTradeAction = TradeActionType.Sell
                        }
                        break
                }
                tickPointListIndex++
            }
            barTicksPointListIndex++
        }

        return this.tradeActionList
    }

    /** Вспомогательные расчёты  */
    auxiliaryCalculation() {

    }

    protected getLastTickPoint()
    {
        const barTicksPoint = this.barTicksPointList[this.barTicksPointList.length - 1]
        return <TickPoint>barTicksPoint.getTickLine().getLastTickPoint()
    }

    /** Действие покупки */
    buyAction() {
        const tickPoint = this.getLastTickPoint()
        this.tradeActionList.buy(tickPoint.getDate(), tickPoint.getPrice())
    }

    /** Действие продажи */
    sellAction() {
        const tickPoint = this.getLastTickPoint()
        this.tradeActionList.sell(tickPoint.getDate(), tickPoint.getPrice())
    }

    buyCondition() {
        return false
    }

    sellCondition() {
        return false
    }
}