import TradeStrategy from './trade_strategy'
import MovingAverageCrossoverParams from './params/moving_average_crossover_params'
import MovingAveragePointListCalculator from './moving_average/moving_average_point_list_calculator'
import TickPoint from '../../point/tick_point'

/** Пересечение скользящих средних */
export default class MovingAverageCrossover extends TradeStrategy {

    fastMovingAverageCalculator?: MovingAveragePointListCalculator

    slowMovingAverageCalculator?: MovingAveragePointListCalculator

    stopLossPercent: number = 0

    stopLossAmount: number = 0

    constructor() {
        super()
        this.params = new MovingAverageCrossoverParams()
    }

    getParams()
    {
        return <MovingAverageCrossoverParams>this.params
    }

    /**
     * @return {String}
     */
    getStrategyName() {
        return 'Пересечение скользящих средних'
    }

    initTradeStrategy(tradeStrategyParams: string) {
        super.initTradeStrategy(tradeStrategyParams)
        const params = this.getParams()
            this.fastMovingAverageCalculator = new MovingAveragePointListCalculator(
            params.fastMovingAverageType,
            params.fastMovingAveragePeriod,
            params.fastMovingAveragePeriodInterval,
            params.fastMovingAverageBarPoint
        )

        this.slowMovingAverageCalculator = new MovingAveragePointListCalculator(
            params.slowMovingAverageType,
            params.slowMovingAveragePeriod,
            params.slowMovingAveragePeriodInterval,
            params.slowMovingAverageBarPoint
        )

        this.stopLossPercent = parseFloat(params.stopLossPercent)
    }

    /** Вспомогательные расчёты  */
    auxiliaryCalculation() {
        if(this.barTicksPointList.length > 0){
            const lastTickPoint = this.getLastTickPoint()
            this.fastMovingAverageCalculator!.addPoint(lastTickPoint)
            this.slowMovingAverageCalculator!.addPoint(lastTickPoint)
        }
    }

    tradeActionCondition(
        checkCondition: (
            fastStartPoint: TickPoint,
            fastEndPoint: TickPoint,
            slowStartPoint: TickPoint,
            slowEndPoint: TickPoint
        ) => boolean
    ) {
        const fastMovingAverageListStart = this.fastMovingAverageCalculator!.getMovingAverageListSize() - 2
        const slowMovingAverageListStart = this.slowMovingAverageCalculator!.getMovingAverageListSize() - 2

        if(fastMovingAverageListStart >= 0){
            const fastMovingAverageListEnd = fastMovingAverageListStart + 1
            const slowMovingAverageListEnd = slowMovingAverageListStart + 1
            const fastStartPoint = this.fastMovingAverageCalculator!.movingAveragePointList[fastMovingAverageListStart]
            const fastEndPoint = this.fastMovingAverageCalculator!.movingAveragePointList[fastMovingAverageListEnd]
            const slowStartPoint = this.slowMovingAverageCalculator!.movingAveragePointList[slowMovingAverageListStart]
            const slowEndPoint = this.slowMovingAverageCalculator!.movingAveragePointList[slowMovingAverageListEnd]

            return checkCondition(fastStartPoint, fastEndPoint, slowStartPoint, slowEndPoint)
        }
        return false
    }

    /**
     * @return {Boolean}
     */
    buyCondition() {
        return this.tradeActionCondition(
            (
                fastStartPoint: TickPoint,
                fastEndPoint: TickPoint,
                slowStartPoint: TickPoint,
                slowEndPoint: TickPoint
            ) => {
                return (
                    (slowStartPoint.getPrice() < fastStartPoint.getPrice())
                    &&
                    (slowEndPoint.getPrice() > fastEndPoint.getPrice())
                )
            }
        )
    }

    buyAction() {
        super.buyAction()
        const actionList = this.tradeActionList.getActionList()
        const buyPrice = actionList[actionList.length - 1].getPrice()
        this.stopLossAmount = buyPrice - buyPrice * this.stopLossPercent / 100
    }

    /**
     * @return {Boolean}
     */
    sellCondition() {
        return this.tradeActionCondition(
            (
                fastStartPoint: TickPoint,
                fastEndPoint: TickPoint,
                slowStartPoint: TickPoint,
                slowEndPoint: TickPoint
            ) => {
                const lastTickPoint = this.getLastTickPoint()

                return lastTickPoint.getPrice() <= this.stopLossAmount
                ||
                (
                    (fastStartPoint.getPrice() < slowStartPoint.getPrice())
                    &&
                    (fastEndPoint.getPrice() > slowEndPoint.getPrice())

                )
            }
        )
    }
}