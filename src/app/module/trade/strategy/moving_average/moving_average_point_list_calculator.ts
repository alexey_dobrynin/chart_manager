import MovingAverageCalculator from '../../../filter/moving_average_calculator'
import TickPoint from '../../../point/tick_point'
import MovingAverageType from '../../../filter/moving_average_type'
import MathHelper from "../../../helper/math_helper";

/** Расчет точек скользящей средней */
export default class MovingAveragePointListCalculator {

    /** @var {TickPoint[]} список точек скользящей средней */
    movingAveragePointList: TickPoint[]

    /** @var {String} тип функции расчета скользящей средней */
    movingAverageType

    /** @var {Function} функция расчета скользящей средней */
    movingAverageCalculator: (periodPointList: TickPoint[], lastFilteredPoint?: TickPoint) => number

    /** @var {TickPoint[]} список исходных точек */
    periodPointList: TickPoint[]

    /** @var {Number} шаг в секундах */
    step

    /** @var {Number} число шагов в периоде */
    period

    /** @var {String} точка на столбце */
    barPoint

    /** @var {Number} дата и время начала последнего периода */
    lastPeriodStart: number

    /** @var {Number} дата и время окончания последнего периода */
    lastPeriodEnd: number

    /**
     * @param {String} movingAverageType тип функции расчета скользящей средней
     * @param {String} period число шагов в периоде
     * @param {String} step шаг в секундах
     * @param {String} barPoint точка на столбце
     */
    constructor(movingAverageType: string, period: string, step: string, barPoint: string) {
        this.movingAverageType = movingAverageType
        switch (movingAverageType){
            default:
            case MovingAverageType.typeList.movingAverageSimple:
                this.movingAverageCalculator = MovingAverageCalculator.simple
                break
            case MovingAverageType.typeList.movingAverageWeighted:
                this.movingAverageCalculator = MovingAverageCalculator.weighted
                break
            case MovingAverageType.typeList.movingAverageExponential:
                this.movingAverageCalculator = MovingAverageCalculator.exponential(parseInt(period))
                break
        }
        this.movingAveragePointList = []
        this.periodPointList = []
        this.step = parseInt(step)
        this.period = parseInt(period)
        this.barPoint = barPoint
        this.lastPeriodStart = 0
        this.lastPeriodEnd = 0
    }

    /**
     * @param {TickPoint} point
     */
    addPoint(point: TickPoint) {
        if(this.lastPeriodStart === 0){
            this.initLastPeriod(point)
        }
        const pointDate = point.getDate()
        const pointDateUnixTime = pointDate.getTime()

        this.periodPointList.push(point)
        if(pointDateUnixTime >= this.lastPeriodEnd){
            const addPeriodDelta = Math.max(
                pointDateUnixTime - this.lastPeriodEnd, this.getStepMilliSeconds()
            )
            this.lastPeriodStart += addPeriodDelta
            this.lastPeriodEnd += addPeriodDelta
            this.refreshPeriodPointList()
        }
        this.addMovingAveragePoint()
    }

    refreshPeriodPointList(){
        while (
            this.periodPointList.length > 0
            && this.periodPointList[0].getDate().getTime() < this.lastPeriodStart
        ){
            this.periodPointList.shift()
        }
    }

    /**
     * @return {Number}
     */
    getMovingAverageListSize() {
        return this.movingAveragePointList.length
    }

    /** расчет и добавление новой точки скользящей средней */
    addMovingAveragePoint() {
        const lastTickPoint = this.periodPointList[this.periodPointList.length - 1]
        const lastFilteredPoint = this.movingAveragePointList.length > 0
            ? this.movingAveragePointList[this.movingAveragePointList.length - 1]
            : undefined
        const movingAverageAmount = this.movingAverageCalculator(this.periodPointList, lastFilteredPoint)
        this.movingAveragePointList.push(new TickPoint(
            MathHelper.round(movingAverageAmount, -4).toString(),
            lastTickPoint.getDateTime()
        ))
    }

    /**
     * @param {TickPoint} point
     */
    initLastPeriod(point: TickPoint){
        const startPeriodDate = point.getDate()
        this.lastPeriodStart = startPeriodDate.getTime()
        const periodMilliSeconds = this.getPeriodMilliSeconds()
        this.lastPeriodEnd = this.lastPeriodStart + periodMilliSeconds
    }

    private getStepMilliSeconds(){
        return this.step * 1000
    }

    private getPeriodMilliSeconds(){
        return this.getStepMilliSeconds() * this.period
    }
}