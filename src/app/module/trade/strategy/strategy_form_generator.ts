import FormElementGenerator from '../../form/form_element_generator'
import AbstractTradeStrategyParams from '../strategy/params/abstract_trade_strategy_params'
import {ActiveTradeStrategyListInterface} from '../active_trade_strategy_list'

/** Генератор формы параметров стратегии торговли */
export default class TradeFormGenerator {

    /**
     * @param {AbstractTradeStrategyParams} strategyParams
     * @param {ActiveTradeStrategyList} activeTradeStrategyList
     */
    static getAddFormElements(strategyParams: AbstractTradeStrategyParams, activeTradeStrategyList: ActiveTradeStrategyListInterface){
        return strategyParams.getAddFormElements(activeTradeStrategyList).map(((formElement) => {
            const getTemplateFunction = FormElementGenerator.getTypeToGetTemplateFunction(formElement)
            return getTemplateFunction(formElement)
        }))
    }

    /**
     * @param {AbstractTradeStrategyParams} strategyParams
     * @param {ActiveTradeStrategyList} activeTradeStrategyList
     */
    static getEditFormElements(strategyParams: AbstractTradeStrategyParams, activeTradeStrategyList: ActiveTradeStrategyListInterface){
        return strategyParams.getEditFormElements(activeTradeStrategyList).map(((formElement) => {
            const getTemplateFunction = FormElementGenerator.getTypeToGetTemplateFunction(formElement)
            return getTemplateFunction(formElement)
        }))
    }
}