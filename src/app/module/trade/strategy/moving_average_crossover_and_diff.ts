import TradeStrategy from './trade_strategy'
import MovingAveragePointListCalculator from './moving_average/moving_average_point_list_calculator'
import TickPoint from '../../point/tick_point'
import MovingAverageCrossoverAndDiffParams from './params/moving_average_crossover_and_diff_params'

/** Пересечение скользящих средних и скользящая средняя разницы цены */
export default class MovingAverageCrossoverAndDiff extends TradeStrategy {

    fastMovingAverageCalculator?: MovingAveragePointListCalculator

    slowMovingAverageCalculator?: MovingAveragePointListCalculator

    movingAverageDiffCalculator?: MovingAveragePointListCalculator

    previousTickPoint?: TickPoint

    stopLossPercent: number = 0

    stopLossAmount: number = 0

    constructor() {
        super()
        this.params = new MovingAverageCrossoverAndDiffParams()
    }

    getParams(): MovingAverageCrossoverAndDiffParams
    {
        return <MovingAverageCrossoverAndDiffParams>this.params
    }

    /**
     * @return {String}
     */
    getStrategyName() {
        return 'Пересечение скользящих средних и скользящая средняя разницы цены'
    }

    initTradeStrategy(tradeStrategyParams: string) {
        super.initTradeStrategy(tradeStrategyParams)
        const params = this.getParams()
        this.fastMovingAverageCalculator = new MovingAveragePointListCalculator(
            params.fastMovingAverageType,
            params.fastMovingAveragePeriod,
            params.fastMovingAveragePeriodInterval,
            params.fastMovingAverageBarPoint
        )

        this.slowMovingAverageCalculator = new MovingAveragePointListCalculator(
            params.slowMovingAverageType,
            params.slowMovingAveragePeriod,
            params.slowMovingAveragePeriodInterval,
            params.slowMovingAverageBarPoint
        )

        this.movingAverageDiffCalculator = new MovingAveragePointListCalculator(
            params.movingAverageDiffType,
            params.movingAverageDiffPeriod,
            params.movingAverageDiffPeriodInterval,
            params.movingAverageDiffBarPoint
        )

        this.stopLossPercent = parseFloat(params.stopLossPercent)
    }

    /** Вспомогательные расчёты  */
    auxiliaryCalculation() {
        if(this.barTicksPointList.length > 0){
            const barTicksPoint = this.barTicksPointList[this.barTicksPointList.length - 1]
            const tickPointList = barTicksPoint.getTickPointList()
            const lastTickPoint = tickPointList[tickPointList.length - 1]
            let previousTickPoint = this.previousTickPoint instanceof TickPoint
                ? this.previousTickPoint
                : lastTickPoint
            const diffTickPoint = new TickPoint(
                (lastTickPoint.getPrice() - previousTickPoint.getPrice()).toString(),
                lastTickPoint.getDateTime()
            )
            this.fastMovingAverageCalculator!.addPoint(lastTickPoint)
            this.slowMovingAverageCalculator!.addPoint(lastTickPoint)
            this.movingAverageDiffCalculator!.addPoint(diffTickPoint)
            this.previousTickPoint = lastTickPoint
        }
    }

    tradeActionCondition(
        checkCondition: (
            fastStartPoint: TickPoint,
            fastEndPoint: TickPoint,
            slowStartPoint: TickPoint,
            slowEndPoint: TickPoint,
            diffLastPoint: TickPoint
        ) => boolean
    ) {
        const fastMovingAverageListStart = this.fastMovingAverageCalculator!.getMovingAverageListSize() - 2
        const slowMovingAverageListStart = this.slowMovingAverageCalculator!.getMovingAverageListSize() - 2
        const movingAverageDiffLast = this.slowMovingAverageCalculator!.getMovingAverageListSize() - 1

        if(fastMovingAverageListStart >= 0){
            const fastMovingAverageListEnd = fastMovingAverageListStart + 1
            const slowMovingAverageListEnd = slowMovingAverageListStart + 1
            const fastStartPoint = this.fastMovingAverageCalculator!.movingAveragePointList[fastMovingAverageListStart]
            const fastEndPoint = this.fastMovingAverageCalculator!.movingAveragePointList[fastMovingAverageListEnd]
            const slowStartPoint = this.slowMovingAverageCalculator!.movingAveragePointList[slowMovingAverageListStart]
            const slowEndPoint = this.slowMovingAverageCalculator!.movingAveragePointList[slowMovingAverageListEnd]
            const diffLastPoint = this.movingAverageDiffCalculator!.movingAveragePointList[movingAverageDiffLast]

            return checkCondition(fastStartPoint, fastEndPoint, slowStartPoint, slowEndPoint, diffLastPoint)
        }
        return false
    }

    /**
     * @return {Boolean}
     */
    buyCondition() {
        return this.tradeActionCondition(
            (
                fastStartPoint: TickPoint,
                fastEndPoint: TickPoint,
                slowStartPoint: TickPoint,
                slowEndPoint: TickPoint,
                diffLastPoint: TickPoint
            ) => {
                return (
                    (fastStartPoint.getPrice() < slowStartPoint.getPrice())
                    &&
                    (fastEndPoint.getPrice() > slowEndPoint.getPrice())
                    &&
                    diffLastPoint.getPrice() > 0
                )
            }
        )
    }

    buyAction() {
        super.buyAction()
        const actionList = this.tradeActionList.getActionList()
        const buyPrice = actionList[actionList.length - 1].getPrice()
        this.stopLossAmount = buyPrice - buyPrice * this.stopLossPercent / 100
    }

    /**
     * @return {Boolean}
     */
    sellCondition() {
        return this.tradeActionCondition(
            (
                fastStartPoint: TickPoint,
                fastEndPoint: TickPoint,
                slowStartPoint: TickPoint,
                slowEndPoint: TickPoint,
                diffLastPoint: TickPoint
            ) => {
                const lastTickPoint = this.getLastTickPoint()

                return lastTickPoint.getPrice() <= this.stopLossAmount
                    ||
                    (
                        (slowStartPoint.getPrice() < fastStartPoint.getPrice())
                        &&
                        (slowEndPoint.getPrice() > fastEndPoint.getPrice())
                    )
            }
        )
    }
}