import AbstractTradeStrategyParams from './abstract_trade_strategy_params'
import BarPoint from '../../../point/bar_point'
import Header from '../../../form/header'
import IntParameter from '../../../form/parameter/int_parameter'
import SelectParameter from '../../../form/parameter/select_parameter'
import PeriodIntervalOptions from '../../../chart/period_interval_options'
import TargetOptions from '../../../option/target_options'
import MovingAverageType from '../../../filter/moving_average_type'
import {ActiveTradeStrategyListType} from '../../active_trade_strategy_list'

/** Параметры для стратегии: пересечение скользящих средних */
export default class MovingAverageCrossoverParams extends AbstractTradeStrategyParams {

    static defaultFastMovingAveragePeriod = '11'
    static defaultSlowMovingAveragePeriod = '23'
    static defaultStopLossPercent = '1'

    fastMovingAveragePeriod: string = MovingAverageCrossoverParams.defaultFastMovingAveragePeriod
    fastMovingAveragePeriodInterval: string = '1'
    fastMovingAverageType: string = MovingAverageType.typeList.movingAverageExponential
    fastMovingAverageBarPoint: string = BarPoint.type.closeValue

    slowMovingAveragePeriod: string = MovingAverageCrossoverParams.defaultSlowMovingAveragePeriod
    slowMovingAveragePeriodInterval: string = '1'
    slowMovingAverageType: string = MovingAverageType.typeList.movingAverageExponential
    slowMovingAverageBarPoint: string = BarPoint.type.closeValue

    stopLossPercent: string = MovingAverageCrossoverParams.defaultStopLossPercent


    constructor(name?: string, code?: string) {
        super(
            name === undefined ? 'Параметры для стратегии: пересечение скользящих средних' : name,
            code === undefined ? MovingAverageCrossoverParams.name : code
        )
    }

    initParameters(activeTradeStrategyList: ActiveTradeStrategyListType) {
        const periodIntervalOption = activeTradeStrategyList.props.state.chartState.getPeriodIntervalOption()
        this.fastMovingAveragePeriod = MovingAverageCrossoverParams.defaultFastMovingAveragePeriod
        this.fastMovingAveragePeriodInterval = periodIntervalOption.getValue()
        this.fastMovingAverageType = MovingAverageType.typeList.movingAverageExponential
        this.fastMovingAverageBarPoint = BarPoint.type.closeValue

        this.slowMovingAveragePeriod = MovingAverageCrossoverParams.defaultSlowMovingAveragePeriod
        this.slowMovingAveragePeriodInterval = periodIntervalOption.getValue()
        this.slowMovingAverageType = MovingAverageType.typeList.movingAverageExponential
        this.slowMovingAverageBarPoint = BarPoint.type.closeValue

        this.stopLossPercent = MovingAverageCrossoverParams.defaultStopLossPercent
    }

    getFormElements(activeTradeStrategyList: ActiveTradeStrategyListType) {
        const fastMovingAverageSectionName = 'Быстрая сколльзщая средняя'
        const slowMovingAverageSectionName = 'Медленная скользщая средняя'
        return [
            new Header({ text: fastMovingAverageSectionName, size: 4, section: fastMovingAverageSectionName}),
            new Header({ text: 'Период', size: 5, section: fastMovingAverageSectionName}),
            new IntParameter({
                name: 'fastMovingAveragePeriod',
                description: 'Период',
                min: 1,
                step: 1,
                value: this.fastMovingAveragePeriod,
                section: fastMovingAverageSectionName,
                onChange: (event) => {
                    this.fastMovingAveragePeriod = event.target.value
                }
            }),
            new SelectParameter({
                name: 'fastMovingAveragePeriodInterval',
                description: 'Интервал',
                options: PeriodIntervalOptions.getOptions(),
                value: this.fastMovingAveragePeriodInterval,
                section: fastMovingAverageSectionName,
                onChange: (event) => {
                    this.fastMovingAveragePeriodInterval = event.target.value
                }
            }),
            new Header({ text: 'Тип скользящей средней', size: 5, section: fastMovingAverageSectionName}),
            new SelectParameter({
                name: 'fastMovingAverageType',
                description: 'Тип скользящей средней',
                options: MovingAverageType.getOptions(),
                value: this.fastMovingAverageType,
                section: fastMovingAverageSectionName,
                onChange: (event) => {
                    this.fastMovingAverageType = event.target.value
                }
            }),
            new Header({ text: 'Точка на столбце', size: 5, section: fastMovingAverageSectionName}),
            new SelectParameter({
                name: 'fastMovingAverageBarPoint',
                value: this.fastMovingAverageBarPoint,
                description: 'Точка на столбце',
                section: fastMovingAverageSectionName,
                onChange: (event) => {
                    this.fastMovingAverageBarPoint = event.target.value
                },
                options: TargetOptions.getOptions()
            }),
            new Header({ text: slowMovingAverageSectionName, size: 4, section: slowMovingAverageSectionName}),
            new Header({ text: 'Период', size: 5, section: slowMovingAverageSectionName}),
            new IntParameter({
                name: 'slowMovingAveragePeriod',
                description: 'Период',
                min: 1,
                step: 1,
                value: this.slowMovingAveragePeriod,
                section: slowMovingAverageSectionName,
                onChange: (event) => {
                    this.slowMovingAveragePeriod = event.target.value
                }
            }),
            new SelectParameter({
                name: 'slowMovingAveragePeriodInterval',
                description: 'Интервал',
                options: PeriodIntervalOptions.getOptions(),
                value: this.slowMovingAveragePeriodInterval,
                section: slowMovingAverageSectionName,
                onChange: (event) => {
                    this.slowMovingAveragePeriodInterval = event.target.value
                }
            }),
            new Header({ text: 'Тип скользящей средней', size: 5, section: slowMovingAverageSectionName}),
            new SelectParameter({
                name: 'slowMovingAverageType',
                description: 'Тип скользящей средней',
                options: MovingAverageType.getOptions(),
                value: this.slowMovingAverageType,
                section: slowMovingAverageSectionName,
                onChange: (event) => {
                    this.slowMovingAverageType = event.target.value
                }
            }),
            new Header({ text: 'Точка на столбце', size: 5, section: slowMovingAverageSectionName}),
            new SelectParameter({
                name: 'slowMovingAverageBarPoint',
                value: this.slowMovingAverageBarPoint,
                description: 'Точка на столбце',
                section: slowMovingAverageSectionName,
                onChange: (event) => {
                    this.slowMovingAverageBarPoint = event.target.value
                },
                options: TargetOptions.getOptions()
            }),
            new Header({ text: 'Процент допустимых потерь', size: 5}),
            new IntParameter({
                name: 'stopLossPercent',
                description: 'процент допустимых потерь',
                min: 0,
                step: 0.01,
                value: this.stopLossPercent,
                onChange: (event) => {
                    this.stopLossPercent = event.target.value
                }
            }),
        ]
    }

}