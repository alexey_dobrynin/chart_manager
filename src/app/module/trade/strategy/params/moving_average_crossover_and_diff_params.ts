import MovingAverageCrossoverParams from './moving_average_crossover_params'
import {ActiveTradeStrategyListType} from '../../active_trade_strategy_list'
import MovingAverageType from '../../../filter/moving_average_type'
import BarPoint from '../../../point/bar_point'
import Header from '../../../form/header'
import IntParameter from '../../../form/parameter/int_parameter'
import SelectParameter from '../../../form/parameter/select_parameter'
import PeriodIntervalOptions from '../../../chart/period_interval_options'
import TargetOptions from '../../../option/target_options'

/** Параметры для стратегии: пересечение скользящих средних и скользящая средняя разницы цены */
export default class MovingAverageCrossoverAndDiffParams extends MovingAverageCrossoverParams {

    static defaultMovingAverageDiffPeriod = '100'

    movingAverageDiffPeriod: string = MovingAverageCrossoverAndDiffParams.defaultMovingAverageDiffPeriod
    movingAverageDiffPeriodInterval: string = '1'
    movingAverageDiffType: string = MovingAverageType.typeList.movingAverageExponential
    movingAverageDiffBarPoint: string = BarPoint.type.closeValue

    constructor(name?: string, code?: string) {
        super(
            name === undefined ? 'Параметры для стратегии: пересечение скользящих средних и скользящая средняя разницы цены' : name,
            code === undefined ? MovingAverageCrossoverAndDiffParams.name : code
        )
    }

    initParameters(activeTradeStrategyList: ActiveTradeStrategyListType) {
        super.initParameters(activeTradeStrategyList)
        const periodIntervalOption = activeTradeStrategyList.props.state.chartState.getPeriodIntervalOption()
        this.movingAverageDiffPeriod = MovingAverageCrossoverAndDiffParams.defaultMovingAverageDiffPeriod
        this.movingAverageDiffPeriodInterval = periodIntervalOption.getValue()
        this.movingAverageDiffType = MovingAverageType.typeList.movingAverageExponential
        this.movingAverageDiffBarPoint = BarPoint.type.closeValue
    }

    getFormElements(activeTradeStrategyList: ActiveTradeStrategyListType) {
        const formElements = super.getFormElements(activeTradeStrategyList)
        const movingAverageDiffSectionName = 'Скользящая средняя разницы цены'
        formElements.push(
            new Header({ text: movingAverageDiffSectionName, size: 4, section: movingAverageDiffSectionName}),
            new Header({ text: 'Период', size: 5, section: movingAverageDiffSectionName}),
            new IntParameter({
                name: 'movingAverageDiffPeriod',
                description: 'Период',
                min: 1,
                step: 1,
                value: this.movingAverageDiffPeriod,
                section: movingAverageDiffSectionName,
                onChange: (event) => {
                    this.movingAverageDiffPeriod = event.target.value
                }
            }),
            new SelectParameter({
                name: 'movingAverageDiffPeriodInterval',
                description: 'Интервал',
                options: PeriodIntervalOptions.getOptions(),
                value: this.movingAverageDiffPeriodInterval,
                section: movingAverageDiffSectionName,
                onChange: (event) => {
                    this.movingAverageDiffPeriodInterval = event.target.value
                }
            }),
            new Header({ text: 'Тип скользящей средней', size: 5, section: movingAverageDiffSectionName}),
            new SelectParameter({
                name: 'movingAverageDiffType',
                description: 'Тип скользящей средней',
                options: MovingAverageType.getOptions(),
                value: this.movingAverageDiffType,
                section: movingAverageDiffSectionName,
                onChange: (event) => {
                    this.movingAverageDiffType = event.target.value
                }
            }),
            new Header({ text: 'Точка на столбце', size: 5, section: movingAverageDiffSectionName}),
            new SelectParameter({
                name: 'movingAverageDiffBarPoint',
                value: this.movingAverageDiffBarPoint,
                description: 'Точка на столбце',
                section: movingAverageDiffSectionName,
                onChange: (event) => {
                    this.movingAverageDiffBarPoint = event.target.value
                },
                options: TargetOptions.getOptions()
            })
        )
        return formElements
    }
}