import Header from '../../../form/header'
import HorizontalRule from '../../../form/horizontal_rule'
import Button from '../../../form/button'
import ObjectHelper from '../../../helper/object_helper'
import {ActiveTradeStrategyListInterface} from '../../active_trade_strategy_list'
import AbstractFormElement from '../../../form/abstract_form_element'

/** Абстрактный наброр парамтров для торговой стратегии */
export default class AbstractTradeStrategyParams {
    [key: string]: any

    name: string
    code:string

    constructor(name?: string, code?: string) {
        this.name = name === undefined ? 'Параметры для стратегии' : name
        this.code = code === undefined ? AbstractTradeStrategyParams.name : code
    }

    getName() {
        return this.name
    }

    getCode() {
        return this.code
    }

    /**
     * Инициализация параметров
     * @param {ActiveTradeStrategyList} activeTradeStrategyList
     */
    initParameters(activeTradeStrategyList: ActiveTradeStrategyListInterface) {

    }

    /**
     * @param {ActiveTradeStrategyList} activeTradeStrategyList
     * @return {AbstractFormElement[]} список элементов формы
     */
    getFormElements(activeTradeStrategyList: ActiveTradeStrategyListInterface): AbstractFormElement[] {
        return [
        ]
    }

    /**
     * @param {ActiveTradeStrategyList} activeTradeStrategyList
     * @return {AbstractFormElement[]} список элементов формы добавления
     */
    getFullFormElements(activeTradeStrategyList: ActiveTradeStrategyListInterface) {
        const fullFormElements = this.getFormElements(activeTradeStrategyList)
        fullFormElements.unshift( new Header({text: this.getName(), size: 3}) )
        fullFormElements.push( new HorizontalRule({}) )
        return fullFormElements
    }

    /**
     * @param {ActiveTradeStrategyList} activeTradeStrategyList
     * @return {AbstractFormElement[]} список элементов формы добавления
     */
    getAddFormElements(activeTradeStrategyList: ActiveTradeStrategyListInterface) {
        this.initParameters(activeTradeStrategyList)
        const addFormElements = this.getFullFormElements(activeTradeStrategyList)
        addFormElements.push(
            new Button({
                text: 'Добавить',
                onClick: (() => {
                    activeTradeStrategyList.addTradeStrategyParams( this.clone() )
                    activeTradeStrategyList.closeModal()
                    activeTradeStrategyList.props.refreshApp()
                })
            })
        )
        return addFormElements
    }

    /**
     * @param {ActiveTradeStrategyList} activeTradeStrategyList
     * @return {AbstractFormElement[]} список элементов формы добавления
     */
    getEditFormElements(activeTradeStrategyList: ActiveTradeStrategyListInterface) {
        const editFormElements = this.getFullFormElements(activeTradeStrategyList)
        editFormElements.push(
            new Button({
                text: 'Изменить',
                onClick: (() => {
                    activeTradeStrategyList.editTradeStrategyParams(this)
                    activeTradeStrategyList.closeModal()
                    activeTradeStrategyList.props.refreshApp()
                })
            })
        )
        return editFormElements
    }

    /**
     * @return {AbstractTradeStrategyParams}
     */
    clone() {
        return ObjectHelper.clone(this)
    }

    /**
     * @param {String} jsonString
     */
    initFromJson(jsonString: string) {
        const toolParams = JSON.parse(jsonString)
        Object.getOwnPropertyNames(this).forEach((propertyName) => {
            this[propertyName] = toolParams[propertyName]
        })
    }

    /**
     * @return {String} json-строка
     */
    toJson(){
        const object: Record<string, any> = {}
        Object.getOwnPropertyNames(this).forEach((propertyName) => {
            object[propertyName] = this[propertyName]
        })
        return JSON.stringify(object)
    }
}