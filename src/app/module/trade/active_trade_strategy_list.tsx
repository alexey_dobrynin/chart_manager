import React from 'react'
import Modal from '../modal/modal'
import StrategyFormGenerator from './strategy/strategy_form_generator'
import {connector, PriceHistoryAppChildRefresh} from '../price_history_app/connector'
import AbstractTradeStrategyParams from './strategy/params/abstract_trade_strategy_params'

interface ActiveTradeStrategyListState {
    modalOpen: boolean
    modalContent: JSX.Element|null
    selectedTradeStrategyParams: string
}

export interface ActiveTradeStrategyListInterface {
    props: PriceHistoryAppChildRefresh
    addTradeStrategyParams: (tradeStrategyParams: AbstractTradeStrategyParams) => void
    editTradeStrategyParams: (tradeStrategyParams: AbstractTradeStrategyParams) => void
    closeModal: () => void
}

/** Список активных стратегий торговли */
class ActiveTradeStrategyList extends React.Component<PriceHistoryAppChildRefresh, ActiveTradeStrategyListState> implements ActiveTradeStrategyListInterface {

    /**
     * @param {Function} props.refreshApp
     */
    constructor(props: PriceHistoryAppChildRefresh) {
        super(props)
        this.state = {
            modalOpen: false,
            modalContent: null,
            selectedTradeStrategyParams: ''
        }
    }

    render() {
        const tradeStrategyList = this.props.state.tradeStrategyList
        let tradeStrategyOptions = tradeStrategyList.getItems().map((tradeStrategy) => {
            return <div
              key={ tradeStrategy.getStrategyName() }
              className="dropdown-item"
              onClick={() => { this.showAddStrategyModal(tradeStrategy.getParams()) }}>
                { tradeStrategy.getStrategyName() }
            </div>
        })

        return <nav
            className="dropdown"
            id="tool_list"
        >
            <button
                className="drop_button"
                onClick={() =>  { this.showModal(this.getActiveParamsListForm()) }}
            >
                Стратегии торговли ({Object.keys(this.props.state.selectedTradeStrategy.items).length })
            </button>
            <div className="dropdown-content">
                { tradeStrategyOptions }
            </div>
            <Modal isOpen={this.state.modalOpen} onClose={() => this.closeModal()}>
                {this.state.modalContent}
            </Modal>
        </nav>
    }

    showModal(form: JSX.Element) {
        this.setState({
            modalOpen: true,
            modalContent: form
        })
    }

    closeModal() {
        this.setState({ modalOpen: false })
    }

    /**
     * @param {AbstractTradeStrategyParams} strategyParams
     */
    showAddStrategyModal(strategyParams: AbstractTradeStrategyParams) {
        const form = <form>{ StrategyFormGenerator.getAddFormElements(strategyParams, this) }</form>
        this.showModal(form)
    }

    /**
     * Добавление параметров стратегии
     * @param {AbstractTradeStrategyParams} tradeStrategyParams
     */
    addTradeStrategyParams(tradeStrategyParams: AbstractTradeStrategyParams) {
        this.props.addTradeStrategyList(tradeStrategyParams)
        this.props.refreshApp()
    }

    /**
     * Изменение параметров стратегии
     * @param {AbstractTradeStrategyParams} tradeStrategyParams
     */
    editTradeStrategyParams(tradeStrategyParams: AbstractTradeStrategyParams) {
        this.props.deleteTradeStrategyParams(this.state.selectedTradeStrategyParams)
        this.props.addTradeStrategyList(tradeStrategyParams)
        this.setState({selectedTradeStrategyParams: tradeStrategyParams.toJson()})
        this.props.refreshApp()
    }

    /**
     * @return {JSX.Element}
     */
    getActiveParamsListForm(){
        const activeParamsListOptions = Object.keys(this.props.state.selectedTradeStrategy.getItems())
          .map((tradeStrategyParams) => {
              return <option
                key={tradeStrategyParams}
                title={ tradeStrategyParams }>
                  { tradeStrategyParams }
              </option>
          })
        return <form onKeyDown={ this.onKeyDown() } style={{maxWidth: "1000px"}}>
            <select
              size={10}
              value={ this.state.selectedTradeStrategyParams }
              onChange={ this.getOnChange() }
              onDoubleClick={ this.getOnEdit() }
              style={{maxWidth: "100%"}}
            >
                <option disabled value="">Выберите параметры торговой стратегии:</option>
                {activeParamsListOptions}
            </select>
            <hr/>
            {
                activeParamsListOptions.length > 0
                &&
                this.state.selectedTradeStrategyParams !== ''
                &&
                <button type="button" onClick={ this.getOnEdit() }>Изменить</button>
            }
            <button type="button" onClick={ this.getOnDelete() }>Удалить</button>
        </form>
    }

    /**
     * @return {Function} callback при изменении выбора параметров торговой стратегии
     */
    getOnChange() {
        return (event: React.ChangeEvent<HTMLSelectElement>) => {
            this.setState({selectedTradeStrategyParams: event.target.value})
            this.showModal(this.getActiveParamsListForm())
        }
    }

    /**
     * @return {Function} функция изменения выбранных параметров торговой стратегии
     */
    getOnEdit() {
        return () => {
            if(this.state.selectedTradeStrategyParams.length > 0) {
                const selectedParamsJson = this.state.selectedTradeStrategyParams
                const selectedTradeStrategy = this.props.state.selectedTradeStrategy.jsonToItem(selectedParamsJson)
                this.showEditParamsModal(selectedTradeStrategy.getParams())
            }
        }
    }

    /**
     * @param {AbstractTradeStrategyParams} tradeStrategyParams
     */
    showEditParamsModal(tradeStrategyParams: AbstractTradeStrategyParams) {
        const form = <form>{ StrategyFormGenerator.getEditFormElements(tradeStrategyParams, this ) }</form>
        this.showModal(form)
    }

    deleteSelectedParams() {
        if(this.state.selectedTradeStrategyParams.length > 0){
            this.props.deleteTradeStrategyParams(this.state.selectedTradeStrategyParams)
            this.setState({selectedTradeStrategyParams: ''})
            this.showModal(this.getActiveParamsListForm())
            this.props.refreshApp()
        }
    }

    /**
     * @return {Function} функция удаления выбранных параметров стратегии торговли
     */
    getOnDelete() {
        return () => {
            this.deleteSelectedParams()
        }
    }

    /**
     * @return {Function}
     */
    onKeyDown() {
        return (event: React.KeyboardEvent) => {
            if(event.key === 'Delete'){
                this.deleteSelectedParams()
            }
        }
    }
}

export const ActiveTradeStrategyListComponent = connector(ActiveTradeStrategyList)
export type ActiveTradeStrategyListType = typeof ActiveTradeStrategyListComponent & ActiveTradeStrategyListInterface
