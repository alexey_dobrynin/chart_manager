import TradeAction from './trade_action'
import {TradeActionType} from './trade_action_type'

export default class TradeActionList {

    actionList: TradeAction[]

    constructor() {
        this.actionList = []
    }

    buy(dateTime: Date, price: number) {
        this.actionList.push(
            new TradeAction(dateTime, price, TradeActionType.Buy)
        )
    }

    sell(dateTime: Date, price: number) {
        this.actionList.push(
            new TradeAction(dateTime, price, TradeActionType.Sell)
        )
    }

    getActionList() {
        return this.actionList
    }

    getBuyList() {
        return this.actionList.filter((tradeAction) => {
            return tradeAction.getActionType() === TradeActionType.Buy
        })
    }

    getSellList() {
        return this.actionList.filter((tradeAction) => {
            return tradeAction.getActionType() === TradeActionType.Sell
        })
    }
}