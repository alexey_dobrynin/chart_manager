import ObjectList from '../object/object_list'
import MovingAverageCrossover from './strategy/moving_average_crossover'
import MovingAverageCrossoverAndDiff from './strategy/moving_average_crossover_and_diff'


/** Список торговых стратегий */
export default class TradeStrategyList extends ObjectList {

    movingAverageCrossover: MovingAverageCrossover

    movingAverageCrossoverAndDiff: MovingAverageCrossoverAndDiff

    constructor() {
        super()
        this.movingAverageCrossover = new MovingAverageCrossover()
        this.movingAverageCrossoverAndDiff = new MovingAverageCrossoverAndDiff()
    }

    /**
     * @return {TradeStrategy[]}
     */
    getItems() {
        return super.getItems()
    }

    /**
     * @return {Object<String, TradeStrategy>}
     */
    getAsObject() {
        return super.getAsObject()
    }

    /**
     * @param {String} tradeStrategyParamsJson
     * @return {TradeStrategy|undefined}
     */
    paramCodeToStrategy(tradeStrategyParamsJson: string)
    {
        const tradeStrategyParamsObject = JSON.parse(tradeStrategyParamsJson)
        for (const propertyName of Object.getOwnPropertyNames(this)){
            /** @type {TradeStrategy} */
            const tradeStrategy = this[propertyName]
            const tradeStrategyParams = tradeStrategy.params
            if(tradeStrategyParamsObject.code === tradeStrategyParams.code){
                tradeStrategy.initTradeStrategy(tradeStrategyParamsJson)
                return tradeStrategy
            }
        }
        return undefined
    }

    /**
     * @param {String} className
     * @return {TradeStrategy|undefined}
     */
    getTradeStrategyByClassName(className: string)
    {
        for (const propertyName of Object.getOwnPropertyNames(this)){
            /** @type {TradeStrategy} */
            const tradeStrategy = this[propertyName]

            if(tradeStrategy.constructor.name === className){
                return tradeStrategy
            }
        }
        return undefined
    }

    /**
     * @param {String} className
     * @return {TradeStrategy|undefined}
     */
    getTradeStrategyByParamsClassName(className: string)
    {
        for (const propertyName of Object.getOwnPropertyNames(this)){
            /** @type {TradeStrategy} */
            const tradeStrategy = this[propertyName]
            const tradeStrategyParams = tradeStrategy.getParams()
            if(tradeStrategyParams.constructor.name === className){
                return tradeStrategy
            }
        }
        return undefined
    }
}