import Option from '../option/option'
import {TradeActionType} from './trade_action_type'

/** Торговая операция */
export default class TradeAction {

    dateTime: Date

    price: number

    actionType: TradeActionType

    /**
     * @param {Date} dateTime
     * @param {Number} price
     * @param {String} actionType
     */
    constructor(dateTime: Date, price: number, actionType: TradeActionType) {
        this.dateTime = dateTime
        this.price = price
        this.actionType = actionType
    }

    /**
     * @return {Date}
     */
    getDateTime() {
        return this.dateTime
    }

    /**
     * @return {Number}
     */
    getPrice(){
        return this.price
    }

    /**
     * @return {String}
     */
    getActionType(): TradeActionType {
        return this.actionType
    }

    /**
     * @return {Option[]} список торговых операций
     */
    static getOptions() {
        return [
            new Option('Купить', TradeActionType.Buy),
            new Option('Продать', TradeActionType.Sell)
        ]
    }

}