import ChartState from '../chart/chart_state'

/** Котировка */
export default class Quote {

  /** @type {ChartState} */
  chartState: ChartState

  /** @type {String} */
  code: string

  /** @type {String} */
  name: string

  /**
   * @param {ChartState} chartState состояние графика
   * @param {String} code код котировки
   * @param {String} name название котировки
   */
  constructor(chartState: ChartState, code: string, name: string) {
    this.chartState = chartState
    this.code = code
    this.name = name
  }
}