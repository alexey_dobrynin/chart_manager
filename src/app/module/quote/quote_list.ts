import Quote from './quote'
import ListObject from '../list/list_object'
import ChartState from '../chart/chart_state'
import initFromObject from '../object/init_from_object'

export default class QuoteList extends ListObject{

  /**
   * @param {{id: number, code: string, name: string}[]} list
   */
  initItemsFromShortObjectList(list: {id: number, code: string, name: string}[])
  {
    this.items = list.map((item) => {
      const chartState = new ChartState()
      chartState.quote = String(item.id)
      return new Quote(chartState, item.code, item.name)
    })
  }

  /**
   * @param {{code: string, name: string, chartState: {}}} object
   * @return {Quote}
   */
  objectToItem(object: {code: string, name: string, chartState: {}}): Quote {
    const chartState = new ChartState()
    initFromObject(chartState, object.chartState)
    return new Quote(chartState, object.code, object.name)
  }

  /**
   * @return {Quote[]}
   */
  getItems(): Quote[] {
    return this.items
  }
}