export default class ObjectList{

  [key: string]: any

  getItems() {
    return Object.getOwnPropertyNames(this).map(
      (propertyName: string) => {
        return this[propertyName]
      })
  }

  getAsObject() {
    const object: Record<string, any> = {}
    for (const propertyName of Object.getOwnPropertyNames(this)){
      object[propertyName] = this[propertyName]
    }
    return object
  }
}