export default class ObjectKeyStructure {

  /** @type {Object} */
  items: Record<string, null> = {}

  /**
   * @param {Object} object
   */
  constructor(object: Record<string, null>) {
    this.initItemsFromObject(object)
  }

  /**
   * @param {Object} object
   */
  initItemsFromObject(object: Record<string, null>)
  {
    this.items = {}
    for (const objectProperty in object){
      if(object.hasOwnProperty(objectProperty)){
        this.items[objectProperty] = null
      }
    }
  }

  /**
   * @param {Object} object
   * @return {Object}
   */
  objectToItem(object: object): object
  {
    return object
  }

  /**
   * @return {Object}
   */
  getItems(): Record<string, any>
  {
    const object: Record<string, object> = {}
    for (const key in this.items){
      if(this.items.hasOwnProperty(key)) {
        object[key] = this.objectToItem(JSON.parse(key))
      }
    }
    return object
  }

}