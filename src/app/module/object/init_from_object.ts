/**
 * @param {Object} classObject
 * @param {Object} json
 */
import ListObject from '../list/list_object'
import ObjectKeyStructure from './object_key_structure'
import ObjectStructure from './object_structure'

export default function initFromObject(classObject: Record<string, any>, object: Record<string, any>) {
  const propertyNames = Object.getOwnPropertyNames(classObject)
  for (const propertyName of propertyNames) {
    if(propertyName in object)
    {
      if(typeof classObject[propertyName]  === 'object'){
        if(classObject[propertyName] instanceof ListObject){
          classObject[propertyName].initItemsFromObjectList(object[propertyName].items)
        } else if (classObject[propertyName] instanceof ObjectStructure) {
          classObject[propertyName].initItemsFromObject(object[propertyName].items)
        } else if(classObject[propertyName] instanceof ObjectKeyStructure){
          classObject[propertyName].initItemsFromObject(object[propertyName].items)
        } else {
          initFromObject(classObject[propertyName], object[propertyName])
        }
      } else {
        classObject[propertyName] = object[propertyName]
      }
    }
  }
}