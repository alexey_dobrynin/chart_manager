export default class ObjectStructure {

  /** @type {Object} */
  items: Record<string, any> = {}

  /**
   * @param {Object} object
   */
  constructor(object: Record<string, any>) {
    this.initItemsFromObject(object)
  }

  /**
   * @param {Object} object
   */
  initItemsFromObject(object: Record<string, any>)
  {
    this.items = {}
    for (const objectProperty in object){
      if(object.hasOwnProperty(objectProperty)){
        this.items[objectProperty] = object[objectProperty]
      }

    }
  }

  /**
   * @return {Object}
   */
  getItems()
  {
    return this.items
  }
}