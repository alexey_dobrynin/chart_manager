import BarPoint from './bar_point'
import TickLine from "../line/tick_line";

/** Точка на гистограмме со списком точек из линейного графика */
export default class BarTicksPoint extends BarPoint {

    /** @var {TickLine} */
    tickLine

    /**
     * @param {TickLine} tickLine список точек линейного графика
     * @param {String} dateTime дата и время в формате "YYYY-MM-DD hh:mm:ss.uuu"
     */
    constructor(tickLine: TickLine, dateTime: string) {
        super(
            tickLine.getMinValue(),
            tickLine.getMaxValue(),
            tickLine.getOpenValue(),
            tickLine.getCloseValue(),
            dateTime
        )
        this.tickLine = tickLine
    }

    /**
     * @return {TickLine}
     */
    getTickLine() {
        return this.tickLine
    }

    /**
     * @return {TickPoint[]} список точек
     */
    getTickPointList()
    {
        return this.tickLine.getPointList()
    }
}