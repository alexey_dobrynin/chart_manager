import AbstractPoint from './abstract_point'
import MathHelper from '../helper/math_helper'

/** Точка на графике гистограммы */
export default class BarPoint extends AbstractPoint{

    minValue: number
    maxValue: number
    openValue: number
    closeValue: number

    static type = Object.freeze({
        minValue: 'minValue',
        averageValue: 'averageValue',
        maxValue: 'maxValue',
        openValue: 'openValue',
        closeValue: 'closeValue'
    })


    /**
     * @param {Number} minValue минимальное значение
     * @param {Number} maxValue максимальное значение
     * @param {Number} openValue значение при открытии
     * @param {Number} closeValue значение при закрытии
     * @param {String} dateTime дата и время в формате "YYYY-MM-DD hh:mm:ss.uuu"
     */
    constructor(minValue: number, maxValue: number, openValue: number, closeValue: number, dateTime: string) {
        super(dateTime)
        this.minValue = minValue
        this.maxValue = maxValue
        this.openValue = openValue
        this.closeValue = closeValue
    }

    /**
     * @return {Number} минимальное значение
     */
    getMinValue(): number {
        return this.minValue
    }

    /**
     * @return {Number} максимальное значение
     */
    getMaxValue(): number {
        return this.maxValue
    }

    /**
     * @return {Number} значение открытия
     */
    getOpenValue() {
        return this.openValue
    }

    /**
     * @return {Number} значение закрытия
     */
    getCloseValue(): number {
        return this.closeValue
    }

    /**
     * @return {Number} среднее значение
     */
    getAverageValue(): number {
        return MathHelper.round((this.maxValue + this.minValue)/2, -4)
    }

    getValueByType(type: string) {
        switch (type) {
            case BarPoint.type.minValue:
                return this.getMinValue()
            case BarPoint.type.averageValue:
                return this.getAverageValue()
            case BarPoint.type.maxValue:
                return this.getMaxValue()
            case BarPoint.type.openValue:
                return this.getOpenValue()
            case BarPoint.type.closeValue:
                return this.getCloseValue()
        }
        return super.getValueByType(type)
    }
}