import AbstractPoint from './abstract_point'

/** Точка на линейном графике */
export default class TickPoint extends AbstractPoint {

    price: number

    /**
     * @param {Number} price цена
     * @param {String} dateTime дата и время в формате "YYYY-MM-DD hh:mm:ss.uuu"
     */
    constructor(price: string, dateTime: string) {
        super(dateTime)
        this.price = parseFloat(price)
    }

    /**
     * @return {Number} цена
     */
    getPrice() {
        return this.price
    }

    getAverageValue() {
        return this.price
    }

    getValueByType(type: string) {
        return this.price
    }
}