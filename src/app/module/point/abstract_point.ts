export default class AbstractPoint {

    dateTime

    /**
     * @param {String} dateTime дата и время в формате "YYYY-MM-DD hh:mm:ss.uuu"
     */
    constructor(dateTime: string) {
        this.dateTime = dateTime
    }

    /**
     * @return {String} dateTime дата и время в формате "YYYY-MM-DD hh:mm:ss.uuu"
     */
    getDateTime(): string {
        return this.dateTime
    }

    /**
     * @return {Number} милисекунды unix-time
     */
    getUnixTime(): number {
        const date = this.getDate()
        return date.getTime()
    }

    /**
     * @return {Date}
     */
    getDate(): Date {
        const dateInfo = this.dateTime.split('.')
        const date = new Date(dateInfo[0])
        if(dateInfo.length > 1){
            date.setMilliseconds(parseInt(dateInfo[1]))
        }
        return date
    }

    /**
     * @param {String} type тип значения
     * @return {Number}
     */
    getValueByType(type: string): number {
        return NaN
    }

    /**
     * @return {Number} среднее значение
     */
    getAverageValue(): number {
        return NaN
    }
}