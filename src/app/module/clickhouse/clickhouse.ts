// Запрос в clickhouse
export default class Clickhouse {

    requestUrl: string
    user: string
    database: string

    /**
     * @param {String} requestUrl url-запрос
     * @param {String} database название базы данных
     * @param {String} user имя пользователя
     */
    constructor(requestUrl: string, database = 'database',  user = 'user') {
        this.requestUrl = requestUrl
        this.database = database
        this.user = user
    }

    /**
     * @param {String} query SQL-запрос
     * @param {Object} params параметры запроса
     * @return {Promise}
     */
    select(query:string, params: Record<string, any> = {}) {
        const defaultParams: Record<string, any> = {
            "add_http_cors_header": 1,
            "log_queries": 1,
            "output_format_json_quote_64bit_integers": 1,
            "output_format_json_quote_denormals": 1,
            "result_overflow_mode": "throw",
            "max_result_rows": 100000,
            "timeout_overflow_mode": "throw",
            "max_execution_time": 60
        }

        for (let paramName in defaultParams){
            if(!(paramName in params)){
                if(defaultParams.hasOwnProperty(paramName)){
                    params[paramName] = defaultParams[paramName]
                }
            }
        }

        let url = `${this.requestUrl}?`
        for (let paramName in params){
            if(params.hasOwnProperty(paramName)) {
                url += `${paramName}=${params[paramName]}&`
            }
        }
        url += `user=${this.user}&database=${this.database}`
        return Clickhouse.postData(url, `${query} FORMAT JSON`)
    }

    static async postData(url: string, data: string) {
        const response = await fetch(url, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: data
        });
        return await response.json(); // parses JSON response into native JavaScript objects
    }
}