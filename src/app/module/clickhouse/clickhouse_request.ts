import {Scale} from '../scale/scale'
import Clickhouse from './clickhouse'

class ClickhouseRequest {

    clickhouse

    /**
     * @param {Clickhouse} clickhouse
     */
    constructor(clickhouse: Clickhouse) {
        this.clickhouse = clickhouse
    }

    /**
     * @param {String} scaleName масштаб
     * @param {Number} quoteId идентификатор котировки
     * @param {String} fromDateTime с даты (формат YYYY-MM-DD hh:mm:ss)
     * @param {String} toDateTime по дату (формат YYYY-MM-DD hh:mm:ss)
     * @return {String} SQL - запрос получения графика платежей
     */
    getChartDataQuery(scaleName: string, quoteId: number, fromDateTime: string, toDateTime: string){
        const scaleDateTime = Scale.dateTime[scaleName]

        return `
            SELECT
              toDateTime(${scaleDateTime}) as date_time,
              MIN(toFloat64(price)) as min_price,
              MAX(toFloat64(price)) as max_price,
              first_value(toFloat64(price)) as open_price,
              last_value(toFloat64(price)) as close_price
            FROM chart.price_history as ph
            WHERE
                ph.quote_id = ${quoteId}
                AND time BETWEEN '${ fromDateTime }' AND '${ toDateTime }'
            GROUP BY date_time
            ORDER BY date_time`
    }

    /**
     * @param {String} scaleName масштаб
     * @param {String} quoteId идентификатор котировки
     * @param {String} fromDateTime с даты (формат YYYY-MM-DD hh:mm:ss)
     * @param {String} toDateTime по дату (формат YYYY-MM-DD hh:mm:ss)
     * @return {Promise} промис запроса на получение данных
     */
    getChartData(scaleName: string, quoteId: number, fromDateTime: string, toDateTime: string){
        return this.clickhouse.select(
            this.getChartDataQuery(scaleName, quoteId, fromDateTime, toDateTime)
        )
    }

    /**
     * @param {Number} quoteId идентификатор котировки
     * @param {String} fromDateTime с даты (формат YYYY-MM-DD hh:mm:ss)
     * @param {String} toDateTime по дату (формат YYYY-MM-DD hh:mm:ss)
     * @return {String} SQL - запрос получения сырых данных за указанный период
     */
    getTickDataQuery(quoteId: number, fromDateTime: string, toDateTime: string) {

        return `
            WITH toString( (num - 1) * FLOOR( 1000 / size) ) as milliSeconds
            SELECT 
                CONCAT(
                    formatDateTime(
                        time,
                        '%Y-%m-%d %H:%M:%S.'
                    ),
                    repeat('0', toUInt64(( 3 - length( milliSeconds )) )),
                    milliSeconds
                ) as time, priceList as price 
            FROM (
                SELECT time, COUNT(price) size, groupArray(price) as priceList
                FROM chart.price_history
                WHERE quote_id = ${ quoteId }
                AND time BETWEEN '${ fromDateTime }' AND '${ toDateTime }'
                GROUP BY time
                ORDER BY time
            ) as t
            ARRAY JOIN priceList,
            arrayEnumerate(priceList) as num`
    }

    /**
     * @param {String} quoteId идентификатор котировки
     * @param {String} fromDateTime с даты (формат YYYY-MM-DD hh:mm:ss)
     * @param {String} toDateTime по дату (формат YYYY-MM-DD hh:mm:ss)
     * @return {Promise} промис запроса на получение тиковых данных
     */
    getTickData(quoteId: string, fromDateTime: string, toDateTime: string) {
        return this.clickhouse.select(
            this.getTickDataQuery(parseInt(quoteId), fromDateTime, toDateTime)
        )
    }

    /**
     * @return {Promise} промис запроса на получение списка котировок
     */
    getQuoteList() {
        return this.clickhouse.select(
          'SELECT `id`, `code`, `name` FROM quote'
        )
    }

    /**
     * @param {Number} quoteId идентификатор котировки
     * @return {Promise} промис запроса на получение периода действия котировоки
     */
    getQuotePeriod(quoteId: number) {
        return this.clickhouse.select(
          `SELECT MIN(ph.time) fromDateTime, MAX(ph.time) toDateTime
           FROM price_history ph 
           WHERE quote_id = ${ quoteId }`)
    }
}

export default ClickhouseRequest