import React from 'react'

export default function Modal(props: {children: JSX.Element | null, onClose: () => void, isOpen: boolean}) {

    const close =
        ((e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
            e.preventDefault()

            if (props.onClose) {
                props.onClose()
            }
        })

    if (!props.isOpen)
        return null

    return (
        <div key="modal-container">
            <div key="modal-window" className="modal-window">
                {props.children}
            </div>
            <div key="background-modal" className="background-modal" onClick={e => close(e)}/>
        </div>
    )
}