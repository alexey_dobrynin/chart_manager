import MovingAverageSimpleFilter from './moving_average_simple_filter'
import MovingAverageWeightedFilter from './moving_average_weighted_filter'
import MovingAverageExponentialFilter from './moving_average_exponential_filter'
import Option from '../option/option'
import MovingAverageFilter from "./moving_average_filter";

export default class MovingAverageFilterType {

    /**
     * @return {Readonly<Object<String, MovingAverageFilter>>}
     */
    static getFilterObjectList(){
        const objectList: Record<string, MovingAverageFilter> = {}
        objectList[MovingAverageSimpleFilter.name] = new MovingAverageSimpleFilter(1, 1)
        objectList[MovingAverageWeightedFilter.name] = new MovingAverageWeightedFilter(1, 1)
        objectList[MovingAverageExponentialFilter.name] = new MovingAverageExponentialFilter(1, 1)
        return Object.freeze(objectList)
    }

    /**
     * @return {Readonly<Object<String, String>>}
     */
    static getTypeList() {
        const typeList: Record<string, string> = {}
        for (const filterName of Object.keys(MovingAverageFilterType.getFilterObjectList())){
            typeList[filterName] = filterName
        }
        return Object.freeze(typeList)
    }

    /**
     * @return {Option[]}
     */
    static getOptions() {
        return [
            new Option('Простая', MovingAverageSimpleFilter.name),
            new Option('Взвешеная', MovingAverageWeightedFilter.name),
            new Option('Экспоненциальная', MovingAverageExponentialFilter.name)
        ]
    }
}