import TickPoint from '../point/tick_point';

export default class MovingAverageCalculator {

    /**
     * @param {TickPoint[]} periodPointList
     * @return {Number}
     */
    static simple(periodPointList: TickPoint[]){
        let priceSum = 0
        for (const point of periodPointList) {
            priceSum += point.getPrice()
        }
        return periodPointList.length > 0 ? priceSum/periodPointList.length : 0
    }

    /**
     * @param {TickPoint[]} periodPointList
     * @return {Number}
     */
    static weighted(periodPointList: TickPoint[]){
        let priceSum = 0
        let weightSum = 0
        for (const index in periodPointList) {
            const point = periodPointList[index]
            const weight = parseInt(index) + 1
            const price = point.getPrice()
            priceSum += price * weight
            weightSum += weight
        }
        return weightSum > 0 ? priceSum/weightSum : 0
    }

    /**
     * @param {String} period
     * @return {Function}
     */
    static exponential(period: number) {
        const smoothingConstant = 2 / (period + 1)
        return (periodPointList: TickPoint[], lastFilteredPoint: TickPoint | undefined) => {
            let exponentialMovingAverage
            if(lastFilteredPoint !== undefined) {
                const constant = smoothingConstant
                const lastPoint = periodPointList[periodPointList.length - 1]
                const lastPointPrice = lastPoint.getPrice()
                const previousExponentialMovingAverage = lastFilteredPoint.getPrice()
                exponentialMovingAverage = lastPointPrice * constant + (1 - constant) * previousExponentialMovingAverage
            } else {
                let priceSum = 0
                for (const point of periodPointList) {
                    priceSum += point.getPrice()
                }
                exponentialMovingAverage = periodPointList.length > 0 ? priceSum / periodPointList.length : 0
            }

            return exponentialMovingAverage
        }
    }
}