import TickPoint from '../point/tick_point'
import MathHelper from '../helper/math_helper'
import DateTimeHelper from '../helper/date_time_helper'

/** Преобразование через фильтр скользящей средней */
export default class MovingAverageFilter {

    period: number
    step: number

    /**
     * @param {Number} period период (в шагах)
     * @param {Number} step шаг (в секундах)
     */
    constructor(period: number, step: number) {
        this.period = period
        this.step = step
    }

    filter(tickPointList: TickPoint[]) {
        /** @type {TickPoint[]} */
        let filteredPointList = []
        if(tickPointList.length > 0) {
            const startPeriodDate = tickPointList[0].getDate()
            let startPeriodUnixTime = startPeriodDate.getTime()
            const stepMilliSeconds = this.step * 1000
            const periodMilliSeconds = stepMilliSeconds * this.period
            let endPeriodUnixTime = startPeriodUnixTime + periodMilliSeconds
            let periodPointList: TickPoint[] = []
            let pointDateUnixTime = 0

            for (const point of tickPointList) {
                const pointDate = point.getDate()
                pointDateUnixTime = pointDate.getTime()

                if(pointDateUnixTime < endPeriodUnixTime){
                    periodPointList.push(point)
                } else {
                    const addPeriodDelta = Math.max(
                        pointDateUnixTime - endPeriodUnixTime, stepMilliSeconds
                    )
                    startPeriodUnixTime += addPeriodDelta
                    endPeriodUnixTime += addPeriodDelta
                    periodPointList.push(point)
                    periodPointList = this.refreshPeriodPointList(periodPointList, startPeriodUnixTime)
                }
                const lastFilteredPoint = filteredPointList.length > 0
                    ? filteredPointList[filteredPointList.length - 1]
                    : undefined;
                const movingAverage = this.calculateMovingAverage(periodPointList, lastFilteredPoint)
                filteredPointList.push(
                    new TickPoint(
                        MathHelper.round(movingAverage, -4).toString(),
                        DateTimeHelper.unixToDateTimeMillisecondsFormat(pointDateUnixTime)
                    )
                )
            }
        }
        return filteredPointList
    }

    calculateMovingAverage(periodPointList: TickPoint[], lastFilteredPoint: TickPoint | undefined)
    {
        return 0
    }

    refreshPeriodPointList(periodPointList: TickPoint[], startPeriodUnixTime: number){
        let removePoints = 0
        for(let position = 0; position < periodPointList.length; position++){
            const point = periodPointList[position]
            if(point.getDate().getTime() < startPeriodUnixTime){
                removePoints++
            } else {
                break
            }
        }
        return periodPointList.slice(removePoints)
    }

    /**
     * @param {Number} period период (в шагах)
     * @param {Number} step шаг (в секундах)
     * @return {MovingAverageFilter}
     */
    clone(period: number, step: number)
    {
        const movingAverageFilter = Object.assign(Object.create(Object.getPrototypeOf(this)), this)
        movingAverageFilter.period = period
        movingAverageFilter.step = step
        return movingAverageFilter
    }
}