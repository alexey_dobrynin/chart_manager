import Option from '../option/option'

/** Типы скользящих средних */
export default class MovingAverageType {

    static typeList = Object.freeze({
        movingAverageSimple: 'movingAverageSimple',
        movingAverageWeighted: 'movingAverageWeighted',
        movingAverageExponential: 'movingAverageExponential'
    })

    /**
     * @return {Option[]} список опций типов скользящих средних
     */
    static getOptions()
    {
        return [
            new Option('Простая скользящая средняя', MovingAverageType.typeList.movingAverageSimple),
            new Option('Взвешенная скользящая средняя', MovingAverageType.typeList.movingAverageWeighted),
            new Option('Экспоненциальная скользящая средняя', MovingAverageType.typeList.movingAverageExponential)
        ]
    }
}