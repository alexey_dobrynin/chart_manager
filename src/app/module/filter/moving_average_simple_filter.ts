import MovingAverageFilter from './moving_average_filter'
import MovingAverageCalculator from './moving_average_calculator'
import TickPoint from "../point/tick_point";

/** Преобразование через фильтр простой скользящей средней */
export default class MovingAverageSimpleFilter extends MovingAverageFilter {

    /**
     * @param {Number} period период (в шагах)
     * @param {Number} step шаг (в секундах)
     */
    constructor(period: number, step: number) {
        super(period, step);
    }

    calculateMovingAverage(periodPointList: TickPoint[], lastFilteredPoint: TickPoint | undefined) {
        return MovingAverageCalculator.simple(periodPointList)
    }
}