import MovingAverageCalculator from './moving_average_calculator'
import MovingAverageFilter from './moving_average_filter'
import TickPoint from '../point/tick_point'

/** Преобразование через фильтр экспоненциальной скользящей средней */
export default class MovingAverageExponentialFilter extends MovingAverageFilter {

    /** @type {Function} */
    exponentialCalculator: (periodPointList: TickPoint[], lastFilteredPoint: TickPoint | undefined) => number

    /**
     * @param {Number} period период (в шагах)
     * @param {Number} step шаг (в секундах)
     */
    constructor(period: number, step: number) {
        super(period, step)
        this.exponentialCalculator = MovingAverageCalculator.exponential(this.period)
    }

    calculateMovingAverage(periodPointList: TickPoint[], lastFilteredPoint: TickPoint | undefined) {
        return this.exponentialCalculator(periodPointList, lastFilteredPoint)
    }

    clone(period: number, step: number) {
        const cloneObject = super.clone(period, step)
        cloneObject.exponentialCalculator = MovingAverageCalculator.exponential(period)
        return cloneObject
    }
}