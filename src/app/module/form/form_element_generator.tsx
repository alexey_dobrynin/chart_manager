import React from 'react'
import Header from './header'
import IntParameter from './parameter/int_parameter'
import SelectParameter from './parameter/select_parameter'
import DateTimeParameter from './parameter/datetime_parameter'
import ColorParameter from './parameter/color_parameter'
import Button from './button'
import HorizontalRule from './horizontal_rule'
import AbstractFormElement from './abstract_form_element'
import MultiselectParameter from './parameter/multiselect_parameter'

/** Генератор элементаов формы */
export default class FormElementGenerator {

  /**
   * @param {AbstractFormElement} formElement
   * @return {Function}
   */
  static getTypeToGetTemplateFunction(formElement: AbstractFormElement) {
    const typeToGetTemplateFunction: Record<string, any> = {}
    typeToGetTemplateFunction[Header.name] = FormElementGenerator.headerToH
    typeToGetTemplateFunction[IntParameter.name] = FormElementGenerator.intToInput
    typeToGetTemplateFunction[SelectParameter.name] = FormElementGenerator.selectToDropdownList
    typeToGetTemplateFunction[MultiselectParameter.name] = FormElementGenerator.selectToMultiselectList
    typeToGetTemplateFunction[DateTimeParameter.name] = FormElementGenerator.dateTimeToInput
    typeToGetTemplateFunction[ColorParameter.name] = FormElementGenerator.colorToInput
    typeToGetTemplateFunction[Button.name] = FormElementGenerator.buttonToAction
    typeToGetTemplateFunction[HorizontalRule.name] = FormElementGenerator.horizontalRuleToHr
    return typeToGetTemplateFunction[formElement.constructor.name]
  }

  /**
   * @param {Header} formElement
   * @return {JSX.Element}
   */
  static headerToH(formElement: Header) {
    const formElementText = formElement.getText()
    const formElementSize = formElement.getSize()
    const formElementSection = formElement.getSection()
    const formElementKey = `h${formElementSize}-${formElementText}_${formElementSection}`
    switch (formElementSize){
      case 1:
        return <h1 key={ formElementKey }>{ formElementText }</h1>
      case 2:
        return <h2 key={ formElementKey }>{ formElementText }</h2>
      case 3:
        return <h3 key={ formElementKey }>{ formElementText }</h3>
      case 4:
        return <h4 key={ formElementKey }>{ formElementText }</h4>
      case 5:
        return <h5 key={ formElementKey }>{ formElementText }</h5>
      case 6:
        return <h6 key={ formElementKey }>{ formElementText }</h6>
    }
  }

  /**
   * @param {IntParameter} formElement
   * @return {JSX.Element}
   */
  static intToInput(formElement: IntParameter) {
    return <input
      key={`int_${formElement.getDescription()}_${formElement.getSection()}`}
      type="number"
      name={ formElement.getName() }
      placeholder={ formElement.getDescription() }
      min={ formElement.getMin()}
      step={ formElement.getStep() }
      defaultValue={ formElement.getValue() }
      title={ formElement.getDescription() }
      onChange={ formElement.getOnChange() }
    />
  }

  /**
   * @param {SelectParameter} formElement
   * @return {JSX.Element}
   */
  static selectToDropdownList(formElement: SelectParameter) {
    const options = formElement.getOptions().map((option) => {
      return <option
        key={ `option_${option.getValue()}`}
        title={ option.getName() }
        value={ option.getValue() }
      >{option.getName()}</option>
    })

    return <select
      key={ `select_${formElement.getDescription()}_${formElement.getSection()}}`}
      name={ formElement.getName() }
      defaultValue={ formElement.getValue() }
      title={ formElement.getDescription() }
      onChange={ formElement.getOnChange() }
      style={{maxWidth: "1000px"}}
    >
      <option disabled value="">{ formElement.getDescription() }</option>
      { options }
    </select>
  }

  /**
   * @param {SelectParameter} formElement
   * @return {JSX.Element}
   */
  static selectToMultiselectList(formElement: SelectParameter) {
    const options = formElement.getOptions().map((option) => {
      return <option
          key={ `option_${option.getValue()}`}
          title={ option.getName() }
          value={ option.getValue() }
      >{option.getName()}</option>
    })

    return <select
        multiple
        key={ `select_${formElement.getDescription()}_${formElement.getSection()}}`}
        name={ formElement.getName() }
        defaultValue={ JSON.parse(formElement.getValue()) }
        title={ formElement.getDescription() }
        onChange={ formElement.getOnChange() }
        style={{maxWidth: "1000px"}}
    >
      <option disabled value="">{ formElement.getDescription() }</option>
      { options }
    </select>
  }

  /**
   * @param {DateTimeParameter} formElement
   * @return {JSX.Element}
   */
  static dateTimeToInput( formElement: DateTimeParameter ) {
    return <input
      key={`date_${formElement.getDescription()}_${formElement.getSection()}`}
      type="datetime-local"
      name={ formElement.getName() }
      placeholder={ formElement.getDescription() }
      defaultValue={ formElement.getValue() }
      title={ formElement.getDescription() }
      step="1"
      onChange={ formElement.getOnChange() }
    />
  }

  /**
   * @param {ColorParameter} formElement
   * @return {JSX.Element}
   */
  static colorToInput( formElement: ColorParameter ) {
    return <input
      key={`color_${formElement.getDescription()}_${formElement.getSection()}`}
      type="color"
      name={ formElement.getName() }
      placeholder={ formElement.getDescription() }
      defaultValue={ formElement.getValue() }
      title={ formElement.getDescription() }
      onChange={ formElement.getOnChange() }
    />
  }

  /**
   * @param {Button} formElement
   * @return {JSX.Element}
   */
  static buttonToAction( formElement: Button ) {
    return <button
      key={`button_${formElement.getText()}_${formElement.getSection()}`}
      onClick={ formElement.getOnclick() }
      type="button"
    >
      { formElement.getText() }
    </button>
  }

  /**
   * @param {HorizontalRule} formElement
   * @return {JSX.Element}
   */
  static horizontalRuleToHr(formElement: HorizontalRule) {
    return <hr key={`hr_${formElement.getSection()}`}/>
  }
}