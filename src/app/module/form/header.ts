import AbstractFormElement from './abstract_form_element'

export default class Header extends AbstractFormElement {

    text: string = ''

    size: number = 1

    /**
     * @param {String} params.text текст заголовка
     * @param {Number|undefined} params.size размер заголовка (от 1 до 6)
     */
    constructor(params: {text: string, size?: number, section?: string}) {
        super(params)
        this.text = params.text
        if(
            this.size === undefined
            ||
            (
                (
                    this.size > 6
                    ||
                    this.size < 1
                )
            )
        ){
            this.size = 1
        } else {
            this.size = <number>params.size
        }
    }

    /**
     * @return {String} текст заголовка
     */
    getText() {
        return this.text
    }

    /**
     * @return {Number} текст заголовка
     */
    getSize() {
        return this.size
    }

}