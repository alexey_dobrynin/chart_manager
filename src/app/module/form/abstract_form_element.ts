export default class AbstractFormElement{

    [key: string]: any

    section: string = ''

    /**
     * @param {String|undefined} params.section название раздела заголовка
     */
    constructor(params?: {section?: string}) {
        if(params !== undefined && params.section !== undefined){
            this.section = params.section
        }
    }

    /**
     * @return {String} название раздела заголовка
     */
    getSection() {
        return this.section
    }
}