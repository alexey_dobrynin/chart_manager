import React from 'react'
import AbstractFormElement from './abstract_form_element'

export default class Button extends AbstractFormElement {

    text: string = ''

    onClick: React.MouseEventHandler<HTMLButtonElement> = () => {}

    /**
     * @param {String} params.text надпись на кнопке
     * @param {Function} params.onClick действие по нажатию кнопки
     */
    constructor(params: {text: string, onClick: () => void, section?: string}) {
        super(params);
        this.text = params.text
        this.onClick = params.onClick
    }

    /**
     * @return {String} надпись на кнопке
     */
    getText() {
        return this.text
    }

    /**
     * @return {Function} действие по нажатию кнопки
     */
    getOnclick() {
        return this.onClick
    }
}