import AbstractParameter from './abstract_parameter'
import React from 'react'

export default class IntParameter extends AbstractParameter {

    min: number = 0

    step: number = 1

    /**
     * @param {String} params.name название параметра
     * @param {String} params.value значение параметра
     * @param {String} params.description описание параметра
     * @param {Function} params.onChange callback срабатывающий при изменении значения
     * @param {Number} params.min минимальное значение
     * @param {Number} params.step шаг
     */
    constructor(params: {
        name: string,
        value: string,
        description: string,
        onChange: (event: React.ChangeEvent<HTMLInputElement>) => void,
        min: number,
        step: number,
        section?: string
    }) {
        super(params)
        this.min = params.min
        this.step = params.step
    }

    /**
     * @return {Number} минимальное значение
     */
    getMin() {
        return this.min;
    }

    /**
     * @return {Number} шаг
     */
    getStep() {
        return this.step
    }
}