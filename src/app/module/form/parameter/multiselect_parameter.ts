import AbstractParameter from './abstract_parameter'
import Option from '../../option/option'
import React from 'react'

/** Выбор нескольких вариантов из списка */
export default class MultiselectParameter extends AbstractParameter {

    options: Option[] = []

    /**
     * @param {String} params.name название параметра
     * @param {String} params.value значения выбранных параметров в виде json
     * @param {String} params.description описание параметра
     * @param {Function} params.onChange callback срабатывающий при изменении значения
     * @param {Option[]} params.options список опций
     */
    constructor(params: {
        name: string,
        value: string,
        description: string,
        onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void,
        options: Option[],
        section?: string
    }) {
        super(params)
        this.options = params.options
    }

    /**
     * @return {Option[]} список опций
     */
    getOptions(): Option[] {
        return this.options
    }
}