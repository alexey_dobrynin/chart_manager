import AbstractFormElement from '../abstract_form_element'

export default class AbstractParameter extends AbstractFormElement {

    name: string = ''

    value: string = ''

    description: string = ''

    onChange?: (event: any) => void

    /**
     * @param {String} params.name название параметра
     * @param {String} params.value значение параметра
     * @param {String} params.description описание параметра
     * @param {Function} params.onChange callback срабатывающий при изменении значения
     */
    constructor(params: {name: string, value: string, description: string, onChange: (event: any) => void, section?: string}) {
        super(params)
        this.name = params.name
        this.value = params.value
        this.description = params.description
        this.onChange = params.onChange
    }

    /**
     * @return {String} название параметра
     */
    getName() {
        return this.name
    }

    /**
     * @return {String} значение параметра
     */
    getValue() {
        return this.value
    }

    /**
     * @return {String} описание параметра
     */
    getDescription() {
        return this.description
    }

    getOnChange() {
        return this.onChange
    }
}