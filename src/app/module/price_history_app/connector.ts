import {connect, ConnectedProps} from 'react-redux'
import PriceHistoryAppState from './price_history_app_state'
import PriceHistoryActions from './price_history_actions'

export const connector = connect(
    (state: PriceHistoryAppState) => ({state: state}),
    dispatch => PriceHistoryActions.getActions(dispatch)
)

type PropsFromRedux = ConnectedProps<typeof connector>

export type PriceHistoryAppConnectorProps = PropsFromRedux & {
    state: PriceHistoryAppState
}

export type PriceHistoryAppChildRefresh = PriceHistoryAppConnectorProps & {
    refreshApp: () => void
}