import React from 'react'
import {isset} from '../helper/php_functions'
import {Scale} from '../scale/scale'
import {ActiveToolListComponent} from './active_tool_list'
import {ChartStateBookmarksComponent} from '../bookmark/chart_state_bookmarks'
import {ActiveTradeStrategyListComponent} from '../trade/active_trade_strategy_list'
import {connector, PriceHistoryAppChildRefresh} from './connector'

type ChartPanelAppProps = PriceHistoryAppChildRefresh & {
  setQuoteId: (quoteId: string) => void
}

class ChartPanelApp extends React.Component<ChartPanelAppProps> {

  /**
   * @param {Function} props.setQuoteId
   * @param {Function} props.refreshApp
   * @return {JSX.Element}
   * @constructor
   */
  constructor(props: ChartPanelAppProps) {
    super(props)
  }

  selectQuote() {
    return (event: React.ChangeEvent<HTMLSelectElement>) => {
      const selectedQuoteId = event.target.value
      this.props.setQuoteId(selectedQuoteId)
    }
  }

  renderSelectQuote() {
    let quoteOptions = this.props.state.quoteList.getItems().map((quote) => {
      return (
        <option key={ quote.chartState.quote } value={ quote.chartState.quote }>
          { quote.code }_{ quote.name }
        </option>
      )
    })
    return <select
      value={ this.props.state.chartState.quote }
      onChange={ this.selectQuote() }
    >{ quoteOptions }</select>
  }

  renderDateTime() {
    const chartState = this.props.state.chartState
    const [date, time] = typeof chartState.dateTime === "string"
      ? chartState.dateTime.split(' ')
      : ["", ""]
    const [fromDate, fromTime] = typeof chartState.fromDateTime === "string"
      ? chartState.fromDateTime.split(' ')
      : ["", ""]
    const [toDate, toTime] = typeof chartState.toDateTime === "string"
      ? chartState.toDateTime.split(' ')
      : ["", ""]
    const minTime = date === fromDate ? fromTime : '00:00:00'
    const maxTime = date === toDate ? toTime : '23:59:59'
    const value = isset(date, time) ? `${date}T${time}` : ''
    const minValue = isset(fromDate, minTime) ? `${fromDate}T${minTime}` : ''
    const maxValue = isset(toDate, maxTime) ? `${toDate}T${maxTime}` : ''

    return (
      <span>
          <input
            type="datetime-local"
            value={ value }
            min={ minValue }
            max={ maxValue }
            step="1"
            onChange={ this.dateTimeOnSelect() }
          />
      </span>
    )
  }

  startDateOnClick() {
    return (() => {
      const chartState = this.props.state.chartState
      if (isset(chartState.quote, chartState.fromDateTime)) {
        this.props.setChartStateDatetime(chartState.quote, chartState.fromDateTime)
        this.props.refreshApp()
      }
    });
  }

  lastDateOnClick() {
    return (() => {
      const chartState = this.props.state.chartState
      if(isset(chartState.quote, chartState.fromDateTime, chartState.toDateTime, chartState.scale)){
        const datetime = chartState.getLastDatetime()
        this.props.setChartStateDatetime(chartState.quote, datetime)
        this.props.refreshApp()
      }
    })
  }

  dateTimeOnSelect() {
    return ((event: React.ChangeEvent<HTMLInputElement>) => {
      const chartState = this.props.state.chartState
      if(isset(chartState.quote, event.target.value)){
        const datetime = event.target.value.split('T').join(' ')
        this.props.setChartStateDatetime(chartState.quote, datetime)
        this.props.refreshApp()
      }
    })
  }

  previousDateOnClick() {
    return (() => {
      const chartState = this.props.state.chartState
      if(isset(chartState.quote, chartState.dateTime, chartState.fromDateTime, chartState.scale)){
        const datetime = chartState.getPreviousDatetime()
        this.props.setChartStateDatetime(chartState.quote, datetime)
        this.props.refreshApp()
      }
    })
  }

  nextDateOnClick() {
    return (() => {
      const chartState = this.props.state.chartState
      if(isset(chartState.quote, chartState.dateTime, chartState.toDateTime, chartState.scale)){
        const datetime = chartState.getNextDatetime()
        this.props.setChartStateDatetime(chartState.quote, datetime)
        this.props.refreshApp()
      }
    })
  }

  renderSelectScale() {
    let scaleOptions = Object.keys(Scale.code).map((scaleKey) => {
      return <option key={scaleKey} value={ scaleKey }>{ scaleKey }</option>
    })
    return <select
      id="chart_scale"
      value={ this.props.state.chartState.scale }
      onChange={ this.scaleOnSelect() }
    >{ scaleOptions }</select>
  }

  scaleOnSelect() {
    return ((event: React.ChangeEvent<HTMLSelectElement>) => {
      const scale = event.target.value
      const chartState = this.props.state.chartState
      const quoteId = chartState.quote
      this.props.setChartStateScale(quoteId, scale)
      const datetime = this.props.state.chartState.getDateOnScaleChange()
      this.props.setChartStateDatetime(chartState.quote, datetime)
      this.props.refreshApp()
    })
  }

  render() {
    return (
      <div className="chart_panel">
        { this.renderSelectQuote() }
        <a className="icon-to-start-alt"
           title="go to start date"
           onClick={ this.startDateOnClick() } />
        <a className="icon-to-start"
           title="go to previous period"
           onClick={ this.previousDateOnClick() } />
        { this.renderDateTime() }
        <a className="icon-to-end"
           title="go to next period"
           onClick={ this.nextDateOnClick() } />
        <a className="icon-to-end-alt"
           title="go to last date"
           onClick={ this.lastDateOnClick() } />
        { this.renderSelectScale() }
        <ActiveToolListComponent refreshApp={ this.props.refreshApp } />
        <ActiveTradeStrategyListComponent refreshApp={ this.props.refreshApp } />
        <ChartStateBookmarksComponent refreshApp={ this.props.refreshApp } />
      </div>
    )
  }
}

export const ChartPanelAppComponent = connector(ChartPanelApp)