import React from 'react'
import {ChartPanelAppComponent} from './chart_panel_app'
import QuoteList from '../quote/quote_list'
import {Scale} from '../scale/scale'
import ChartDataGetter from '../chart/chart_data_getter'
import ChartOptions from '../chart/chart_options'
import TickLine from '../line/tick_line'
import ReactECharts from 'echarts-for-react'
import TickPointListGetter from '../chart/tick_point_list_getter'
import BarTicksLine from '../line/bar_ticks_line'
import DateTimeHelper from '../helper/date_time_helper'
import {isset} from '../helper/php_functions'
import ClickhouseRequest from '../clickhouse/clickhouse_request'
import {PriceHistoryAppConnectorProps, connector} from './connector'


type PriceHistoryAppProps = PriceHistoryAppConnectorProps & {
  clickhouseRequest: ClickhouseRequest
}

interface PriceHistoryAppStateInterface {
  chartOptions?: ChartOptions
}

export interface PriceHistoryAppInterface {
  props: PriceHistoryAppProps
  refreshApp: () => void
}

class PriceHistoryApp extends React.Component<PriceHistoryAppProps, PriceHistoryAppStateInterface> implements PriceHistoryAppInterface {

  /**
   * @param {ClickhouseRequest} props.clickhouseRequest
   * @param {PriceHistoryAppState} props.state
   * @return {JSX.Element}
   * @constructor
   */
  constructor(props: PriceHistoryAppProps) {
    super(props)
    this.state = {
      chartOptions: undefined
    }
  }

  componentDidMount() {
    this.loadQuoteList()
  }

  loadQuoteList() {
    this.props.clickhouseRequest.getQuoteList().then((response) => {
      const quoteList = new QuoteList([])
      quoteList.initItemsFromShortObjectList(response.data)
      this.props.setQuoteList(quoteList)
      if(this.props.state.chartState.quote === undefined && quoteList.getItems().length > 0){
        const firstQuote = quoteList.getItems()[0]
        this.setQuoteId()(firstQuote.chartState.quote)
      }
      this.refreshApp()()
    })
  }

  refreshApp(){
    return (() => {
      this.changeChart()
    })
  }

  setQuoteId() {
    return ((quoteId: string) => {
      this.props.setQuote(quoteId)
      const quoteList = this.props.state.quoteList.getItems().filter((quote) => {
        return quoteId === quote.chartState.quote
      })
      const selectedQuote = quoteList[0]
      if(
        quoteList.length > 0
        &&
        selectedQuote.chartState.fromDateTime
        &&
        selectedQuote.chartState.toDateTime
        &&
        selectedQuote.chartState.dateTime
      ){
        this.props.setChartStateFromDatetime(quoteId, selectedQuote.chartState.fromDateTime)
        this.props.setChartStateToDatetime(quoteId, selectedQuote.chartState.toDateTime)
        this.props.setChartStateDatetime(quoteId, selectedQuote.chartState.dateTime)
        this.props.setChartStateScale(quoteId, selectedQuote.chartState.scale)
        this.props.setChartStateDatetime(quoteId, selectedQuote.chartState.dateTime)
        this.props.setChartStateZoomXStart(quoteId, selectedQuote.chartState.dataZoomXStart.toString())
        this.props.setChartStateZoomXEnd(quoteId, selectedQuote.chartState.dataZoomXEnd.toString())
        this.props.setChartStateTimeFilter(quoteId, this.props.state.chartState.getEndPeriodDateTime())
        this.refreshApp()()
      } else {
        this.props.clickhouseRequest.getQuotePeriod(parseInt(quoteId)).then((response) => {
          const fromDateTime = response.data[0].fromDateTime
          const toDateTime = response.data[0].toDateTime
          this.props.setChartStateFromDatetime(quoteId, fromDateTime)
          this.props.setChartStateToDatetime(quoteId, toDateTime)
          this.setQuoteChartStateDefault()
          this.refreshApp()()
        })
      }
    })
  }

  /**
   * @return {ChartDataGetter}
   */
  createChartDataGetter()
  {
    return new ChartDataGetter(this.props.clickhouseRequest, this.props.state.chartState)
  }

  /**
   * @return {TickPointListGetter}
   */
  createTickPointListGetter()
  {
    return new TickPointListGetter()
  }

  changeChart() {
    const chartState = this.props.state.chartState
    if(chartState.checkQueryParamsExists()){
      if(chartState.scale === Scale.code.tick){
        this.createChartDataGetter().getTickData().then((response) => {
          const chartOptions = new ChartOptions(
            new TickLine(response.data),
            this
          )
          this.setState({chartOptions: chartOptions})
        })
      } else {
        this.createTickPointListGetter().getTickLine(this.createChartDataGetter()).then((tickPointList) => {
          const chartOptions = new ChartOptions(
            new BarTicksLine(tickPointList, chartState.scale),
            this
          )
          this.setState({chartOptions: chartOptions})
        })
      }
    }
  }

  setQuoteChartStateDefault(){
    const chartState = this.props.state.chartState
    const quoteId = chartState.quote
    this.props.setChartStateScale(quoteId, Scale.code.tick)
    const defaultDatetime = this.props.state.chartState.getLastDatetime()
    this.props.setChartStateDatetime(quoteId, defaultDatetime)
    this.props.setChartStateZoomXStart(quoteId, '0')
    this.props.setChartStateZoomXEnd(quoteId, '100')
    this.props.setChartStateTimeFilter(quoteId, this.props.state.chartState.getEndPeriodDateTime())
  }

  render() {
    return <div className="price_history_app">
      <ChartPanelAppComponent
        setQuoteId={this.setQuoteId()}
        refreshApp={this.refreshApp()}
      />
      {
        this.state.chartOptions &&
        <ReactECharts
          className="price_history_chart"
          option={ isset(this.state.chartOptions) ? this.state.chartOptions.getChartOptions() : {} }
          notMerge={true}
          onEvents={
            {
              click: (params: {value: [number]}) => {
                const chartState = this.props.state.chartState
                this.props.setChartStateZoomXStart(chartState.quote, '0')
                this.props.setChartStateZoomXEnd(chartState.quote, '100')
                const timeFilter = DateTimeHelper.unixToDateTimeFormat(params.value[0])
                this.props.setChartStateTimeFilter(chartState.quote, timeFilter)
                this.setState({})
              }
            }
          }
        />
      }
    </div>
  }

}

export const PriceHistoryAppComponent = connector(PriceHistoryApp)