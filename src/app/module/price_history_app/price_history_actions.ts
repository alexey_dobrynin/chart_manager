import {Dispatch, Action, AnyAction, Reducer} from 'redux'
import PriceHistoryAppState from './price_history_app_state'
import ObjectHelper from '../helper/object_helper'
import initFromObject from '../object/init_from_object'
import ChartState from '../chart/chart_state'
import SelectedTools from '../tool/selected_tools'
import QuoteList from '../quote/quote_list'
import Quote from '../quote/quote'
import SetQuoteListAction from '../action/set_quote_list_action'
import SetQuoteAction from '../action/set_quote_action'
import SetChartStateFromDatetimeAction from '../action/set_chart_state_from_datetime_action'
import SetChartStateToDatetimeAction from '../action/set_chart_state_to_datetime_action'
import SetChartStateDatetimeAction from '../action/set_chart_state_datetime_action'
import SetChartStateScaleAction from '../action/set_chart_state_scale_action'
import SetChartStateZoomXStartAction from '../action/set_chart_state_zoomx_start_action'
import SetChartStateZoomXEndAction from '../action/set_chart_state_zoomx_end_action'
import SetChartStateTimeFilterAction from '../action/set_chart_state_time_filter_action'
import AbstractTool from '../tool/abstract_tool'
import AddActiveToolListAction from '../action/add_active_tool_list_action'
import DeleteActiveToolListAction from '../action/delete_active_tool_list_action'
import Bookmark from '../bookmark/bookmark'
import CreateChartBookmarkAction from '../action/create_chart_bookmark_action'
import RenameChartBookmarkAction from '../action/rename_chart_bookmark_action'
import DeleteChartBookmarkAction from '../action/delete_chart_bookmark_action'
import LoadChartFromBookmarkAction from '../action/load_chart_from_bookmark_action'
import AbstractTradeStrategyParams from '../trade/strategy/params/abstract_trade_strategy_params'
import AddTradeStrategyListAction from '../action/add_trade_strategy_list_action'
import DeleteTradeStrategyParamsAction from '../action/delete_trade_strategy_params_action'


export default class PriceHistoryActions {

  static type = Object.freeze({
    SET_QUOTE_LIST: 'SET_QUOTE_LIST',
    SET_QUOTE: 'SET_QUOTE',
    SET_CHART_STATE_FROM_DATETIME: 'SET_CHART_STATE_FROM_DATETIME',
    SET_CHART_STATE_TO_DATETIME: 'SET_CHART_STATE_TO_DATETIME',
    SET_CHART_STATE_DATETIME: 'SET_CHART_STATE_DATETIME',
    SET_CHART_STATE_SCALE: 'SET_CHART_STATE_SCALE',
    SET_CHART_STATE_DATA_ZOOM_X_START: 'SET_CHART_STATE_DATA_ZOOM_X_START',
    SET_CHART_STATE_DATA_ZOOM_X_END: 'SET_CHART_STATE_DATA_ZOOM_X_END',
    SET_CHART_STATE_TIME_FILTER: 'SET_CHART_STATE_TIME_FILTER',
    ADD_ACTIVE_TOOL_LIST: 'ADD_ACTIVE_TOOL_LIST',
    DELETE_ACTIVE_TOOL_LIST: 'DELETE_ACTIVE_TOOL_LIST',
    CREATE_CHART_BOOKMARK: 'CREATE_CHART_BOOKMARK',
    RENAME_CHART_BOOKMARK: 'RENAME_CHART_BOOKMARK',
    DELETE_CHART_BOOKMARK: 'DELETE_CHART_BOOKMARK',
    LOAD_CHART_FROM_BOOKMARK: 'LOAD_CHART_FROM_BOOKMARK',
    ADD_TRADE_STRATEGY_LIST: 'ADD_TRADE_STRATEGY_LIST',
    DELETE_TRADE_STRATEGY_PARAMS: 'DELETE_TRADE_STRATEGY_PARAMS',
  })

  static getDefaultState()
  {
    return new PriceHistoryAppState()
  }

  static getActions(dispatch: Dispatch<Action>) {
    return {
      setQuoteList: PriceHistoryActions.setQuoteListAction(dispatch),
      setQuote: PriceHistoryActions.setQuoteAction(dispatch),
      setChartStateFromDatetime: PriceHistoryActions.setChartStateFromDatetimeAction(dispatch),
      setChartStateToDatetime: PriceHistoryActions.setChartStateToDatetimeAction(dispatch),
      setChartStateDatetime: PriceHistoryActions.setChartStateDatetimeAction(dispatch),
      setChartStateScale: PriceHistoryActions.setChartStateScaleAction(dispatch),
      setChartStateZoomXStart: PriceHistoryActions.setChartStateZoomXStartAction(dispatch),
      setChartStateZoomXEnd: PriceHistoryActions.setChartStateZoomXEndAction(dispatch),
      setChartStateTimeFilter: PriceHistoryActions.setChartStateTimeFilterAction(dispatch),
      addActiveToolList: PriceHistoryActions.addActiveToolListAction(dispatch),
      deleteActiveToolList: PriceHistoryActions.deleteActiveToolListAction(dispatch),
      createChartBookmark: PriceHistoryActions.createChartBookmarkAction(dispatch),
      renameChartBookmark: PriceHistoryActions.renameChartBookmarkAction(dispatch),
      deleteChartBookmark: PriceHistoryActions.deleteChartBookmarkAction(dispatch),
      loadChartFromBookmark: PriceHistoryActions.loadChartFromBookmarkAction(dispatch),
      addTradeStrategyList: PriceHistoryActions.addTradeStrategyListAction(dispatch),
      deleteTradeStrategyParams: PriceHistoryActions.deleteTradeStrategyParamsAction(dispatch),
    }
  }

  static getReducers(): Reducer<PriceHistoryAppState, AnyAction>
  {

    return (
      (state: PriceHistoryAppState = PriceHistoryActions.getDefaultState(), action: AnyAction) =>
      {
        switch (action.type){
          case PriceHistoryActions.type.SET_QUOTE_LIST:
            return PriceHistoryActions.setQuoteListReducer(state, <SetQuoteListAction>action)
          case PriceHistoryActions.type.SET_QUOTE:
            return PriceHistoryActions.setQuoteReducer(state, <SetQuoteAction>action)
          case PriceHistoryActions.type.SET_CHART_STATE_FROM_DATETIME:
            return PriceHistoryActions.setChartStateFromDatetimeReducer(state, <SetChartStateFromDatetimeAction>action)
          case PriceHistoryActions.type.SET_CHART_STATE_TO_DATETIME:
            return PriceHistoryActions.setChartStateToDatetimeReducer(state, <SetChartStateToDatetimeAction>action)
          case PriceHistoryActions.type.SET_CHART_STATE_DATETIME:
            return PriceHistoryActions.setChartStateDatetimeReducer(state, <SetChartStateDatetimeAction>action)
          case PriceHistoryActions.type.SET_CHART_STATE_SCALE:
            return PriceHistoryActions.setChartStateScaleReducer(state, <SetChartStateScaleAction>action)
          case PriceHistoryActions.type.SET_CHART_STATE_DATA_ZOOM_X_START:
            return PriceHistoryActions.setChartStateZoomXStartReducer(state, <SetChartStateZoomXStartAction>action)
          case PriceHistoryActions.type.SET_CHART_STATE_DATA_ZOOM_X_END:
            return PriceHistoryActions.setChartStateZoomXEndReducer(state, <SetChartStateZoomXEndAction>action)
          case PriceHistoryActions.type.SET_CHART_STATE_TIME_FILTER:
            return PriceHistoryActions.setChartStateTimeFilterReducer(state, <SetChartStateTimeFilterAction>action)
          case PriceHistoryActions.type.ADD_ACTIVE_TOOL_LIST:
            return PriceHistoryActions.addActiveToolListReducer(state, <AddActiveToolListAction>action)
          case PriceHistoryActions.type.DELETE_ACTIVE_TOOL_LIST:
            return PriceHistoryActions.deleteActiveToolListReducer(state, <DeleteActiveToolListAction>action)
          case PriceHistoryActions.type.CREATE_CHART_BOOKMARK:
            return PriceHistoryActions.createChartBookmarkReducer(state, <CreateChartBookmarkAction>action)
          case PriceHistoryActions.type.RENAME_CHART_BOOKMARK:
            return PriceHistoryActions.renameChartBookmarkReducer(state, <RenameChartBookmarkAction>action)
          case PriceHistoryActions.type.DELETE_CHART_BOOKMARK:
            return PriceHistoryActions.deleteChartBookmarkReducer(state, <DeleteChartBookmarkAction>action)
          case PriceHistoryActions.type.LOAD_CHART_FROM_BOOKMARK:
            return PriceHistoryActions.loadChartFromBookmarkReducer(state, <LoadChartFromBookmarkAction>action)
          case PriceHistoryActions.type.ADD_TRADE_STRATEGY_LIST:
            return PriceHistoryActions.addTradeStrategyListReducer(state, <AddTradeStrategyListAction>action)
          case PriceHistoryActions.type.DELETE_TRADE_STRATEGY_PARAMS:
            return PriceHistoryActions.deleteTradeStrategyParamsReducer(state, <DeleteTradeStrategyParamsAction>action)
          default:
            return state
        }
      }
    )
  }

  static setQuoteListAction(dispatch: Dispatch<Action>) {
    return (
        (quoteList: QuoteList) => {
        dispatch({type: PriceHistoryActions.type.SET_QUOTE_LIST, quoteList: quoteList})
      }
    )
  }

  static setQuoteListReducer(
      state: PriceHistoryAppState,
      action: SetQuoteListAction
  ): PriceHistoryAppState {
    const newState = state.clone()
    newState.quoteList = action.quoteList
    return newState
  }

  static setQuoteAction(dispatch: Dispatch<Action>): (quoteId: string) => void{
    return (
        (quoteId: string) => {
        dispatch({type: PriceHistoryActions.type.SET_QUOTE, quoteId: quoteId})
      }
    )
  }

  static setQuoteReducer(
      state: PriceHistoryAppState,
      action: SetQuoteAction
  ): PriceHistoryAppState {
    const newState = state.clone()
    newState.chartState.quote = action.quoteId
    return newState
  }

  static setQuoteStateParameterReducer(
      state: PriceHistoryAppState,
      action: AnyAction,
      stateParameterName: string
  ): PriceHistoryAppState {
    const newState = state.clone()
    const quoteList = newState.quoteList.getItems().filter((quote: Quote) => {
      return quote.chartState.quote === action.quoteId
    })
    if(quoteList.length > 0){
      quoteList[0].chartState[stateParameterName] = action[stateParameterName]
      newState.chartState[stateParameterName] = action[stateParameterName]
    }
    return newState
  }

  static setQuoteStateParameterAction(
      dispatch: Dispatch<Action>,
      actionType: string,
      stateParameterName: string) {
    return (
      (quoteId: string, parameter: string) => {
        const actionObject: any = {
          type: actionType,
          quoteId: quoteId
        }
        actionObject[stateParameterName] = parameter
        dispatch(actionObject)
      }
    )
  }

  static setChartStateFromDatetimeAction(dispatch: Dispatch<Action>) {
    return PriceHistoryActions.setQuoteStateParameterAction(
      dispatch,
      PriceHistoryActions.type.SET_CHART_STATE_FROM_DATETIME,
      'fromDateTime'
    )
  }

  static setChartStateFromDatetimeReducer(state: PriceHistoryAppState, action: SetChartStateFromDatetimeAction) {
    return PriceHistoryActions.setQuoteStateParameterReducer(state, action, 'fromDateTime')
  }

  static setChartStateToDatetimeAction(dispatch: Dispatch<Action>) {
    return PriceHistoryActions.setQuoteStateParameterAction(
      dispatch,
      PriceHistoryActions.type.SET_CHART_STATE_TO_DATETIME,
      'toDateTime'
    )
  }

  static setChartStateToDatetimeReducer(state: PriceHistoryAppState, action: SetChartStateToDatetimeAction) {
    return PriceHistoryActions.setQuoteStateParameterReducer(state, action, 'toDateTime')
  }

  static setChartStateDatetimeAction(dispatch: Dispatch<Action>) {
    return PriceHistoryActions.setQuoteStateParameterAction(
      dispatch,
      PriceHistoryActions.type.SET_CHART_STATE_DATETIME,
      'dateTime'
    )
  }

  static setChartStateDatetimeReducer(state: PriceHistoryAppState, action: SetChartStateDatetimeAction) {
    return PriceHistoryActions.setQuoteStateParameterReducer(state, action, 'dateTime')
  }

  static setChartStateScaleAction(dispatch: Dispatch<Action>) {
    return PriceHistoryActions.setQuoteStateParameterAction(
      dispatch,
      PriceHistoryActions.type.SET_CHART_STATE_SCALE,
      'scale'
    )
  }

  static setChartStateScaleReducer(state: PriceHistoryAppState, action: Action) {
    return PriceHistoryActions.setQuoteStateParameterReducer(state, action, 'scale')
  }

  static setChartStateZoomXStartAction(dispatch: Dispatch<Action>) {
    return PriceHistoryActions.setQuoteStateParameterAction(
      dispatch,
      PriceHistoryActions.type.SET_CHART_STATE_DATA_ZOOM_X_START,
      'dataZoomXStart'
    )
  }

  static setChartStateZoomXStartReducer(state: PriceHistoryAppState, action: SetChartStateZoomXStartAction) {
    return PriceHistoryActions.setQuoteStateParameterReducer(state, action, 'dataZoomXStart')
  }

  static setChartStateZoomXEndAction(dispatch: Dispatch<Action>) {
    return PriceHistoryActions.setQuoteStateParameterAction(
      dispatch,
      PriceHistoryActions.type.SET_CHART_STATE_DATA_ZOOM_X_END,
      'dataZoomXEnd'
    )
  }

  static setChartStateZoomXEndReducer(state: PriceHistoryAppState, action: SetChartStateZoomXEndAction) {
    return PriceHistoryActions.setQuoteStateParameterReducer(state, action, 'dataZoomXEnd')
  }

  static setChartStateTimeFilterAction(dispatch: Dispatch<Action>) {
    return PriceHistoryActions.setQuoteStateParameterAction(
      dispatch,
      PriceHistoryActions.type.SET_CHART_STATE_TIME_FILTER,
      'timeFilter'
    )
  }

  static setChartStateTimeFilterReducer(state: PriceHistoryAppState, action: SetChartStateTimeFilterAction) {
    return PriceHistoryActions.setQuoteStateParameterReducer(state, action, 'timeFilter')
  }

  static addActiveToolListAction(dispatch: Dispatch<Action>) {
    return (
      (tool: AbstractTool) => {
        dispatch({type: PriceHistoryActions.type.ADD_ACTIVE_TOOL_LIST, tool: tool})
      }
    )
  }

  static addActiveToolListReducer(state: PriceHistoryAppState, action: AddActiveToolListAction) {
    const newState = state.clone()
    newState.selectedTools.items[action.tool.toJson()] = action.tool
    return newState
  }

  static deleteActiveToolListAction(dispatch: Dispatch<Action>) {
    return (
      (toolJson: string) => {
        dispatch({type: PriceHistoryActions.type.DELETE_ACTIVE_TOOL_LIST, toolJson: toolJson})
      }
    )
  }

  static deleteActiveToolListReducer(state: PriceHistoryAppState, action: DeleteActiveToolListAction) {
    const newState = state.clone()
    delete newState.selectedTools.items[action.toolJson]
    return newState
  }

  static createChartBookmarkAction(dispatch: Dispatch<Action>) {
    return (
        (bookmark: Bookmark) => {
          dispatch({type: PriceHistoryActions.type.CREATE_CHART_BOOKMARK, bookmark: bookmark})
      }
    )
  }

  static createChartBookmarkReducer(state: PriceHistoryAppState, action: CreateChartBookmarkAction) {
    const newState = state.clone()
    const bookmark = action.bookmark
    newState.bookmarkList.items[bookmark.name] = bookmark
    return newState
  }

  static renameChartBookmarkAction(dispatch: Dispatch<Action>) {
    return (
        (oldName: string, newName: string) => {
        dispatch({type: PriceHistoryActions.type.RENAME_CHART_BOOKMARK, oldName: oldName, newName: newName})
      }
    )
  }

  static renameChartBookmarkReducer(state: PriceHistoryAppState, action: RenameChartBookmarkAction) {
    const newState = state.clone()
    if(action.newName !== action.oldName){
      const bookmark: Record<string, any> = ObjectHelper.clone(newState.bookmarkList.items[action.oldName])
      delete newState.bookmarkList.items[action.oldName]
      bookmark.name = action.newName
      newState.bookmarkList.items[action.newName] = bookmark
    }
    return newState
  }

  static deleteChartBookmarkAction(dispatch: Dispatch<Action>) {
    return (
        (name: string) => {
        dispatch({type: PriceHistoryActions.type.DELETE_CHART_BOOKMARK, name: name})
      }
    )
  }

  static deleteChartBookmarkReducer(state: PriceHistoryAppState, action: DeleteChartBookmarkAction) {
    const newState = state.clone()
    if(action.name in newState.bookmarkList.items){
      delete newState.bookmarkList.items[action.name]
    }
    return newState
  }

  static loadChartFromBookmarkAction(dispatch: Dispatch<Action>) {
    return (
        (bookmarkName: string) => {
        dispatch({type: PriceHistoryActions.type.LOAD_CHART_FROM_BOOKMARK, bookmarkName: bookmarkName})
      }
    )
  }

  static loadChartFromBookmarkReducer(state: PriceHistoryAppState, action: LoadChartFromBookmarkAction) {
    const newState = state.clone()
    if(action.bookmarkName in newState.bookmarkList.items){
      const bookmark: Bookmark = newState.bookmarkList.items[action.bookmarkName]
      newState.chartState = new ChartState()
      initFromObject(newState.chartState, bookmark.chartState)
      newState.selectedTools = new SelectedTools(bookmark.selectedTools.items)
    }
    return newState
  }

  static addTradeStrategyListAction(dispatch: Dispatch<Action>) {
    return (
        (tradeStrategyParams: AbstractTradeStrategyParams) => {
        dispatch({
          type: PriceHistoryActions.type.ADD_TRADE_STRATEGY_LIST,
          tradeStrategyParams: tradeStrategyParams
        })
      }
    )
  }

  static addTradeStrategyListReducer(state: PriceHistoryAppState, action: AddTradeStrategyListAction) {
    const newState = state.clone()
    newState.selectedTradeStrategy.items[action.tradeStrategyParams.toJson()] = action.tradeStrategyParams
    return newState
  }

  static deleteTradeStrategyParamsAction(dispatch: Dispatch<Action>) {
    return (
        (tradeStrategyParams: string) => {
        dispatch({
          type: PriceHistoryActions.type.DELETE_TRADE_STRATEGY_PARAMS,
          tradeStrategyParams: tradeStrategyParams
        })
      }
    )
  }

  static deleteTradeStrategyParamsReducer(state: PriceHistoryAppState, action: DeleteTradeStrategyParamsAction) {
    const newState = state.clone()
    delete newState.selectedTradeStrategy.items[action.tradeStrategyParams]
    return newState
  }
}