import ChartState from '../chart/chart_state'
import initFromObject from '../object/init_from_object'
import QuoteList from '../quote/quote_list'
import ToolList from '../tool/tool_list'
import SelectedTools from '../tool/selected_tools'
import BookmarkList from '../bookmark/bookmark_list'
import TradeStrategyList from '../trade/trade_strategy_list'
import SelectedTradeStrategy from '../trade/strategy/selected_trade_strategy'
import {AnyAction, Store} from "redux";

export default class PriceHistoryAppState {

  /** @type {ChartState} */
  chartState

  /** @type {QuoteList} */
  quoteList

  /** @type {ToolList} */
  toolList

  /** @type {SelectedTools} */
  selectedTools

  /** @type {BookmarkList} */
  bookmarkList

  /** @type {TradeStrategyList} */
  tradeStrategyList

  /** @type {SelectedTradeStrategy} */
  selectedTradeStrategy

  constructor() {
    this.chartState = new ChartState()
    this.quoteList = new QuoteList([])
    this.toolList = new ToolList()
    this.selectedTools = new SelectedTools({})
    this.bookmarkList = new BookmarkList({})
    this.tradeStrategyList = new TradeStrategyList()
    this.selectedTradeStrategy = new SelectedTradeStrategy({})
  }

  /**
   * @return {PriceHistoryAppState}
   */
  clone() {
    return Object.assign(Object.create(this), this)
  }

  /**
   * @return {String}
   */
  static getLocalStorageName() {
    return 'priceHistoryAppState'
  }

  static loadFromStorage()
  {
    const priceHistoryAppState = new PriceHistoryAppState()
    try {
      const serializedState = localStorage.getItem(PriceHistoryAppState.getLocalStorageName())
      if(typeof serializedState === 'string'){
        const jsonObject = JSON.parse(serializedState)
        initFromObject(priceHistoryAppState, jsonObject)
      }
    } catch (err) {
      console.log('PriceHistoryAppState::loadFromStorage error: ', err)
    }
    return priceHistoryAppState
  }

  /**
   * @param {Store<Function>} store
   * @return {Function}
   */
  static saveToStorage(store: Store<any, AnyAction>)
  {
    /** @type PriceHistoryAppState */
    return () => {
      const state = store.getState()
      try {
        if(state instanceof PriceHistoryAppState){
          const serializedState = JSON.stringify(state)
          localStorage.setItem(PriceHistoryAppState.getLocalStorageName(), serializedState)
        }
      } catch (err) {
        console.log('PriceHistoryAppState::saveToStorage error: ', err)
      }
    }
  }
}