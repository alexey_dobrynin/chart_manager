import React from 'react'
import Modal from '../modal/modal'
import ToolFormGenerator from '../tool/tool_form_generator'
import AbstractTool from '../tool/abstract_tool'
import {connector, PriceHistoryAppChildRefresh} from './connector'


interface ActiveToolListState {
  modalOpen: boolean
  modalContent: JSX.Element|null
  selectedTool: string
}

export interface ActiveToolListInterface {
  props: PriceHistoryAppChildRefresh
  addTool: (tool: AbstractTool) => void
  closeModal: () => void
  editTool: (abstractTool: AbstractTool) => void
}

/** Список активных инструментов */
class ActiveToolList extends React.Component<PriceHistoryAppChildRefresh, ActiveToolListState> implements ActiveToolListInterface{

  constructor(props: PriceHistoryAppChildRefresh) {
    super(props)
    this.state = {
      modalOpen: false,
      modalContent: null,
      selectedTool: ''
    }
  }

  /**
   * Добавление активного инструмента
   * @param {AbstractTool} tool
   */
  addTool(tool: AbstractTool) {
    this.props.addActiveToolList(tool)
    this.props.refreshApp()
  }

  /**
   * Изменение активного инструмента
   * @param {AbstractTool} abstractTool
   */
  editTool(abstractTool: AbstractTool) {
    if(this.state.selectedTool !== '') {
      this.props.deleteActiveToolList(this.state.selectedTool)
      this.props.addActiveToolList(abstractTool)
      this.setState({selectedTool : abstractTool.toJson()})
    }
    this.props.refreshApp()
  }

  /**
   * @return {Function} callback при изменении выбора активного инструмента
   */
  getOnChange() {
    return (event: React.ChangeEvent<HTMLSelectElement>) => {
      this.setState({
        modalContent: this.getToolListForm(event.target.value),
        selectedTool: event.target.value
      })
    }
  }

  /**
   * @return {Function} функция удаления активного инструмента
   */
  getOnDelete() {
    return () => {
      this.deleteSelectedTool()
    }
  }

  deleteSelectedTool() {
    if(this.state.selectedTool.length > 0){
      this.props.deleteActiveToolList(this.state.selectedTool)
      this.setState({selectedTool: ''})
      this.showModal(this.getToolListForm(''))
    }
  }

  /**
   * @return {Function} функция изменения активного инструмента
   */
  getOnEdit() {
    return () => {
      if(this.state.selectedTool.length > 0) {
        const selectedTool = this.state.selectedTool
        const tools = this.props.state.selectedTools.getItems()
        const tool = tools[selectedTool]
        this.showEditToolModal(tool)
      }
    }
  }

  /**
   * @param {AbstractTool} tool
   */
  showEditToolModal(tool: AbstractTool) {
    const form = <form>{ ToolFormGenerator.getEditFormElements(tool, this ) }</form>
    this.showModal(form)
  }

  render() {
    const toolList = this.props.state.toolList
    let toolOptions = toolList.getItems().map((tool) => {
      return <div
        key={ tool.getName() }
        className="dropdown-item"
        onClick={() => this.showAddToolModal(tool)}>{ tool.getName() }</div>
    })

    return <nav
      className="dropdown"
      id="tool_list"
    >
      <button
        className="drop_button"
        onClick={() => this.showModal(this.getToolListForm(this.state.selectedTool)) }
      >
        Инструменты ({Object.keys(this.props.state.selectedTools.items).length })
      </button>
      <div className="dropdown-content">
        { toolOptions }
      </div>
      <Modal isOpen={this.state.modalOpen} onClose={() => this.closeModal()}>
        {this.state.modalContent}
      </Modal>
    </nav>
  }

  showModal(form: JSX.Element) {
    this.setState({
      modalOpen: true,
      modalContent: form
    })
  }

  closeModal() {
    this.setState({ modalOpen: false })
  }

  showAddToolModal(tool: AbstractTool) {
    const form = <form key="showAddToolModal">{ ToolFormGenerator.getAddFormElements(tool, this) }</form>
    this.showModal(form)
  }

  /**
   * @param {String} selectedTool
   * @return {JSX.Element} форма редактирования списка активных инструментов
   */
  getToolListForm(selectedTool: string) {
    const activeToolListOptions = Object.keys(this.props.state.selectedTools.getItems())
      .map((toolSettings) => {
        const toolSettingsObject = JSON.parse(toolSettings)
        return <option
          key={toolSettings}
          style={ {backgroundColor: toolSettingsObject.color} }
          title={ toolSettings }
        >{ toolSettings }</option>
      })

    return <form onKeyDown={ this.onKeyDown() }>
      <select
        size={ 10 }
        value={ selectedTool }
        onChange={ this.getOnChange() }
        onDoubleClick={ this.getOnEdit() }
        style={{maxWidth: "1000px"}}
      >
        <option disabled value="">Выберите инструмент:</option>
        {activeToolListOptions}
      </select>
      <hr/>
      <button type="button" onClick={ this.applyTools() }>Применить</button>
      {
        Object.keys(this.props.state.selectedTools.getItems()).length > 0 &&
        <button type="button" onClick={ this.getOnEdit() }>Изменить</button>
      }
      <button type="button" onClick={ this.getOnDelete() }>Удалить</button>
    </form>
  }

  /**
   * @return {Function}
   */
  applyTools() {
    return () => {
      this.closeModal()
      this.props.refreshApp()
    }
  }

  /**
   * @return {Function}
   */
  onKeyDown() {
    return (event: React.KeyboardEvent) => {
      if(event.key === 'Delete'){
        this.deleteSelectedTool()
      }
    }
  }
}

export const ActiveToolListComponent = connector(ActiveToolList)