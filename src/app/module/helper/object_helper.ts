export default class ObjectHelper {

  /**
   * @param {Object} object
   * @return {Object}
   */
  static clone(object: Record<string, any>): any {
    const cloneObject = Object.create(Object.getPrototypeOf(object))
    Object.getOwnPropertyNames(object).forEach((propertyName) => {
      if(object.hasOwnProperty(propertyName)){
        cloneObject[propertyName] = object[propertyName]
      }
    })
    return cloneObject
  }

  /**
   * @param {Object} obj
   * @return {string}
   */
  static toJson(obj: Record<string, any>): string{
    const object: Record<string, any> = {}
    Object.getOwnPropertyNames(obj).forEach((propertyName) => {
      object[propertyName] = obj[propertyName]
    })
    return JSON.stringify(object)
  }
}