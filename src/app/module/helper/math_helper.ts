export default class MathHelper {
    /**
     * @param {String} type тип корректировки
     * @param {Number} value число
     * @param {Number} exp показатель степени (десятичный логарифм основания корректировки)
     * @returns {Number} скорректированное значение
     */
    static decimalAdjust(type: 'round' | 'floor' | 'ceil', value: number, exp: number): number{
        // Если степень не определена, либо равна нулю...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value)
        }
        value = +value
        exp = +exp
        // Если значение не является числом, либо степень не является целым числом...
        if (isNaN(value) || !(exp % 1 === 0)) {
            return NaN
        }
        // Сдвиг разрядов
        let val = value.toString().split('e')
        value = Math[type](+(val[0] + 'e' + (val[1] ? (+val[1] - exp) : -exp)))
        // Обратный сдвиг
        val = value.toString().split('e')
        return +(val[0] + 'e' + (val[1] ? (+val[1] + exp) : exp))
    }

    /**
     * @param {Number} value число
     * @param {Number} exp показатель степени (десятичный логарифм основания корректировки)
     * @returns {Number} математически округлённое значение
     */
    static round(value: number, exp: number): number{
        return MathHelper.decimalAdjust('round', value, exp)
    }

    /**
     * @param {Number} value число
     * @param {Number} exp показатель степени (десятичный логарифм основания корректировки)
     * @returns {Number} округление в меньшую сторону
     */
    static floor(value: number, exp: number): number {
        return MathHelper.decimalAdjust('floor', value, exp)
    }

    /**
     * @param {Number} value число
     * @param {Number} exp показатель степени (десятичный логарифм основания корректировки)
     * @returns {Number} округление в большую сторону
     */
    static ceil(value: number, exp: number): number {
        return MathHelper.decimalAdjust('ceil', value, exp)
    }
}