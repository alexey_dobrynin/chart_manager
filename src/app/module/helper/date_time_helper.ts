import DatePeriod from '../time/date_period'
import TimePeriod from '../time/time_period'

export default class DateTimeHelper {

    /**
     * @param {Date} date
     * @return {string} дата в формате `Y-m-d`
     */
    static dateToDateFormat(date: Date): string{
        const year = date.getFullYear();
        const month = ('0' + (date.getMonth() + 1)).slice(-2);
        const day = ('0' + date.getDate()).slice(-2);
        return `${year}-${month}-${day}`
    }

    /**
     * @param {Date} dateTime
     * @return {string} дата и время в формате `Y-m-d H:i:s`
     */
    static dateTimeToDateTimeFormat(dateTime: Date): string{
        const dateFormat = DateTimeHelper.dateToDateFormat(dateTime)
        const hours = ('0' + dateTime.getHours()).slice(-2);
        const minutes = ('0' + dateTime.getMinutes()).slice(-2);
        const seconds = ('0' + dateTime.getSeconds()).slice(-2);
        return `${dateFormat} ${hours}:${minutes}:${seconds}`
    }

    /**
     * @param {Number} unixTime время unixtime в милисекундах
     * @return {String} дата и время в формате `Y-m-d H:i:s`
     */
    static unixToDateTimeFormat(unixTime: number): string{
        const dateTime = new Date(unixTime)
        return DateTimeHelper.dateTimeToDateTimeFormat(dateTime)
    }

    /**
     * @param {Number} unixTime время unixtime в милисекундах
     * @return {String} дата и время в формате `Y-m-d H:i:s`
     */
    static unixToDateTimeMillisecondsFormat(unixTime: number): string{
        const dateTime = new Date(unixTime)
        const dateTimeString = DateTimeHelper.unixToDateTimeFormat(unixTime)
        const milliseconds = dateTime.getMilliseconds().toString().padStart(3, '0')
        return `${dateTimeString}.${milliseconds}`
    }

    /**
     * @param {Date} date
     * @return {Date}
     */
    static clone(date: Date): Date {
        return new Date(date.getTime())
    }

    /**
     * @param {Date} dateTime
     * @return {Date}
     */
    static dateTimeToDate(dateTime: Date): Date {
        const date = DateTimeHelper.clone(dateTime)
        date.setHours(0,0,0,0)
        return date
    }

    /**
     * @param {Date} dateTime
     * @return {Date}
     */
    static getNextDate(dateTime: Date): Date {
        const date = DateTimeHelper.dateTimeToDate(dateTime)
        date.setDate(dateTime.getDate() + 1)
        return date
    }

    /**
     * @param {Date} dateTime
     * @return {Date}
     */
    static getNextHour(dateTime: Date): Date {
        const nextHourDateTime = DateTimeHelper.clone(dateTime)
        nextHourDateTime.setMilliseconds(0)
        nextHourDateTime.setSeconds(0)
        nextHourDateTime.setMinutes(0)
        const nextHourUnixTime = nextHourDateTime.getTime() + 60*60*1000
        return new Date(nextHourUnixTime)
    }

    /**
     * @param {...Date} args
     * @return {Date}
     */
    static max(...args: Date[]): Date{
        return new Date(Math.max(...args.map(Number)))
    }

    /**
     * @param {...Date} args
     * @return {Date}
     */
    static min(...args: Date[]): Date{
        return new Date(Math.min(...args.map(Number)))
    }

    /**
     * @param {DatePeriod} datePeriod
     * @return {DatePeriod[]}
     */
    static getDatePeriodList(datePeriod: DatePeriod): DatePeriod[] {
        const datePeriodList = []
        if(datePeriod.getBegin() <= datePeriod.getEnd()) {
            let endDate
            let startDate = DateTimeHelper.clone(datePeriod.getBegin())
            do {
                endDate = DateTimeHelper.min(
                    DateTimeHelper.getNextDate(startDate),
                    DateTimeHelper.clone(datePeriod.getEnd())
                )
                datePeriodList.push(new DatePeriod(startDate, endDate))
                startDate = DateTimeHelper.clone(endDate)
            } while (endDate < datePeriod.getEnd())

        }
        return datePeriodList
    }

    /**
     * @param {DatePeriod} datePeriod
     * @return {DatePeriod[]}
     */
    static getHourPeriodList(datePeriod: DatePeriod): DatePeriod[] {
        const hourPeriodList = []
        if(datePeriod.getBegin() <= datePeriod.getEnd()) {
            let endHour
            let startHour = DateTimeHelper.clone(datePeriod.getBegin())
            do {
                endHour = DateTimeHelper.min(
                    DateTimeHelper.getNextHour(startHour),
                    DateTimeHelper.clone(datePeriod.getEnd())
                )
                hourPeriodList.push(new DatePeriod(startHour, endHour))
                startHour = DateTimeHelper.clone(endHour)
            } while (endHour < datePeriod.getEnd())

        }
        return hourPeriodList
    }

    /**
     * @param {DatePeriod[]} hourPeriodList
     * @param {TimePeriod[]} timePeriodListFilter
     * @return {DatePeriod[]}
     */
    static filterHourPeriodList(hourPeriodList: DatePeriod[], timePeriodListFilter: TimePeriod[]): DatePeriod[] {
        const filteredPeriodList: DatePeriod[] = []
        hourPeriodList.forEach((hourPeriod) => {
            timePeriodListFilter.forEach((timePeriod) => {
                timePeriod.changeDate(hourPeriod.getBegin())
                if(hourPeriod.getEnd() > timePeriod.getBegin() && hourPeriod.getBegin() < timePeriod.getEnd()){
                    filteredPeriodList.push(
                        new DatePeriod(
                            DateTimeHelper.max(hourPeriod.getBegin(), timePeriod.getBegin()),
                            DateTimeHelper.min(hourPeriod.getEnd(), timePeriod.getEnd())
                        )
                    )
                }
            })
        })
        return  filteredPeriodList
    }
}