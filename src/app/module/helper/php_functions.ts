export function isset(...args: any[]): boolean {
  for(const argument of args){
    if(argument === undefined || argument === null || argument === ''){
      return false
    }
  }
  return true
}

/**
 * @param {String} string
 * @return {string}
 */
export function lcfirst(string: string): string {
  return string.charAt(0).toLowerCase() + string.slice(1)
}