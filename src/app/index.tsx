require('./../../public/css/price_history_app.css')
require('./../../public/css/icons.css')
import React from 'react'
import ReactDOM from 'react-dom'
import {PriceHistoryAppComponent} from './module/price_history_app/price_history_app'
import PriceHistoryActions from './module/price_history_app/price_history_actions'
import PriceHistoryAppState from './module/price_history_app/price_history_app_state'
import { Provider } from 'react-redux'
import {AnyAction, createStore, Store} from 'redux'
import Clickhouse from './module/clickhouse/clickhouse'
import ClickhouseRequest from './module/clickhouse/clickhouse_request'
import {initEChartTradeRectShapes} from './module/chart/echart_shape'


class Index {

  store: Store<PriceHistoryAppState, AnyAction>

  /** @type {Clickhouse} */
  clickhouse: Clickhouse

  constructor(clickhouse: Clickhouse) {
    this.store = this.buildStore()
    this.clickhouse = clickhouse
    this.subscribeSaveStateToLocalStorage()
    initEChartTradeRectShapes()
    this.renderPriceHistoryApp()
  }

  createClickhouseRequest()
  {
    return new ClickhouseRequest(this.clickhouse)
  }

  /**
   * @return {Store<Function>}
   */
  getStore()
  {
    return this.store
  }

  /**
   * @return {Store<Function>}
   */
  buildStore()
  {
    return createStore(PriceHistoryActions.getReducers(), PriceHistoryAppState.loadFromStorage())
  }

  subscribeSaveStateToLocalStorage()
  {
    this.store.subscribe(PriceHistoryAppState.saveToStorage(this.store))
  }

  renderPriceHistoryApp(){
    ReactDOM.render(
      <Provider store={this.getStore()}>
        <PriceHistoryAppComponent
          clickhouseRequest={this.createClickhouseRequest()}
        />
      </Provider>,
      document.getElementById('price_history_app')
    )
  }
}

new Index(
    new Clickhouse('http://127.0.0.1:8123/', 'chart', 'user')
)